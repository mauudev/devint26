#ifndef TENSOR_FLOW_H
#define TENSOR_FLOW_H

#include <string>
#include <IDTO.h>
#include <json.hpp>
#include <INodeAST.h>

using Json = nlohmann::json;
using namespace std;

class TensorFlow: public IDTO, public INodeAST
{
    string m_tensorFlowName;
public:
    TensorFlow();

    TensorFlow(const string&);

    ~TensorFlow();

    const string getTensorFlowName() const;

    void setTensorFlowName(const string&);

    Json serialize();

    void deserialize(Json&);

    Statement &setASTTensorFlowName(BuilderCodePy&);

    Statement &getASTTensorFlowName(BuilderCodePy&);

    Statement &generateASTInitNode(BuilderCodePy&) override;

    Statement &generateASTBodyNode(BuilderCodePy&) override;

    Statement &generateASTClassNode(BuilderCodePy&) override;
};

#endif
