#ifndef KEYBOARD_HANDLER_H
#define KEYBOARD_HANDLER_H

#include <iostream>
#include <string>
#include <json.hpp>
#include <IDTO.h>
#include <PythonAST.h>
#include <BuilderCodePy.h>
#include <BuilderCode.h>
#include <PythonInjector.h>
#include <INodeAST.h>

using namespace std;
using Json = nlohmann::json;

class KeyboardHandler: public IDTO, public INodeAST
{
private:
    string m_keyboardName;

public:
    KeyboardHandler();

    KeyboardHandler(const string&);

    ~KeyboardHandler();

    const string getKeyboardName() const;

    void setKeyboardName(const string&);

    Statement &setKeyboardNameGenerator(BuilderCodePy&);

    Statement &getKeyboardNameGenerator(BuilderCodePy&) const;

    Statement &generateASTInitNode(BuilderCodePy&) override;

    Statement &generateASTBodyNode(BuilderCodePy&) override;

    Statement &generateASTClassNode(BuilderCodePy&) override;

    Json serialize();

    void deserialize(Json&);
};

#endif
