#ifndef PROJECT_TOOLBOX_H
#define PROJECT_TOOLBOX_H

#include <string>
#include "Agent.h"
#include "Toolbox.h"
#include "OpenAI.h"

using namespace std;

class ProjectToolbox : public Toolbox
{
public:
    ProjectToolbox();

    ~ProjectToolbox();

    Agent *getNewAgent(const string&) const;

    OpenAI *getNewOpenAI(const string&) const;
};

#endif
