#ifndef SCREENHANDLER_H
#define SCREENHANDLER_H

#include <string>
#include <json.hpp>
#include <IDTO.h>
#include <PythonAST.h>
#include <BuilderCodePy.h>
#include <BuilderCode.h>
#include <PythonInjector.h>
#include <INodeAST.h>

using namespace std;

class ScreenHandler: public IDTO, public INodeAST
{
private:
    int m_screenSpeed;
    int m_screenHeight;
    int m_screenWidth;
    string m_screenName;

public:
    ScreenHandler();

    ScreenHandler(const int&, const int&, const int&, const string&);

    ~ScreenHandler();

    void setScreenHandlerSpeed(const int&);

    void setScreenHandlerHeight(const int&);

    void setScreenHandlerWidth(const int&);

    void setScreenHandlerName(const string&);

    const int getScreenHandlerSpeed() const;

    const int getScreenHandlerHeight() const;

    const int getScreenHandlerWidth() const;

    const string getScreenHandlerName() const;

    Statement &setScreenHandlerNameGenerator(BuilderCodePy&);

    Statement &setScreenHandlerWidthGenerator(BuilderCodePy&);

    Statement &setScreenHandlerHeightGenerator(BuilderCodePy&);

    Statement &setScreenHandlerSpeedGenerator(BuilderCodePy&);

    Statement &getScreenHandlerNameGenerator(BuilderCodePy&) const;

    Statement &getScreenHandlerWidthGenerator(BuilderCodePy&) const;

    Statement &getScreenHandlerHeightGenerator(BuilderCodePy&) const;

    Statement &getScreenHandlerSpeedGenerator(BuilderCodePy&) const;

    Statement &generateASTInitNode(BuilderCodePy&) override;

    Statement &generateASTBodyNode(BuilderCodePy&) override;

    Statement &generateASTClassNode(BuilderCodePy&) override;

    Json serialize();

    void deserialize(Json&);



};

#endif