#ifndef THINKER_H
#define THINKER_H

#include "Project.h"
#include "ProjectToolbox.h"
#include "AgentToolbox.h"

using namespace std;

class Thinker
{
private:
    static Thinker *m_instance;
    ProjectToolbox *m_projectToolbox;
    AgentToolbox *m_agentToolbox;
    Project *m_project;

public:
    Thinker();

    ~Thinker();

    static Thinker *getInstance();

    Project *newProject();

    ProjectToolbox *getProjectToolbox() const;

    AgentToolbox *getAgentToolbox() const;

    Project *getProject() const;
};

#endif
