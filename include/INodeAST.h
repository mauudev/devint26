#ifndef INODE_AST_H
#define INODE_AST_H

#include <BuilderCodePy.h>

class INodeAST
{
public:

    INodeAST(){}

    virtual ~INodeAST(){}
    //function for the nodes init
    virtual Statement &generateASTInitNode(BuilderCodePy&) = 0;
    //container for all nodes extras
    virtual Statement &generateASTBodyNode(BuilderCodePy&) = 0;
    //container for all node class
    virtual Statement &generateASTClassNode(BuilderCodePy&) = 0;

};

#endif
