#ifndef AGENT_TOOLBOX_H
#define AGENT_TOOLBOX_H

#include "OutputOfAgent.h"
#include "KeyboardHandler.h"
#include "OpenAI.h"
#include "InputOfAgent.h"
#include "ScreenHandler.h"
#include "TensorFlow.h"

using namespace std;

class AgentToolbox
{
public:
    AgentToolbox();

    ~AgentToolbox();

    OutputOfAgent *getNewOutput() const;

    OpenAI *getNewOpenAI(const string&) const;

    InputOfAgent *getNewInput() const;

    ScreenHandler *getNewScreenHandler(const int&, const int&,
        const int&, const string&) const;

    KeyboardHandler *getNewKeyboardHandler(const string&) const;

    TensorFlow *getNewTensorFlow(const string&) const;
};

#endif
