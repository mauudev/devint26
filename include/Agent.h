#ifndef AGENT_H
#define AGENT_H

#include <IDTO.h>
#include <json.hpp>
#include <string>
#include <INodeAST.h>
#include "OutputOfAgent.h"
#include "KeyboardHandler.h"
#include "OpenAI.h"
#include "InputOfAgent.h"
#include "ScreenHandler.h"
#include "TensorFlow.h"

using namespace std;
using Json = nlohmann::json;

class Agent: public IDTO, public INodeAST
{
private:
    string m_agentName;
    OutputOfAgent *m_outputOfAgent;
    OpenAI *m_openAI;
    InputOfAgent *m_inputOfAgent;
    ScreenHandler *m_screenHandler;
    KeyboardHandler *m_keyboardHandler;
    TensorFlow *m_tensorFlow;

public:
    Agent();

    Agent(const string&);

    ~Agent();

    void addNewOutput(OutputOfAgent*);

    OutputOfAgent *getOutputOfAgent() const;

    const string getAgentName() const;

    void setAgentName(const string&);

    void addNewOpenAI(OpenAI*);

    OpenAI *getOpenAI() const;

    void addNewInput(InputOfAgent*);

    InputOfAgent *getInputOfAgent() const;

    void addNewScreenHandler(ScreenHandler*);

    ScreenHandler *getScreenHandler() const;

    void addNewKeyboardHandler(KeyboardHandler*);

    KeyboardHandler *getKeyboardHandler() const;

    void addNewTensorFlow(TensorFlow*);

    TensorFlow *getTensorFlow() const;

    Json serialize();

    void deserialize(Json&);

    Statement &setAgentNameAST(BuilderCodePy&);

    Statement &getAgentNameAST(BuilderCodePy&);

    Statement &generateASTInitNode(BuilderCodePy&) override;

    Statement &generateASTBodyNode(BuilderCodePy&) override;

    Statement &generateASTClassNode(BuilderCodePy&) override;

    Statement &buildASTNode(BuilderCodePy&);

    const string generateCode();
};
#endif
