#ifndef ISTEP_H
#define ISTEP_H

#include <BuilderCodePy.h>

class IStep
{
public:

    IStep(){}

    virtual ~IStep(){}

    virtual Statement &getNextStep(BuilderCodePy&) = 0;

};

#endif
