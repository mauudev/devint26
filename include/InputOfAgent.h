#ifndef INPUT_OF_AGENT_H
#define INPUT_OF_AGENT_H

#include <BuilderCodePy.h>
#include <iostream>
#include <fstream>
#include <INodeAST.h>

class InputOfAgent : public INodeAST
{
public:
    InputOfAgent();

    ~InputOfAgent();

    Statement &generateASTInitNode(BuilderCodePy&) override;

    Statement &generateASTBodyNode(BuilderCodePy&) override;

    Statement &generateASTClassNode(BuilderCodePy&) override;

};

#endif
