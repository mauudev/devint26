#ifndef GTKMM_EXAMPLE_CANVAS_H
#define GTKMM_EXAMPLE_CANVAS_H

#include <gtkmm.h>
#include <gdkmm/pixbuf.h>
#include "Tool.h"


class Canvas : public Gtk::DrawingArea
{
private:
    typedef std::vector<Tool*> typeVecItems;
    bool m_flag;
    bool m_dragDataRequestedForDrop;
    Tool *m_dropItem;
    typeVecItems m_canvasTool;
public:
    Canvas();

    virtual ~Canvas();

    int getCanvasToolSize();
    std::vector<Tool*>* getItemsVector();
    bool onDeleteKeyPress(GdkEventKey*);
    
    void itemDraw(const Tool *p_item,
        const Cairo::RefPtr<Cairo::Context> &p_cairo,
        bool p_preview);

    bool on_draw(const Cairo::RefPtr<Cairo::Context> &p_cairo) override;

    void on_drag_data_received(const Glib::RefPtr<Gdk::DragContext> &p_context,
        int p_x,
        int p_y,
        const Gtk::SelectionData &p_selectionData,
        guint p_info,
        guint p_time) override;

    bool on_drag_motion(const Glib::RefPtr<Gdk::DragContext> &p_context, 
        int p_x,
        int p_y,
        guint p_time) override;

    bool on_drag_drop(const Glib::RefPtr<Gdk::DragContext> &p_context,
        int p_x,
        int p_y,
        guint p_time) override;

    bool on_button_press_event(GdkEventButton *p_event) override;

    bool on_button_release_event(GdkEventButton *p_event) override;
    
    bool on_motion_notify_event(GdkEventMotion *p_event) override;

};

#endif
