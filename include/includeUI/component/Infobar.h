#ifndef INFOBAR_H
#define INFOBAR_H
#include <gtkmm/infobar.h>
#include <gtkmm/label.h>
#include <string>

class InfoBar
{
private:
    Gtk::InfoBar m_infoBar;
    Gtk::Label m_messageLabel;
public:
    InfoBar();
    virtual ~InfoBar();
    void onInfoBarResponse(int);
    Gtk::InfoBar &getInstance();
    void setMessage(const std::string &,const Gtk::MessageType &);
};

#endif