#ifndef ACTION_TOOL_BUTTON_H
#define ACTION_TOOL_BUTTON_H
#include <glibmm.h>
#include <gtkmm/button.h>
#include <gtkmm/image.h>
#include <string>

class ActionToolButton:public Gtk::Button
{
private:
    Gtk::Image m_img;

public:
    ActionToolButton();
    ActionToolButton(const std::string& p_file);
    void enable();
    void disable();
};

#endif