#ifndef TOOLDEFINITION_H
#define TOOLDEFINITION_H

#include <string>
#include <gtkmm.h>

class ToolDefinition : public Gtk::ToolButton
{
    std::string m_typeName;
    std::string m_toolTip;
    std::string m_filepath;
    Gtk::Image m_image;
    Glib::RefPtr<Gdk::Pixbuf> m_imageResource;
    Gtk::Menu m_menuPopup;
    int m_id;
    int m_imageHeight;
    int m_imageWidth;

public:
    ToolDefinition();
    ToolDefinition(std::string p_typeName, std::string p_toolTip, int p_id, 
        int p_imageWidth, int p_imageHeight);
    virtual ~ToolDefinition();

    std::string getTypeName()const;
    std::string getFilepath()const;
    std::string getToolTip()const;
    int getId()const;
    int getImageWidth()const;
    int getimageHight()const;
    void setImageWidth(int);
    void setImageHeight(int);
    Glib::RefPtr<Gdk::Pixbuf> getImage(); 
    void setImage(std::string p_imageName, int p_imageWidth, int p_image);
};
#endif
