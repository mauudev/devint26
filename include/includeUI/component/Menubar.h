#ifndef GTKMM_MENUBAR_H
#define GTKMM_MENUBAR_H
#include <gtkmm.h>
#include <iostream>
using namespace std;

class MenuBar
{
protected:
    Gtk::MenuBar m_menuBar;
    Gtk::Menu m_subMenuFiles;
    Gtk::Menu m_subMenuEdit;
    Gtk::MenuItem m_menuFiles;
    Gtk::MenuItem m_menuEdit;
    Gtk::MenuItem m_newProject;
    Gtk::MenuItem m_openProject;
    Gtk::MenuItem m_saveProject;
    Gtk::MenuItem m_quit;
    Gtk::SeparatorMenuItem m_line;
    Gtk::MenuItem m_menuCut;
    Gtk::MenuItem m_menuCopy;
    Gtk::MenuItem m_menuPaste;
    Gtk::MenuItem m_menuQuit;
    Gtk::MenuItem m_menuExample;
public:
    MenuBar();
    ~MenuBar();
    void createMenuBar(Gtk::Box &);
    void onButtonClickedNew();
    void onButtonClickedOpen();
    void onButtonClickedSave();
    void onButtonClickedClose();
    void otherButtonClicked();
    void onButtonClickedQuit();
};
#endif
