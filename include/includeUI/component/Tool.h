#ifndef GTKMM_TOOL_H
#define GTKMM_TOOL_H

#include <gtkmm.h>
#include "ToolDefinition.h"

class Tool
{

public:
    Glib::RefPtr<Gdk::Pixbuf> m_pixbuf;
    int m_x;
    int m_y;
    ToolDefinition *m_button;
    bool m_toolSelectedForDraw;
    bool m_toolSelectedForEvent;
    int m_width;
    int m_heigth;

    Tool(Gtk::Widget *p_canvas, 
        ToolDefinition *p_button,
        double p_x,
        double p_y);

    Glib::ustring getIconName();

    int getAxisX();

    int getAxisY();

    Glib::RefPtr<Gdk::Pixbuf> getImage();

    int getWidth();

    int getHeigth();

    bool isSelectedForEvent();

    
};
#endif