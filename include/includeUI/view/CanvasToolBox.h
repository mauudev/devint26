#ifndef CANVAS_TOOL_BOX_H
#define CANVAS_TOOL_BOX_H
#include <gtkmm/box.h>
#include "ActionToolToggle.h"
#include "ActionToolButton.h"
#include "Canvas.h"

class CanvasToolBox:public Gtk::Box
{
private:
    ActionToolToggle m_actionToolSelect;
    ActionToolToggle m_actionToolRemove;
    ActionToolToggle m_actionToolSelectArea;
    ActionToolToggle m_actionToolLink;
    ActionToolButton m_actionToolCamera;
    ActionToolButton m_actionToolCopy;
    ActionToolButton m_actionToolDelete;
    ActionToolButton m_actionToolClear;
    Canvas* m_pointCanvas;

public:
    CanvasToolBox();
    ~CanvasToolBox();
    void eventsToggle();
    void eventsButton();
    void mutuallyExclusiveSelect();
    void mutuallyExclusiveRemove();
    void mutuallyExclusiveSelectArea();
    void mutuallyExclusiveBlocking();
    void mutuallyExclusiveLink();
    const char *whichIsActive();
    void actionCanvasCopy();
    void actionCanvasDelete();
    void actionCanvasCamera();
    void actionCanvasClear();
    void setPointCanvas(Canvas* p_canvas);
    const CanvasToolBox* getInstance();

    void removeSelectedTools();

};

#endif