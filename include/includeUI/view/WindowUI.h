#ifndef GTKMM_WINDOWUI_H
#define GTKMM_WINDOWUI_H

#include <gtkmm.h>
#include "CanvasToolBox.h"
#include "Menubar.h"
#include "Infobar.h"
#include "Buttonbox.h"
#include "Linkbutton.h"
#include "Togglebutton.h"
#include "Canvas.h"
#include "ToolPalette.h"


class WindowUI : public Gtk::Window
{
private:
    Gtk::Box m_vBoxMain;
    Gtk::Box m_menuBox;
    Gtk::Box m_hBox;
    Gtk::Box m_infoBox;

    LinkButton m_component1{"https://www.google.com/","Componente 1"};
    LinkButton m_component2{"https://www.google.com/","Componente 2"};
    LinkButton m_component3{"https://www.google.com/","Componente 3"};
    LinkButton m_component4{"https://www.google.com/","Componente 4"};

    MenuBar *m_menuBar = new MenuBar();
    InfoBar m_infoBar;
    Canvas m_canvas;
    Gtk::ScrolledWindow m_scrolledWindowCanvas;
    ToolPalette m_toolPalette;
    CanvasToolBox m_canvasToolBox;

public:
    WindowUI();
    virtual ~WindowUI();
};

#endif