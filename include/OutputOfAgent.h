#ifndef OUTPUT_OF_AGENT_H
#define OUTPUT_OF_AGENT_H

#include <BuilderCodePy.h>
#include <iostream>
#include <fstream>
#include <json.hpp>
#include <INodeAST.h>

using Json = nlohmann::json;
using namespace std;

class OutputOfAgent: public INodeAST
{
public:
    OutputOfAgent();

    ~OutputOfAgent();

    Statement &generateASTInitNode(BuilderCodePy&) override;

    Statement &generateASTBodyNode(BuilderCodePy&) override;

    Statement &generateASTClassNode(BuilderCodePy&) override;
};

#endif
