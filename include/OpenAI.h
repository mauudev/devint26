#ifndef OPEN_AI_H
#define OPEN_AI_H

#include <string>
#include <IDTO.h>
#include <json.hpp>
#include <INodeAST.h>

using namespace std;
using Json = nlohmann::json;

class OpenAI: public IDTO, public INodeAST
{
private:
    string m_openAIName;

public:
    OpenAI();

    OpenAI(const string&);

    ~OpenAI();

    const string getOpenAIName() const;

    void setOpenAIName(const string&);

    Json serialize();

    void deserialize(Json&);

    Statement &setASTOpenAIName(BuilderCodePy&);

    Statement &getASTOpenAIName(BuilderCodePy&);

    Statement &generateASTInitNode(BuilderCodePy&) override;

    Statement &generateASTBodyNode(BuilderCodePy&) override;

    Statement &generateASTClassNode(BuilderCodePy&) override;
};
#endif
