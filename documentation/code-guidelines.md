
# Compilers

All reasonable warning levels should be enabled. Some warning levels, such as GCC's `-Weffc++` warning mode can be too noisy and will not be recommended for normal compilation.

## GCC / Clang

A good combination of settings is `-Wall -Wextra -Wshadow -Wnon-virtual-dtor -pedantic`

 * `-Wall -Wextra`: reasonable and standard
 * `-Wshadow`: warn the user if a variable declaration shadows another with the same name in the same scope
 * `-Wnon-virtual-dtor`: warn the user if a class with virtual functions has a non-virtual destructor. This can lead to hard to track down memory errors
 * `-pedantic`: warn about non-portable code, C++ that uses language extensions.

## CMake

# C++ Coding Standards

Style guidelines are not overly strict. The important thing is that code is clear and readable with an appropriate amount of whitespace and reasonable length lines. A few best practices are also mentioned.

## Descriptive and Consistent Naming

C++ allows for arbitrary length identifier names, so there's no reason to be terse when naming variables. Use descriptive names, and be consistent in the style

### Just 80 columns for any file

All files must have a maximum of 80 columns , try to separate it in new lines.

### Common C++ Naming Conventions

 * Types start with capitals: `MyClass`
 * functions and variables start with lower case: `myMethod`
 * constants are all capital: `const int PI=3.14159265358979323;`

#### Don't forget

 * `lowerCamelCase`. For function names, templates and internal vas of funcions
 * `UpperCamelCase`. For class names and her file

*Note that the C++ standard does not follow any of these guidelines. Everything in the standard is lowercase only.*

### Distinguish Private Object Data

Name private data with a `m_` prefix to distinguish it from public data.

### Distinguish Function Parameters

Name function parameters with an `p_` prefix.

### New lines if there are many arguments

For functions, but avoid this form (smell code)
```cpp
int someFunction(int p_one,
    int p_two,
    int p_three,
    int p_four)
{
    // do something
}

int someVar = someFunction(varOne,
    varTwo,
    varThree,
    varFour);
```

### Well formed example

```cpp
class MyClass
{
// Just Parameters
private:
    int m_data;

protected:
    int m_dataProtected;

public:
    int m_dataPublic;

// Just methods
public:
    MyClass(int p_data)
    : m_data{p_data}
    {
    }

    int getDataPublic() const
    {
        return m_dataPublic;
    }

protected:
    int getDataProtected() const
    {
        return m_dataProtected;
    }

private:
    void someInternalOperations()
    {
    }
};
```

## Distinguish C++ Files From C Files

C++ source file should be named `.cpp` NOT `.cc` NOT `.c`
C++ header files should be named `.h` NOT `.hpp`

## Use `nullptr`

C++11 introduces `nullptr` which is a special type denoting a null pointer value. This should be used instead of 0 or NULL to indicate a null pointer.

## Comments

Comment blocks should use `//`, not `/* */`. Using // makes it much easier to comment out a block of code while debugging.


```cpp
// this function does something
int myFunction()
{
}
```

To comment out this function block during debugging we might do:

```cpp
// this function does something
// this function does something
int myFunction()
{
}
```

## Include Guards

Header files must contain an distinctly named include guard to avoid problems with including the same header multiple times or conflicting with other headers from other projects

```cpp
#ifndef MYPROJECT_MYCLASS_H
#define MYPROJECT_MYCLASS_H

class MyClass
{
};

#endif
```

## 4 spaces indent level.

Tabs are not allowed, and a mixture of tabs and spaces is strictly forbidden. Modern autoindenting IDEs and editors require a consistent standard to be set.

```cpp
// Good Idea
int myFunction(bool p_b)
{
    if (p_b)
    {
        // do something
    }
}
```

## {} are required for everthing.
Leaving them off can lead to semantic errors in the code.

```cpp
// Bad Idea
// this compiles and does what you want, but can lead to confusing
// errors if close attention is not paid.
for (int i = 0; i < 15; ++i)
    cout << i << "\n";

// Bad Idea
// the cout is not part of the loop in this case even though it appears to be
int sum = 0;
for (int i = 0; i < 15; ++i)
    ++sum;
    cout << i << "\n";


// Good Idea
// It's clear which statements are part of the loop (or if block, or whatever)
int sum = 0;
for (int i = 0; i < 15; ++i)
{
    ++sum;
    cout << i << "\n";
}
```

## Prefer pre-increment to post-increment
... when it is semantically correct. Pre-increment is faster then post-increment because it does not require a copy of the object to be made.

```cpp
// Bad Idea
for (int i = 0; i < 15; i++)
{
    cout << i << "\n";
}

// Good Idea
for (int i = 0; i < 15; ++i)
{
    cout << i << "\n";
}
```

## Keep lines a reasonable length

```cpp
// Bad Idea
// hard to follow
if (x && y && myFunctionThatReturnsBool() && caseNumber3 && (15 > 12 || 2 < 3))
{
}

// Good Idea
// Logical grouping, easier to read
if (x && y && myFunctionThatReturnsBool()
    && caseNumber3
    && (15 > 12 || 2 < 3))
{
}
```

## Use "" For Including Local Files
... `<>` is [reserved for system includes](http://blog2.emptycrate.com/content/when-use-include-verses-include).

```cpp
// Bad Idea. Requires extra -I directives to the compiler
// and goes against standards
#include <string>
#include <includes/MyHeader.h>

// Worse Idea
// requires potentially even more specific -I directives and
// makes code more difficult to package and distribute
#include <string>
#include <MyHeader.h>


// Good Idea
// requires no extra params and notifies the user that the file
// is a local file
#include <string>
#include "MyHeader.h"
```

## Initialize Member Variables
...with the member initializer list

```cpp
// Bad Idea
class MyClass
{
private:
    int m_value;
public:
    MyClass(int p_value)
    {
    m_value = p_value;
    }
};

// Good Idea
// C++'s member initializer list is unique to the language and leads to
// cleaner code and potential performance gains that other languages cannot
// match
class MyClass
{
private:
    int m_value;
public:
    MyClass(int p_value)
    : m_value{p_value}
    {
    }
};
```

## Forward Declare when Possible

instead of:
```cpp
// some header file
class MyClass;

void doSomething(const MyClass &);
```

This:

```cpp
// some header file
#include "MyClass.h"

void doSomething(const MyClass &);
```

This is a proactive approach to simplify compilation time and rebuilding dependencies.

## Always Use Namespaces

There is almost never a reason to declare an identifier in the global namespaces. Instead, functions and classes should exist in an appropriately named namespaces or in a class inside of a namespace. Identifiers which are placed in the global namespace risk conflicting with identifiers from other (mostly C, which doesn't have namespaces) libraries.

## Avoid Compiler Macros

Compiler definitions and macros are replaced by the pre-processor before the compiler is ever run. This can make debugging very difficult because the debugger doesn't know where the source came from.

```cpp
// Good Idea
using namespace std;
cout << "hello" << "\n";
MyClass::myFunction();
string myFunction();

// Bad Idea
namespace my_project {
    class Constants {
    public:
        static const double PI = 3.14159;
    };
}
```

## Use Exceptions Instead of Return Values to Indicate Error

Exceptions cannot be ignored. Return values, such as using `boost::optional`, can be ignored and if not checked can cause crashes or memory errors. An exception, on the other hand, can be caught and handled. Potentially all the way up the highest level of the application with a log and automatic restart of the application.

Stroustrup, the original designer of C++, [makes this point](http://www.stroustrup.com/bs_faq2.html#exceptions-why) much better than I ever could.

## Avoid global data
... this includes singleton objects

Global data leads to unintended sideeffects between functions and can make code difficult or impossible to parallelize. Even if the code is not intended today for parallelization, there is no reason to make it impossible for the future.

## Const as much as possible
`const` tells the compiler that a variable or method is immutable. This helps the compiler optimize the code and helps the developer know if a function side effects. Also, using `const &` prevents the compiler from copying data unnecessarily. [Here](http://kotaku.com/454293019) are some comments on const from John Carmack.

```cpp
// Bad Idea
using namespace std;
class MyClass
{
private:
    string m_value;
public:
    MyClass(string p_value)
    : m_value{p_value}
    {
    }

    string get_value()
    {
        return m_value;
    }
};


// Good Idea
using namespace std;
class MyClass
{
private:
    string m_value;
public:
  MyClass(const string &t_value)
    : m_value{t_value}
  {
  }

  string get_value() const
  {
    return m_value;
  }
};
```
## Spaces inside parenthesis, brackets and braces to separate variables from operators
```cpp
// Bad Idea
if( aux == true )
{
}
// Good Idea
if(aux == true)
{
}
```
## Declarations of pointers and references

```cpp
// Bad Idea
char* a;
char * a;
myFunction(char* a);
myFunction(string& a);
String& myFunction();

// Good Idea
char *a;
myFunction(char *a);
myFunction(string &a);
String &myFunction();
```

```cpp
// Bad Idea
while(aux)
// Good Idea
while(aux != nullptr)
```

## Space between functions

```cpp
// Bad Idea
myFunction1()
{
}
myFunction2()
{
}

// Good Idea
myFunction1()
{
}

myFunction2()
{
}
```

## Space between parameters or values of the functions, arrays and brackets.

```cpp
// Bad Idea
myFunction1(int a , int b);
myFunction2(300 , 500 , 200);
int a[] = {16 , 2 , 77};
Myclass myclass{200 , 500};
// Good Idea
myFunction1(int a, int b);
myFunction2(300, 500, 200);
int a[] = {16, 2, 77};
Myclass myclass{200, 500};
```

## Prefer Stack Operations to Heap Operations

Heap operations have performance penalties in mulithreaded environments on most platforms and can possibly lead to memory errors if not used carefully.

Modern C++11 has special move operations which are designed to enhances the performance of stack based data by reducing or eliminating copies, which can bring even the single threaded case on par with heap based operations.

## References and Further Reading


 * http://geosoft.no/development/cppstyle.html
 * http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml (Note that google's standard document makes several recommendations which we will NOT be following. For example, they explicitly forbid the use of exceptions, which makes [RAII](http://blog2.emptycrate.com/content/nobody-understands-c-part-2-raii) impossible.)
 * http://www.parashift.com/c++-faq/
 * http://www.cplusplus.com/
 * http://sourceforge.net/apps/mediawiki/cppcheck/index.php?title=ListOfChecks
 * http://emptycrate.com/
 * https://gist.github.com/lefticus/10191322 (thanks @lefticus)
