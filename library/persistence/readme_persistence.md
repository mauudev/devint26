USO DE PERSISTENCIA
===================
Libs/persistence
* Revisar CMakeLists del proyecto persist para su importacion
* Eliminar los archivos con el nombre: /include/IDTO.h y /source/Persistence.cpp debido a los conflictos

IMPORTAR LIBRERIA
------------------
En CMakeLists incluir:

INCLUDE_DIRECTORIES(library/persistence/include)

ADD_SUBDIRECTORY(library/persistence)


TARGET_LINK_LIBRARIES(nameProject persistencelib)

IDTO
----
* Para utilizar IDTO las clases a ser utilizadas deben incluir metodos tipo SETTERS (ej: void setYear(int year) - Modificadores) y GETTERS (ej: int getYear() - para obtener los atributos).
* Para realizar las clases que realizaran lo de **serialize** (transforma la clase a notacion JSON) y **deserialize** (recupera informacion del JSON) deben tener en el **archivo.h** lo siguiente:
```
    #include <json.hpp>
    #include <IDTO.h>

    using Json = nlohmann::json;
    class Usuario:public IDTO 
    ...
```

* En el **archivo.cpp** los metodos serialize y deserialize deben ser implementados de manera personalizada para cada clase:
```
    Json Institucion::serialize()
    {
        Json json;
        json[NOMBREINSTITUCION]= _nombreInstitucion;            // json[nombre_atr1] = atr1;
            for(int i = 0; i < _usuarios.size(); i++)          // En caso de que el atributo sea vector
            {
                json[USUARIOS][i] = _usuarios[i].serialize();	// La clase Usuario sabe realizar su serialize
            }
        return json;
    }
    void Institucion::deserialize(Json &json)
    {
        vector<Usuario>aux;
        this->setNombreInstitucion(json[NOMBREINSTITUCION]);
            for(int i = 0; i < json[USUARIOS].size(); i++)
            {
                Usuario u;
                u.deserialize(json[USUARIOS][i]);
                aux.push_back(u);
            }
            this->setUsuarios(aux);
    }
```

Persistence
-----------
* En caso de tener una clase el la que se concentran todas las clases, de las cuales se realizaran persistencia, se tiene en **archivo.h** :

```
    #include <Persistence.h>
    
    class Institucion:public Persistence    // Persistence ya trae consigo el IDTO, no es necesario volver a importar para el serialize y deserialize

```

MAIN
----
* Para utilizar el **save()** (exporta o lo guarda en un Json) y **load()** (importa o carga desde un Json), es opcional modificar el **fileName** nombre del archivo.

* Utilizar los metodos:

```
    //Exportar a Json
    Institucion *i1;                    // Puntero a Objeto del tipo Institucion
    i1->setFileName("Institucion.json");// Modifica el nombre de archivo del json puede ser miArchivo.json o /home/carpeta/miArchivo.json
    i1->save();                         // Guarda la informacion en el "Institucion.json"

    i1->printDatos();                   // Para verificar los datos y comparar con el archivo.json

    // Importar a Json
    Institucion i2;                     // Objeto del tipo Institucion
    i2.setFileName("Institucion.json");	// Modifica el nombre de archivo del json 	
    i2.load();                          // Carga el JSONfile "Institucion.json" de donde recuperara informacion

    i2.printDatos();                    // Para verificar los datos y comparar con el archivo.json
```

TEST
----
* Revisar los /test/test.cpp para mejor comprension

EJECUCION DE LA PRUEBA
----------------------
* Ejecutar los siguientes comandos para ejecutar las pruebas

```
mkdir bin
cd bin
cmake ..
make
./persist

```

* Finalmente dentro del bin, en caso de que todo se ejecute de manera correcta se puede observar un .json, donde se encuentra exportada la informacion

Autores
-------
* Agueda Villca (agueda.villca@funcacion-jala.org)
* Sandy Frida Garcia Cusi (sandy.garcia@fundacion-jala.org)