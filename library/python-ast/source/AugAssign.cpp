#include <AugAssign.h>

AugAssign::AugAssign()
{
	
}

AugAssign :: AugAssign(Expression* target, Operator op, Expression* value)
{
	_target = target;
	_op = op;
	_value = value;
}

string AugAssign ::getOp()
{
	return _operators[_op];
}

Expression* AugAssign :: getTarget()
{
	return _target;
}

Expression* AugAssign :: getValue()
{
	return _value;
}

string AugAssign :: toString()
{
	return "AugAssign";
}

void AugAssign :: accept(Visitor& visitor)
{
	visitor.visit(this);
}