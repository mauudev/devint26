#include <Return.h>

const string Return::_keyWord = "return";

Return::Return()
{

}

Return::Return(Expression* value)
{
	_value = value;
}

string Return::getPalabraReservada()
{
	return _keyWord;
}

Expression* Return::getValue()
{
	return _value;
}

string Return::toString()
{
	return _keyWord;
}

void Return::accept(Visitor& visitor)
{
	visitor.visit(this);
}