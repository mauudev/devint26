#include <ClassDef.h>

ClassDef::ClassDef(){}

ClassDef::ClassDef(string funcName)
{
	_nameFunction = funcName;
	_isEmpty = true;
}

ClassDef& ClassDef::addParameter(Expression* parameter)
{
	_paramList.addParameter(parameter);
	return *this;
}

ClassDef& ClassDef::addBodyNode(Statement* statementNode)
{
 	_body.push_back(statementNode);
 	return *this;
}

ClassDef& ClassDef::addBodyNode(Expression* expressionNode)
{
 	_body.push_back(expressionNode);
 	return *this;
}

string ClassDef::toString() 
{
	return "";
}

void ClassDef:: accept(Visitor& visitor) 
{
  visitor.visit(this);
}

int ClassDef::SizeBodyList()
{
  return _body.size();
}

vector<NodeAST*> ClassDef::GetBodyList()
{
  return _body;
}

void ClassDef::addParameters(ParameterList paramList)
{
  this->_paramList = paramList;
}

string ClassDef::NoBody() 
{
	if(_isEmpty) return " pass";
	else return "";
}

CallFunction ClassDef::Call()
{
	CallFunction call{*this};
	return call;
}

CallFunction ClassDef::Call(Expression* func)
{
	CallFunction call{*this,func};
	return call;
}

string ClassDef::getName()
{
	return _nameFunction;
}

ParameterList& ClassDef::getParameters()
{
	return _paramList;
}