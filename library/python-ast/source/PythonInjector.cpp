#include <PythonInjector.h>

using namespace std;


PythonInjector::PythonInjector(string text)
{
	_pythonCode = text;
  splitLines();
}

void PythonInjector::splitLines()
{
  int size = _pythonCode.size();
  string cont="";
  for(int i=0; i<size;i++)
  {
    cont +=  _pythonCode[i];
    if(_pythonCode[i]=='\n' or (i==(size-1))) 
    {
      string ns = cont;
      lines.push_back(ns);
      cont="";
    }
  }
}

int PythonInjector::getNumberLines()
{
	return _numberLines;
}

vector<string> PythonInjector::getLines()
{
	return lines;
}

string PythonInjector::getCode()
{
  return _pythonCode;
}

string PythonInjector::toString()
{
  return "PythonInjector";
}
  
void PythonInjector::accept(Visitor &visitor)
{
  visitor.visit(this);
}
