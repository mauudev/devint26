#include <Module.h>

Module::Module()
{
	
}

Module::Module(Expression* node)
{
	_body.push_back(node);
}	

Module::Module(Statement* node)
{
	_body.push_back(node);
}

Module& Module::addBody(Expression* node)
{
	_body.push_back(node);
	return *this;
}

Module& Module::addBody(Statement* node)
{
	_body.push_back(node);
	return *this;
}

string Module::toString()
{
	return "Module";
}

vector<NodeAST*> Module::getBody()
{
	return _body;
}

void Module::accept(Visitor& visitor)
{
	visitor.visit(this);
}