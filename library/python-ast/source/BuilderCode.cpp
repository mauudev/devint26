#include <BuilderCode.h>
#include <typeinfo>

BuilderCode::BuilderCode()
{
  _code = "";
}
void BuilderCode::visit(BinaryOperator* binaryOperator)
{
  visitChildNodes(binaryOperator);
}

void BuilderCode::visit(BoolOp* boolOperator)
{
  visitChildNodes(boolOperator);
}

void BuilderCode::visit(Number* number)
{
  _code +=(number->toString());
}

void BuilderCode::visit(Assignment* assing)
{
  visitChildNodes(assing);
}

void BuilderCode::visit(StringExp* str)
{
  _code += "\"";
  _code += str->GetString();
  _code += "\"";
}

void BuilderCode::visitElse(vector<NodeAST*> bodyElse)
{
  _tab++;
  for(Node* childNode : bodyElse)
  {
      _code += "\n";
      _code += generateTabs();
      childNode->accept(*this);
  }
  _tab--;
}

void BuilderCode::visit(While* w)
{
  _code += "while ";
  w->getExprBool()->accept(*this);
  _code +=":";
  visitChildNodes(w);
  if(w->hasBodyElse())
  {
    _code += "\n";
    _code += generateTabs();
    _code +="else :";
    visitElse(w->getBodyElse());
  }
}

void BuilderCode::visit(For* f)
{
  _code += "for ";
  f->getExprAssignment()->accept(*this);
  _code += f->getReservedWord();
  f->getExprIterable()->accept(*this);
  _code +=" :";
  visitChildNodes(f);
  if(f->hasBodyElse())
  {
    _code += "\n";
    _code += generateTabs();
    _code +="else :";
    visitElse(f->getBodyElse());
  }
}

void BuilderCode::visit(FunctionDef* funDef)
{
  _code +="def "+funDef->getName()+"(";
  visit(&funDef->getParameters());
  _code += "):";
  if(funDef->SizeBodyList()>0)
  {
    visitChildNodes(funDef);
  }
  else
  {
    _code += funDef->NoBody();
  }
}

void BuilderCode::visit(ClassDef* classDef)
{
  _code +="class "+classDef->getName()+"(";
  visit(&classDef->getParameters());
  _code += "):";
  if(classDef->SizeBodyList()>0)
  {
    visitChildNodes(classDef);
  }
  else
  {
    _code += classDef->NoBody();
  }
}

void BuilderCode::visit(Concat* concat)
{
  if(concat->SizeBodyList()>0)
  {
    visitChildNodes(concat);
  }
  else
  {
    _code += concat->NoBody();
  }
}

void BuilderCode::visit(CallFunction* callFunction)
{
  if(callFunction->GetFunc()) callFunction->GetFunc()->accept(*this);
  if(callFunction->GetFunc()&&callFunction->GetFunction()) _code+=".";
  if(callFunction->GetFunction())
    _code += callFunction->toString();
  _code += "(";
  visitChildNodes(callFunction->GetArguments().getChildList());
  if(callFunction->GetKeyWords().getChildList().size()>0)
    callFunction->GetArguments().getChildList().size() > 0? _code+=", ":""
  ,visitChildNodes(callFunction->GetKeyWords().getChildList());
  _code += ")";
}

void BuilderCode::visit(ParameterList* parameterList)
{
  visitChildNodes(parameterList);
}

void BuilderCode::visit(Print* print)
{
  _code += "print";
  _code += print->GetChevron();
  if(print->sizeChildList()>0)
  {
    visitChildNodes(print);
  }
}

void BuilderCode::visit(Import* import)
{
  _code += "import ";
  for(pair<string, string > p : import->getImportList())
  {
    if(p.second == "")
    {
      _code+=p.first;
    }else
    {
      _code+=p.first;
      _code+=" as ";
      _code+=p.second;
    }
    _code+=", ";
  }
  _code.pop_back();
  _code.pop_back();
}

void BuilderCode::visit(ImportFrom* importFrom)
{
  for(auto import : importFrom-> getImportList())
  {
    _code += "from "+import.relativeModule;
    _code += " import "+import.identifier;
    if(import.name !="")
    {
      _code += " as "+import.name;

    }
    _code+="\n";
  }
  for(auto importAll : importFrom-> getImportListAll())
  {
    _code += "from "+importAll+" import *";
    _code+="\n";
  }
}

void BuilderCode::visit(Return* ret)
{
  _code+=ret->toString();
  _code += " ";
  ret->getValue()->accept(*this);
  _code += "\n";
}

void BuilderCode::visit(PythonInjector* pi)
{
  for(string  line : pi->getLines())
  {

    if(_code[_code.size()-1] != '\t')
    {
      if(_code[_code.size()-1] == '\n')
      _code += generateTabs();
    }
    _code += line;
  }
}

void BuilderCode::visit(Dictionary* dict)
{
  _code += "{";
  if(dict->sizeKeyList()>0)
  {
    visitChildNodes(dict);
  }
  _code+=" }";
}

void BuilderCode::visit(List* list)
{
  _code += "[";
  if(list->sizeChildList()>0)
  {
    visitChildNodes(list);
  }
  _code += "]";
}

void BuilderCode::visit(Name* name)
{
  _code += name->toString();
}

void BuilderCode::visit(InsertFun* insertFun)
{
  for(string  line : insertFun->getLines())
  {

    if(_code[_code.size()-1] != '\t')
    {
      if(_code[_code.size()-1] == '\n')
      _code += generateTabs();
    }
    _code += line;
  }
}

void BuilderCode::visit(Attribute* attribute)
{
  attribute->GetValue()->accept(*this);
  _code += "." + attribute->toString();

}

void BuilderCode::visit(KeyWord* keyWord)
{
  _code+=keyWord->toString();
  _code+="=";
  keyWord->GetValue()->accept(*this);
}

void BuilderCode::visit(Slice* slice)
{
  slice->GetLower()->accept(*this);
  _code+=" : ";
  slice->GetUpper()->accept(*this);
  if(slice->GetStep())
  {
    _code+=" : ";
    slice->GetStep()->accept(*this);
  }
}

void BuilderCode::visit(Index* index)
{
  index->GetValue()->accept(*this);
}

void BuilderCode::visit(ExtSlice* slice)
{
  visitChildNodes(slice);
}

void BuilderCode::visit(UnaryOp* unaryOp)
{
  _code += unaryOp->getUnaryOp();
  _code += " ";
  unaryOp->getOperand()->accept(*this);
}

void BuilderCode::visit(Subscript* subscript)
{
  subscript->GetValue()->accept(*this);
  _code+="[";
  subscript->GetSlice()->accept(*this);
  _code+="]";
}

void BuilderCode::visit(Compare* compare)
{
  compare->getLeft()->accept(*this);
  int i;
  vector<Expression*> comparators=compare->getComparators();
  vector<cmpOp> ops=compare->getOps();
  for(i=0; i<ops.size(); i++)
  {
    _code+=" ";
    _code+=compare->getCmpOpName(ops[i]);
    _code+=" ";
    comparators[i]->accept(*this);
  }
}

void BuilderCode::visit(Break* brk)
{
  _code += brk->toString();
}

void BuilderCode::visit(Module* module)
{
  for(NodeAST* childNode : module->getBody())
  {
    childNode->accept(*this);
    _code += "\n";
  }
  _code.pop_back();
}

void BuilderCode::visit(Global* global)
{
  _code += "global ";
  for(Name* name : global->getList())
  {
    name->accept(*this);
    _code += " ,";
  }
  _code.pop_back();
  _code.pop_back();
}

void BuilderCode::visit(Tuple* tuple)
{
  visitChildNodes(tuple->getChildList());
}

void BuilderCode::visit(AugAssign* aug)
{
  aug->getTarget()->accept(*this);
  _code += " "+aug->getOp()+" ";
  aug->getValue()->accept(*this);
  _code += " ";
}

string BuilderCode::getCode()
{
  return _code;
}

void BuilderCode:: clearCode()
{
  _code = "";
}

void BuilderCode::visitChildNodes(BinaryOperator* binaryOperator)
{
  _code += "(";
  binaryOperator->getLeft()->accept(*this);
  _code += binaryOperator->getBinOpe();
  binaryOperator->getRight()->accept(*this);
  _code += ")";
}

void BuilderCode:: visitChildNodes(Assignment* assing)
{
  visitChildNodes(assing->getChildList());
  _code += assing->getEquals();
  assing->GetExpressionList()->accept(*this);
}

void BuilderCode::visitChildNodes(BoolOp* boolOp)
{
  boolOp->getValueLeft()->accept(*this);
  _code +=" ";
  _code += boolOp->getBool();
  _code +=" ";
  boolOp->getValueRight()->accept(*this);
}

void BuilderCode:: visitChildNodes(FunctionDef* functionDef)
{
  _tab++;
  for(Node* childNode : functionDef->GetBodyList())
  {
      _code += "\n";
      _code += generateTabs();
      childNode->accept(*this);
  }
  _tab--;
}

void BuilderCode:: visitChildNodes(ClassDef* classDef)
{
  _tab++;
  for(Node* childNode : classDef->GetBodyList())
  {
      _code += "\n";
      _code += generateTabs();
      childNode->accept(*this);
  }
  _tab--;
}

void BuilderCode:: visitChildNodes(Concat* concat)
{
  for(Node* childNode : concat->GetBodyList())
  {
      _code += "\n";
      _code += generateTabs();
      childNode->accept(*this);
  }
}

void BuilderCode::visitChildNodes(ParameterList* parameterList)
{
  for(Node* childNode : parameterList->getChildList())
  {
      childNode->accept(*this);
      _code += ", ";
  }
  if(parameterList->getChildList().size()>0)_code.pop_back(),_code.pop_back();
}

void BuilderCode::visitChildNodes(While* w)
{
  _tab++;
  for(Node* childNode : w->getChildList())
  {
      _code += "\n";
      _code += generateTabs();
      childNode->accept(*this);
  }
  _tab--;
}

void BuilderCode::visitChildNodes(For* f)
{
  _tab++;
  for(Node* childNode : f->getChildList())
  {
      _code += "\n";
      _code += generateTabs();
      childNode->accept(*this);
  }
  _tab--;
}

void BuilderCode::visit(If* ifDef)
{
  _code += "if ";
  ifDef->GetExpression()->accept(*this);
  _code += ":";
  visitChildNodes(ifDef);
}

void BuilderCode::visit(With* with)
{
  _code += "with ";
  for(pair<Node*, string > p: with->GetExprList())
  {
    if(p.second == "")
    {
      p.first->accept(*this);
    }else
    {
      p.first->accept(*this);
      _code+=" as ";
      _code+=p.second;
    }
    _code += ", ";
  }
  _code.pop_back();
  _code.pop_back();
  _code += ":";
  visitChildNodes(with);
}

void BuilderCode::visitChildNodes(With* with)
{
  _tab++;
  for(Node* childNode : with->getChildList())
  {
      _code += "\n";
      _code += generateTabs();
      childNode->accept(*this);
  }
  _tab--;
}

void BuilderCode:: visitChildNodes(If* ifDef)
{
  bool orelse = false;
  _tab++;
  for(Node* childNode : ifDef->GetBodyList())
  {
      _code += "\n";
      _code += generateTabs();
      childNode->accept(*this);
  }
  _tab--;
  for (Node* childNode : ifDef->GetElif())
  {
    If* aux = dynamic_cast<If*>(childNode);
    if(aux)
    {
      _code += "\n";
      _code += generateTabs();
      _code += "elif ";
      aux->GetExpression()->accept(*this);
      _code += ":";
      visitChildNodes(aux);
    }
    else
    {
      _code += "\n";
      if(!orelse)
      {
        _code += generateTabs();
        _code += "else:\n";
        orelse = true;
      }
      _tab++;
      _code += generateTabs();
      childNode->accept(*this);
      _tab--;
    }
  }
}

void BuilderCode::visitChildNodes(Print* print)
{
  for(Node* childNode : print->getChildList())
  {
      _code += " ";
      childNode->accept(*this);
      _code += ",";
  }
  if(print->GetParameterSize() > 0)
  {
      _code.pop_back();
      _code += " %(";
      print->GetParameters(*this);
      _code += ") ";
  }
  if(print->GetNewLine())
      _code.pop_back();
}

void BuilderCode::visitChildNodes(vector<NodeAST*> list)
{
  for(Node* childNode: list)
  {
    childNode->accept(*this);
    _code +=", ";
  }
  if(list.size()>0) _code.pop_back(),_code.pop_back();
}

void BuilderCode::visitChildNodes(Dictionary* dict)
{
  for(pair<Expression*, Expression*> p : dict->GetKeyList())
  {
    p.first->accept(*this);
    _code+=" : ";
    p.second->accept(*this);
    _code+=",";
  }
  _code.pop_back();
}

void BuilderCode::visitChildNodes(ExtSlice* slice)
{
  for(Node* childNode : slice->getChildList())
  {
      childNode->accept(*this);
      _code += ", ";
  }
  _code.pop_back();
  _code.pop_back();
}

string BuilderCode::generateTabs()
{
  string t = "";
  for(int i=0; i<_tab; i++)
  {
    t += "\t";
  }
  return t;
}

string BuilderCode::generateTab()
{
  string t = "";
  for(int i=0; i<_tab-1; i++)
  {
    t += "\t";
  }
  return t;
}

void BuilderCode::visitChildNodes(List* list)
{
  for(Node* childNode : list->getChildList())
  {
      childNode->accept(*this);
      _code += ", ";
  }
      _code.pop_back();
      _code.pop_back();
}
