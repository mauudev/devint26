#include <NodeAST.h>

NodeAST::NodeAST()
{
}
vector<NodeAST*> NodeAST::getChildList()
{
  return list;
}

void NodeAST::addChild(class NodeAST* node)
{
  list.push_back(node);
}

int NodeAST:: sizeChildList()
{
  return list.size();
}

void NodeAST:: printChildList()
{
  for(Node* node : list){
      cout<< node->toString() <<endl;
  }
}
