#include <KeyWord.h>

KeyWord::KeyWord(){}

KeyWord::KeyWord(const string& arg, Expression* value)
:_arg(arg),_value(value){}

string KeyWord::toString()
{
	return _arg;
}

Expression* KeyWord::GetValue()
{
	return _value;
}

void KeyWord::accept(Visitor& visitor)
{
	visitor.visit(this);
}