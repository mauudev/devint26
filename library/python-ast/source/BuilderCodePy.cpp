#include <BuilderCodePy.h>

Name& BuilderCodePy::name(string name)
{
	string key=keyGen.generateKey();
	_nameMap[key]=Name(name);
	return _nameMap[key];
}

InsertFun& BuilderCodePy::ins(string insertFun)
{
	string key=keyGen.generateKey();
	_insertFunMap[key]=InsertFun(insertFun);
	return _insertFunMap[key];
}

Number& BuilderCodePy::num(int val)
{
	string key=keyGen.generateKey();
	_numMap[key]=Number(val);
	return _numMap[key];
}

Number& BuilderCodePy::num(double val)
{
	string key=keyGen.generateKey();
	_numMap[key]=Number(val);
	return _numMap[key];
}

Number& BuilderCodePy::num(float val)
{
	string key=keyGen.generateKey();
	_numMap[key]=Number(val);
	return _numMap[key];
}

Assignment& BuilderCodePy::assign(Expression* l, Expression* r)
{
	string key=keyGen.generateKey();
	_assignMap[key]=Assignment(l,r);
	return _assignMap[key];
}

FunctionDef& BuilderCodePy::functionDef(string name)
{
	string key=keyGen.generateKey();
	_funcMap[key]=FunctionDef(name);
	return _funcMap[key];
}

ClassDef& BuilderCodePy::classDef(string name)
{
	string key=keyGen.generateKey();
	_classMap[key]=ClassDef(name);
	return _classMap[key];
}

Concat& BuilderCodePy::concat()
{
	string key=keyGen.generateKey();
	_concatMap[key]=Concat();
	return _concatMap[key];
}

For& BuilderCodePy::forStmt(Expression* e, Expression* eIt)
{
	string key=keyGen.generateKey();
	_forMap[key]=For(e,eIt);
	return _forMap[key];
}

List& BuilderCodePy::list()
{
	string key=keyGen.generateKey();
	_listMap[key]=List();
	return _listMap[key];
}

List& BuilderCodePy::list(Expression* e)
{
	string key=keyGen.generateKey();
	_listMap[key]=List(e);
	return _listMap[key];
}

Attribute& BuilderCodePy::attribute(Expression* v, string a)
{
	string key=keyGen.generateKey();
	_attrMap[key]=Attribute(v,a);
	return _attrMap[key];
}

CallFunction& BuilderCodePy::call(Expression* func)
{
	string key=keyGen.generateKey();
	_callMap[key]=CallFunction(func);
	return _callMap[key];
}

Subscript& BuilderCodePy::subscript(Expression* v, SliceType* s)
{
	string key=keyGen.generateKey();
	_subsMap[key]=Subscript(v,s);
	return _subsMap[key];
}

ExtSlice& BuilderCodePy::extSlice(SliceType* s)
{
	string key=keyGen.generateKey();
	_extSliceMap[key]=ExtSlice(s);
	return _extSliceMap[key];
}

Slice& BuilderCodePy::slice(Expression* l, Expression* u)
{
	string key=keyGen.generateKey();
	_sliceMap[key]=Slice(l, u);
	return _sliceMap[key];
}

Index& BuilderCodePy::index(Expression* v)
{
	string key=keyGen.generateKey();
	_indexMap[key]=Index(v);
	return _indexMap[key];
}

Return& BuilderCodePy::returnStmt(Expression* e)
{
	string key=keyGen.generateKey();
	_retMap[key]=Return(e);
	return _retMap[key];
}

If& BuilderCodePy::ifStmt(Expression* e)
{
	string key=keyGen.generateKey();
	_ifMap[key]=If(e);
	return _ifMap[key];
}

Print& BuilderCodePy::print()
{
	string key=keyGen.generateKey();
	_printMap[key]=Print();
	return _printMap[key];
}

Print& BuilderCodePy::print(Expression* e, Expression* n)
{
	string key=keyGen.generateKey();
	_printMap[key]=Print(e, n);
	return _printMap[key];
}

BoolOp& BuilderCodePy::boolOp(Expression* l, Bool op, Expression* r)
{
	string key=keyGen.generateKey();
	_boolOpMap[key]=BoolOp(l,op,r);
	return _boolOpMap[key];
}

BinaryOperator& BuilderCodePy::binOp(Expression* l, Operator op, Expression* r)
{
	string key=keyGen.generateKey();
	_binOpMap[key]=BinaryOperator(l,op,r);
	return _binOpMap[key];
}

UnaryOp& BuilderCodePy::unaryOp(UnaryOpType u, Expression* o)
{
	string key=keyGen.generateKey();
	_unaryOpMap[key]=UnaryOp(u,o);
	return _unaryOpMap[key];
}

Compare& BuilderCodePy::compare(Expression* l, cmpOp op, Expression* c)
{
	string key=keyGen.generateKey();
	_compareMap[key]=Compare(l,op,c);
	return _compareMap[key];
}

KeyWord& BuilderCodePy::keyword(const string& a, Expression* v)
{
	string key=keyGen.generateKey();
	_keyWordMap[key]= KeyWord(a,v);
	return _keyWordMap[key];	
}

Break& BuilderCodePy::breakStmt()
{
	string key=keyGen.generateKey();
	_breakMap[key]=Break();
	return _breakMap[key];
}

Dictionary& BuilderCodePy::dict(Expression* k, Expression* v)
{
	string key=keyGen.generateKey();
	_dictMap[key]=Dictionary(k, v);
	return _dictMap[key];
}

StringExp& BuilderCodePy::str(string str)
{
	string key=keyGen.generateKey();
	_strMap[key]=StringExp(str);
	return _strMap[key];
}

Tuple& BuilderCodePy::tuple(Expression* exprOne, Expression* exprTwo)
{
	string key=keyGen.generateKey();
	_tupleMap[key]=Tuple(exprOne, exprTwo);
	return _tupleMap[key];
}

While& BuilderCodePy::whileStmt(Expression* test)
{
	string key=keyGen.generateKey();
	_whileMap[key]=While(test);
	return _whileMap[key];
}

With& BuilderCodePy::withStmt(Expression* e, string a)
{
	string key=keyGen.generateKey();
	_withMap[key]=With(e, a);
	return _withMap[key];
}

With& BuilderCodePy::withStmt(Expression* e)
{
	string key=keyGen.generateKey();
	_withMap[key]=With(e);
	return _withMap[key];
}

Import&	BuilderCodePy::import(string m, string a)
{
	string key=keyGen.generateKey();
	_importMap[key]=Import(m,a);
	return _importMap[key];
}

Import& BuilderCodePy::import(string m)
{
	string key=keyGen.generateKey();
	_importMap[key]=Import	(m);
	return _importMap[key];
}

ImportFrom& BuilderCodePy::importFrom(string m)
{
	string key=keyGen.generateKey();
	_importFromMap[key]=ImportFrom(m);
	return _importFromMap[key];
}

ImportFrom& BuilderCodePy::importFrom(string rm, string i)
{
	string key=keyGen.generateKey();
	_importFromMap[key]=ImportFrom(rm, i);
	return _importFromMap[key];
}

ImportFrom& BuilderCodePy::importFrom(string rm, string i, string a)
{
	string key=keyGen.generateKey();
	_importFromMap[key]=ImportFrom(rm, i, a);
	return _importFromMap[key];
}

Global& BuilderCodePy::global(Name* n)
{
	string key=keyGen.generateKey();
	_globalMap[key]=Global(n);
	return _globalMap[key];
}

AugAssign& BuilderCodePy::augAssign(Expression* t, Operator o, Expression* v)
{
	string key=keyGen.generateKey();
	_augAssignMap[key]=AugAssign(t,o,v);
	return _augAssignMap[key];
}

BuilderCodePy& BuilderCodePy::addCode(Statement* s)
{
	_mod.addBody(s);
	return *this;
}

BuilderCodePy& BuilderCodePy::addCode(Expression* e)
{
	_mod.addBody(e);
	return *this;
}

string BuilderCodePy::generateCode()
{
	visitor.visit(&_mod);
	return visitor.getCode();
}

string BuilderCodePy::generateCode(Statement* s)
{
	_mod.addBody(s);
	visitor.visit(&_mod);
	return visitor.getCode();
}

string BuilderCodePy::generateCode(Expression* e)
{
	_mod.addBody(e);
	visitor.visit(&_mod);
	return visitor.getCode();
}
