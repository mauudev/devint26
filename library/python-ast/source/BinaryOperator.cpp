#include <BinaryOperator.h>

BinaryOperator::BinaryOperator()
{}

BinaryOperator::BinaryOperator(Expression* left, Operator op, Expression* right)
{
    _left = left;
    _right = right;
    returnOp(op);
}

void BinaryOperator::returnOp(Operator op)
{
    switch(op)
    {
        case ADD : _op = "+"; break;
        case SUB : _op = "-"; break;
        case MULT : _op = "*"; break;
        case DIV : _op = "/"; break;
        case MOD : _op = "%"; break;
        case POW : _op = "**"; break;
    }
}

string BinaryOperator::getBinOpe()
{
    return _op;
}

Expression* BinaryOperator::getLeft()
{
  return _left;
}

Expression * BinaryOperator::getRight()
{
  return _right;
}

string BinaryOperator::toString() 
{
  return _op;
}

void BinaryOperator:: accept(Visitor &visitor) 
{
    visitor.visit(this);
}