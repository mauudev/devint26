#include <With.h>

With::With()
{
		
}

With::With(Expression* expr, string alias)
{
	Add(expr,alias);
}

With::With(Expression* expr)
{
	Add(expr,"");
}

With& With::addExpr(Expression* expr, string alias)
{
	Add(expr,alias);
	return *this;
}

With& With::addExpr(Expression* expr)
{
	Add(expr,"");
	return *this;
}

With& With::addBody(Statement* statement)
{
	addChild(statement);
	return *this;
}

With& With::addBody(Expression* expression)
{
	addChild(expression);
	return *this;
}

vector<pair<Expression*, string>> With::GetExprList()
{
	return _exprList;
}

void With::Add(Expression* expr, string alias)
{
	pair <Expression*, string> e (expr,alias);
	_exprList.push_back(e);
}

string With::toString()
{
	return "With";
}

void With::accept(Visitor &visitor)
{
	visitor.visit(this);
}
