#include <Subscript.h>

Subscript::Subscript(){}


Subscript::Subscript(Expression* value, SliceType* slice)
{
	_value = value;
	_slice = slice;
}

Subscript& Subscript::SetValue(Expression* value)
{
	_value = value;
	return *this;
}

Subscript& Subscript::SetSlice(SliceType* slice)
{
	_slice = slice;
	return *this;
}

Expression* Subscript::GetValue()
{
	return _value;
}

SliceType* Subscript::GetSlice()
{
	return _slice;
}

string Subscript::toString() 
{

}

void Subscript::accept(Visitor &visitor) 
{
	visitor.visit(this);
}