#include <If.h>

If::If()
{
	
}

If::If(Expression* test)
{
	_test = test;
}

If& If::addIfNode(Statement* body)
{
 	_body.push_back(body);
 	return *this;
}

If& If::addIfNode(Expression* body)
{
 	_body.push_back(body);
 	return *this;
}

If& If::addElifNode(Statement* orelse)
{
	_orelse.push_back(orelse);
 	return *this;
}

If& If::addElifNode(Expression* orelse)
{
	_orelse.push_back(orelse);
 	return *this;
}

vector<NodeAST*> If::GetElif()
{
	return _orelse;
}

Expression* If::GetExpression()
{	
  	return _test;
} 

vector<NodeAST*> If::GetBodyList()
{
  return _body;
}

string If::toString() 
{

}

void If:: accept(Visitor& visitor) 
{
  visitor.visit(this);
}
