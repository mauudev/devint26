#include <Global.h>

Global::Global()
{
	
}

Global::Global(Name* name)
{
	addGlobal(name);
}

Global& Global::addGlobal(Name* name)
{
	_list.push_back(name);
	return *this;
}

vector<Name*> Global::getList()
{
	return _list;
}

string Global::toString()
{
	return "global";
}

void Global::accept(Visitor& visitor)
{
	visitor.visit(this);
}