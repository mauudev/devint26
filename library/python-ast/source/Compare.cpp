#include <Compare.h>

Compare::Compare(){}

Compare::Compare(Expression* left, vector<cmpOp> ops, vector<Expression*> comparators)
:_left(left),_ops(ops),_comparators(comparators)
{		

}

Compare::Compare(Expression* left, cmpOp op, Expression* comparator)
{
	_left=left;
	_ops.push_back(op);
	_comparators.push_back(comparator);
}

Expression* Compare::getLeft()
{
	return _left;
}

vector<cmpOp> Compare::getOps()
{
	return _ops;
}

vector<Expression*> Compare::getComparators()
{
	return _comparators;
}

string Compare::toString()
{

}

string Compare::getCmpOpName(cmpOp op)
{
	return cmpOpNames[op];
	/*string opValue;
	switch(op){
		case EQ:
		opValue=cmpOpValue[op];
		break;
		case NOTEQ:
		opValue=cmpOpValue[op];
		break;
		case LT:
		opValue=cmpOpValue[op];
		break;
		case LTE:
		opValue=cmpOpValue[op];
		break;
		case GT:
		opValue=cmpOpValue[op];
		break;
		case GTE:
		opValue=cmpOpValue[op];
		break;
		case IS:
		opValue=cmpOpValue[op];
		break;
		case ISNOT:
		opValue=cmpOpValue[op];
		break;
		case IN:
		opValue=cmpOpValue[op];
		break;
		case NOTIN:
		opValue=cmpOpValue[op];
		break;
	}
	return opValue;*/
}

void Compare::accept(Visitor& visitor)
{
	visitor.visit(this);
}