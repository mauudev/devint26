#include <BoolOp.h>

BoolOp::BoolOp(){}

BoolOp::BoolOp( Expression* left, Bool op, Expression* right)
{
  _op = op;
  _valueLeft = left;    
  _valueRight = right;      
}

Expression* BoolOp::getValueLeft()
{
  return _valueLeft;
}

string BoolOp::getBool()
{
  return BoolopExp[_op];
} 

Expression* BoolOp::getValueRight()
{
  return _valueRight;
}

string BoolOp::toString() 
{
  
}

void BoolOp::accept(Visitor& visitor) 
{
  	visitor.visit(this);
}
