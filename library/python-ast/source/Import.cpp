#include <Import.h>

Import::Import()
{
	
}

Import::Import(string module, string alias)
{
	addModule(module, alias);
}

Import::Import(string module)
{
	addModule(module, "");
}

Import& Import::addImport(string module)
{
	addModule(module, "");
	return *this;
}

Import& Import::addImport(string module, string alias)
{
	addModule(module, alias);
	return *this;
}

vector<pair<string, string>> Import::getImportList()
{
	return _importList;
}

void Import::addModule(string module, string alias)
{
	pair <string, string> import (module,alias);
	_importList.push_back(import);
}

string Import::toString()
{
	return "Import";
}
	
void Import::accept(Visitor &visitor)
{
	visitor.visit(this);
}