#include <Number.h>

  Number::Number(){}

  Number::Number(int val) {

    number = to_string(val);
  }

  Number::Number(float val) {
    std::ostringstream streamObj3;
    streamObj3 << std::fixed;
    streamObj3 << std::setprecision(1);
    streamObj3 << val;
    number = streamObj3.str();
    
  }

  Number::Number(double val) { 
    stringstream ss (stringstream::in | stringstream::out);
    ss << val;
    number = ss.str();
  }

  string Number::toString() 
  {
    return number;
  }
  void Number::accept(Visitor &visitor) 
  {
    visitor.visit(this);
  }