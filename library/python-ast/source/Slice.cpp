#include <Slice.h>

Slice::Slice()
{
	
}

Slice::Slice(Expression* lower, Expression* upper)
{
	_lower = lower;
	_upper = upper;
	_step = nullptr;
}

Slice& Slice::SetLower(Expression* lower)
{
	_lower = lower;
	return *this;
}

Slice& Slice::SetUpper(Expression* upper)
{
	_upper = upper;
	return *this;
}

Slice& Slice::SetStep(Expression* step)
{
	_step = step;
	return *this;
}

Expression* Slice::GetLower()
{
	return _lower;
}

Expression* Slice::GetUpper()
{
	return _upper;
}

Expression* Slice::GetStep()
{
	return _step;
}

string Slice::toString() 
{

}

void Slice::accept(Visitor &visitor) 
{
	visitor.visit(this);
}