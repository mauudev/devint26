#include <Concat.h>

Concat::Concat()
{
	_isEmpty = true;
}

Concat& Concat::addBodyNode(Statement* statementNode)
{
 	_body.push_back(statementNode);
 	return *this;
}

Concat& Concat::addBodyNode(Expression* expressionNode)
{
 	_body.push_back(expressionNode);
 	return *this;
}

string Concat::toString() 
{
	return "";
}

void Concat:: accept(Visitor& visitor) 
{
  visitor.visit(this);
}

int Concat::SizeBodyList()
{
  return _body.size();
}

vector<NodeAST*> Concat::GetBodyList()
{
  return _body;
}

void Concat::addParameters(ParameterList paramList)
{
  this->_paramList = paramList;
}

string Concat::NoBody() 
{
	if(_isEmpty) return " pass";
	else return "";
}

CallFunction Concat::Call()
{
	CallFunction call{*this};
	return call;
}

CallFunction Concat::Call(Expression* func)
{
	CallFunction call{*this,func};
	return call;
}

ParameterList& Concat::getParameters()
{
	return _paramList;
}