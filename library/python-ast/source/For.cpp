#include <For.h>

For::For(){}

For:: For(Expression* expression, Expression* expressionIt)
{
	_expression = expression;
	_expressionIt = expressionIt;
	_reservedWord = " in ";
}

For& For::addBody(Statement* statement)
{
	addChild(statement);
	return *this;
}

For& For::addBody(Expression* expression)
{
	addChild(expression);
	return *this;
}

For& For::addBodyElse(Statement* statement)
{
	bodyElse.push_back(statement);
	return *this;
}

For& For::addBodyElse(Expression* expression)
{
	bodyElse.push_back(expression);
	return *this;
}

Expression* For::getExprAssignment()
{
	return _expression;
}

Expression* For::getExprIterable()
{
	return _expressionIt;
}

bool For::hasBodyElse()
{
	return (bodyElse.size()>0)? true : false;
}

vector<NodeAST*> For::getBodyElse()
{
	return bodyElse;
}

string For::toString() 
{
    return "for";
}

string For::getReservedWord()
{
	return _reservedWord;
}

void For::accept(Visitor &visitor) 
{
    visitor.visit(this);
}


