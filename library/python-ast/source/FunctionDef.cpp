#include <FunctionDef.h>

FunctionDef::FunctionDef(){}

FunctionDef::FunctionDef(string funcName)
{
	_nameFunction = funcName;
	_isEmpty = true;
}

FunctionDef& FunctionDef::addParameter(Expression* parameter)
{
	_paramList.addParameter(parameter);
	return *this;
}

FunctionDef& FunctionDef::addBodyNode(Statement* statementNode)
{
 	_body.push_back(statementNode);
 	return *this;
}

FunctionDef& FunctionDef::addBodyNode(Expression* expressionNode)
{
 	_body.push_back(expressionNode);
 	return *this;
}

string FunctionDef::toString() 
{
	return "";
}

void FunctionDef:: accept(Visitor& visitor) 
{
  visitor.visit(this);
}

int FunctionDef::SizeBodyList()
{
  return _body.size();
}

vector<NodeAST*> FunctionDef::GetBodyList()
{
  return _body;
}

void FunctionDef::addParameters(ParameterList paramList)
{
  this->_paramList = paramList;
}

string FunctionDef::NoBody() 
{
	if(_isEmpty) return " pass";
	else return "";
}

CallFunction FunctionDef::Call()
{
	CallFunction call{*this};
	return call;
}

CallFunction FunctionDef::Call(Expression* func)
{
	CallFunction call{*this,func};
	return call;
}

string FunctionDef::getName()
{
	return _nameFunction;
}

ParameterList& FunctionDef::getParameters()
{
	return _paramList;
}