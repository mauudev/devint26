#include <Tuple.h>

Tuple::Tuple()
{

}

Tuple::Tuple(Expression* exprOne, Expression* exprTwo)
{
	addChild(exprOne);
	addChild(exprTwo);
}

Tuple& Tuple::AddExpression(Expression* expression)
{
	addChild(expression);
	return *this;
}

string Tuple::toString()
{
	return "";
}

void Tuple::accept(Visitor& visitor)
{
	visitor.visit(this);
}