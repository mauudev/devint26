#include <CallFunction.h>

CallFunction::CallFunction(){}

CallFunction::CallFunction(Expression* fun):_func(fun)
{
	_function=nullptr;
	_class=nullptr;
}

CallFunction::CallFunction(FunctionDef& function)
{
	_function = &function;		
	_func=nullptr;
}

CallFunction::CallFunction(ClassDef& function)
{
	_class = &function;		
	_func=nullptr;
}

CallFunction::CallFunction(Concat& function)
{
	_concat = &function;
	_func=nullptr;
}

CallFunction::CallFunction(FunctionDef& function, Expression* func):
_func(func)
{
	_function = &function;
}

CallFunction::CallFunction(ClassDef& function, Expression* func):
_func(func)
{
	_class = &function;
}

CallFunction::CallFunction(Concat& function, Expression* func):
_func(func)
{
	_concat = &function;
}

string CallFunction::toString() 
{
	_function->addParameters(_argumentsList);
	return _function->getName();
}

void CallFunction::accept(Visitor& visitor)
{
	visitor.visit(this);
}

CallFunction& CallFunction::addArgument(Expression* argument)
{
	_argumentsList.addParameter(argument);
	return *this;
}

CallFunction& CallFunction::AddKeyWord(KeyWord* key)
{	
	_keyWordsList.addParameter(key);
	return *this;
}

ParameterList CallFunction::GetArguments()
{
	return _argumentsList;
}

ParameterList CallFunction::GetKeyWords()
{
	return _keyWordsList;
}

Expression* CallFunction::GetFunc()
{
	return _func;
}

FunctionDef* CallFunction::GetFunction()
{	
	return _function;
}