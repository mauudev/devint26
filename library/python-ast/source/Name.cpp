#include <Name.h>

Name::Name()
{
	_id="";
}

Name::Name(string id):_id(id){}

void Name::setId(string id)
{
	_id=id;
}

string Name::toString()
{
	return _id;
}

void Name::accept(Visitor& visitor)
{
	visitor.visit(this);
}