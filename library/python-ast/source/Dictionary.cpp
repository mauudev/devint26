#include <Dictionary.h>

Dictionary::Dictionary()
{
	
}

Dictionary::Dictionary(Expression* key, Expression* value)
{
	pair<Expression*, Expression*> keyPair(key, value);
	_keyList.push_back(keyPair);
}

Dictionary& Dictionary::AddKey(Expression* key, Expression* value)
{
	pair<Expression*, Expression*> keyPair(key, value);
	_keyList.push_back(keyPair);
	return *this;
}

vector<pair<Expression*, Expression*>> Dictionary::GetKeyList()
{
	return _keyList;
}

int Dictionary::sizeKeyList()
{
  return _keyList.size();
}

string Dictionary::toString()
{
	
}
	
void Dictionary::accept(Visitor &visitor)
{
	visitor.visit(this);
}
