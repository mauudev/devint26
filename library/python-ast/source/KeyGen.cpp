#include <KeyGen.h>

KeyGen::KeyGen()
{
	d=0; c=0;
}

string KeyGen::generateKey()
{
	string key=to_string(d);
	d++;
	c=(c+1)%26;
	key+=(char)(c+'A');
	return key;
}