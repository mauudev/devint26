#include <limits.h>
#include "gtest/gtest.h"

class PrintTest   : public ::testing::Test 
{
   

};

TEST_F(PrintTest, NoExpressionsPrint)
{

    BuilderCodePy py;
    Print& print = py.print(); 
    string expected = "print";
    ASSERT_EQ(expected, py.generateCode(&print));
    
}

TEST_F(PrintTest, ChevronPrint)
{
    BuilderCodePy py;
    Print& print = py.print(&py.name("variable"), &py.num(2));
    string expected = "print >> variable, 2";
    ASSERT_EQ(expected, py.generateCode(&print));
    
}

TEST_F(PrintTest, SimplePrint)
{

    BuilderCodePy py;
    Print& print = py.print();
    print.AddExpression(&py.num(2)).AddExpression(&py.name("a"));
    string expected = "print 2, a";
    ASSERT_EQ(expected, py.generateCode(&print));
    
}

TEST_F(PrintTest, NoNewLinePrint)
{

    BuilderCodePy py;
    Print& print = py.print();
    print.AddExpression(&py.num(2)).AddExpression(&py.name("a")).NoNewLine();
    string expected = "print 2, a,";
    ASSERT_EQ(expected, py.generateCode(&print));
    
}

TEST_F(PrintTest, CallFunctionPrint)
{

    BuilderCodePy py;
    Print& print = py.print();
    print.AddExpression(&py.num(2)).AddExpression(&py.name("a")).AddExpression(&py.call(&py.name("method")));
    string expected = "print 2, a, method()";
    ASSERT_EQ(expected, py.generateCode(&print));
    
}

TEST_F(PrintTest, PrintWithParameters)
{

    BuilderCodePy py;
    Print& print = py.print();
    print.AddExpression(&py.num(2)).AddExpression(&py.num(4)).AddExpression(&py.str("%a tiene %d gatos")).AddParameter(&py.name("a")).AddParameter(&py.name("c"));
    string expected = "print 2, 4, \"%a tiene %d gatos\" %(a, c)";
    ASSERT_EQ(expected, py.generateCode(&print));
    
}

TEST_F(PrintTest, PrintWithArguments)
{

    BuilderCodePy py;
    Print& print = py.print();
    print.AddExpression(&py.num(2)).AddExpression(&py.name("a")).AddExpression(&py.call(&py.name("method")).addArgument(&py.str("gato")).addArgument(&py.num(2)));
    string expected = "print 2, a, method(\"gato\", 2)";
    ASSERT_EQ(expected, py.generateCode(&print));
    
}
