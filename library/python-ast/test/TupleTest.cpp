#include <limits.h>
#include "gtest/gtest.h"

class TupleTest : public ::testing::Test 
{
   

};

TEST_F(TupleTest, MultipleTuple)
{
	BuilderCodePy py;
	Tuple& tuple = py.tuple(&py.num(2), &py.num(4));
    tuple.AddExpression(&py.name("a")).AddExpression(&py.str("variable")); 
    string expected = "2, 4, a, \"variable\"";
    ASSERT_EQ(expected, py.generateCode(&tuple));
    
}
