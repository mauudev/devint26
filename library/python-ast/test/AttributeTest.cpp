#include <limits.h>
#include "gtest/gtest.h"

class AttributeTest: public ::testing::Test 
{
};

TEST_F(AttributeTest, SimpleAttribute)
{
    Name self("self");
    Attribute at(&self,"input");
    BuilderCode visitor;
    visitor.visit(&at);
    string expected = "self.input";
    ASSERT_EQ(expected,visitor.getCode());   
}

TEST_F(AttributeTest, AssigmentAndAttribute)
{    
    Name tf("tf");
    Attribute nn(&tf,"nn");
    Attribute relu(&nn,"relu");
    Name act("activation_nf");
    Assignment assig(&act,&relu);
    BuilderCode visitor;
    visitor.visit(&assig);
    string expected = "activation_nf = tf.nn.relu";
    ASSERT_EQ(expected,visitor.getCode());   
}

TEST_F(AttributeTest, SimpleCallFuntionSingleAndAttribute)
{   
    FunctionDef funFully("fully_connected");
    Name tf("tf");
    CallFunction callFunFully=funFully.Call(&tf);
    BuilderCode visitor;
    visitor.visit(&callFunFully);
    string expected = "tf.fully_connected()";
    ASSERT_EQ(expected,visitor.getCode());   
}

TEST_F(AttributeTest, CallFuntionWithArgumentsAndAttribute)
{   
    Name tf("tf");
    FunctionDef reshape("reshape");
    CallFunction callReshape=reshape.Call(&tf);
    FunctionDef multinomial("multinomial");
    CallFunction callMultinomial=multinomial.Call(&tf);
    Name weights("weights");
    Number one(1);
    callMultinomial.addArgument(&weights).addArgument(&one);
    Name brackets("[]");
    callReshape.addArgument(&callMultinomial).addArgument(&brackets);    
    Name self("self");
    Attribute _reshapeModel(&self,"_reshapeModel");
    Assignment assig(&_reshapeModel,&callReshape);
    BuilderCode visitor;
    visitor.visit(&assig);
    string expected = "self._reshapeModel = tf.reshape(tf.multinomial(weights, 1), [])";
    ASSERT_EQ(expected,visitor.getCode());   
}