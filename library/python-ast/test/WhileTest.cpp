#include <limits.h>
#include "gtest/gtest.h"

#include <BuilderCode.h>
#include <BuilderCodePy.h>

class WhileTest   : public ::testing::Test 
{
   

};
TEST_F(WhileTest, simpleWhileWithBoolExpression)
{

    Number c1 = Number(2);
    Name var = Name("variable");
    Compare bool1(&var,Eq, &c1);
    While while1 = While(&bool1); 
    BuilderCode visitor = BuilderCode();
    visitor.visit(&while1);
    string expected = "while(variable == 2):";
    ASSERT_EQ(expected, visitor.getCode());
    
}

TEST_F(WhileTest, simpleWhileWithCallExpression)
{
    BuilderCodePy py;
    While& w = py.whileStmt(&py.call(&py.name("method"))
                        .addArgument(&py.num(2))
                        .addArgument(&py.name("variable")));
    string expected = "while(method(2, variable)):";
    ASSERT_EQ(expected, py.generateCode(&w));   
}

TEST_F(WhileTest, WhileWithCall)
{

    Number c1 = Number(2);
    Number c2 = Number(4);
    Name var1 = Name("variable");
    Name var2 = Name("a");
    Assignment assing = Assignment(&var2, &c2);
    Compare bool1(&var1,Eq, &c1);
    FunctionDef function = FunctionDef("method");
    CallFunction call = function.Call();
    call.addArgument(&c1).addArgument(&var1);
    While while1 = While(&bool1);
    while1.addBody(&assing).addBody(&call); 
    BuilderCode visitor = BuilderCode();
    visitor.visit(&while1);
    string expected = "while(variable == 2):\n\ta = 4\n\tmethod(2, variable)";
    ASSERT_EQ(expected, visitor.getCode());
    
}

TEST_F(WhileTest, WhileWithIf)
{

    Number c1 = Number(2);
    Number c2 = Number(4);
    Name var1 = Name("variable");
    Name var2 = Name("a");
    Assignment assing1 = Assignment(&var2, &c2);
    Assignment assing2 = Assignment(&var1, &c1);
    Compare bool1(&var1,Eq, &c1);
    BoolOp bool2 = BoolOp(&c1,OR, &c2);
    If if1 = If(&bool2);
    if1.addIfNode(&assing1).addIfNode(&assing2);
    While while1 = While(&bool1);
    while1.addBody(&assing1).addBody(&if1); 
    BuilderCode visitor = BuilderCode();
    visitor.visit(&while1);
    string expected = "while(variable == 2):\n\ta = 4\n\tif (2 or 4):\n\t\ta = 4\n\t\tvariable = 2";
    ASSERT_EQ(expected, visitor.getCode());
    
}

TEST_F(WhileTest, WhileWithAnotherWhile)
{

    Number c1 = Number(2);
    Number c2 = Number(4);
    Name var1 = Name("variable");
    Name var2 = Name("a");
    Assignment assing1 = Assignment(&var2, &c2);
    Assignment assing2 = Assignment(&var1, &c1);
    Compare bool1(&var1,Eq, &c1);
    BoolOp bool2(&c1,OR, &c2);
    While while2 = While(&bool2);
    while2.addBody(&assing1).addBody(&assing2);
    While while1 = While(&bool1);
    while1.addBody(&assing1).addBody(&while2); 
    BuilderCode visitor = BuilderCode();
    visitor.visit(&while1);
    string expected = "while(variable == 2):\n\ta = 4\n\twhile((2 or 4)):\n\t\ta = 4\n\t\tvariable = 2";
    ASSERT_EQ(expected, visitor.getCode());
}

