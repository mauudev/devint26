#include <limits.h>
#include "gtest/gtest.h"

#include <BuilderCode.h>

#include <PythonInjector.h>

class FunctionsTest : public ::testing::Test {  
};
TEST_F(FunctionsTest, simpleFunctionWithoutReturn){

	BuilderCodePy py;
	FunctionDef& f=py.functionDef("method").addParameter(&py.name("test"))
							.addBodyNode(&py.assign(&py.name("test"),&py.binOp(&py.num(32),ADD,&py.num(54))));    
	string expected = "def method(test):\n\ttest = (32+54)";

	ASSERT_EQ(expected, py.generateCode(&f));
	
}

TEST_F(FunctionsTest, simpleFunctionWithReturn){

	BuilderCodePy py;
	FunctionDef& f=py.functionDef("method").addParameter(&py.name("test"))
							.addBodyNode(&py.assign(&py.name("test"),&py.binOp(&py.num(32),ADD,&py.num(54))))
							.addBodyNode(&py.returnStmt(&py.boolOp(&py.name("variable"),AND,&py.num(54))));
	string expected = "def method(test):\n\ttest = (32+54)\n\treturn (variable and 54)\n";

	ASSERT_EQ(expected, py.generateCode(&f));
	
}

TEST_F(FunctionsTest, simpleFunctionWithMethodReturn){

	Number n1 = Number(32);
	Number n2 = Number(54);

	Name var1 = Name("test");
	Name var2 = Name("variable");

	BinaryOperator add = BinaryOperator(&n1, ADD, &n2);

	Assignment assign = Assignment(&var1, &add);

	BoolOp bool1 = BoolOp(&var2,AND, &n2);

	Return ret(&bool1);

	FunctionDef function = FunctionDef("method");
	function.addParameter(&var1);
	function.addBodyNode(&assign).addBodyNode(&ret);

	FunctionDef boolFunction = FunctionDef("Bool");

	BuilderCode visitor = BuilderCode();
	visitor.visit(&function);

	string expected = "def method(test):\n\ttest = (32+54)\n\treturn (variable and 54)\n";

	ASSERT_EQ(expected, visitor.getCode());
	
}

TEST_F(FunctionsTest, simpleFunctionWithReturnOfAMethod){

	Number n1 = Number(32);
	Number n2 = Number(54);

	Name var1 = Name("test");
	Name var2 = Name("variable");

	BinaryOperator add = BinaryOperator(&n1, ADD, &n2);

	Assignment assign = Assignment(&var1, &add);

	FunctionDef function = FunctionDef("method");
	CallFunction call = function.Call();
	call.addArgument(&n1);
	Return ret(&call);
	function.addParameter(&var1);
	function.addBodyNode(&assign).addBodyNode(&ret);

	FunctionDef boolFunction = FunctionDef("Bool");

	BuilderCode visitor = BuilderCode();
	visitor.visit(&function);

	string expected = "def method(test):\n\ttest = (32+54)\n\treturn method(32)\n";

	ASSERT_EQ(expected, visitor.getCode());
	
}

TEST_F(FunctionsTest, functionWithSentenceIF){

	Number c1 = Number(1);
	Number c2 = Number(2);
	Number c3 = Number(3);

	BinaryOperator add = BinaryOperator(&c1, ADD, &c2);
	BinaryOperator mul = BinaryOperator(&c2, MULT, &c3);
   
	Compare equal(&add,Eq, &mul);

	FunctionDef function = FunctionDef("comparations");
	If _if = If(&equal);
	function.addBodyNode(&_if);

	BuilderCode visitor = BuilderCode();
	visitor.visit(&function);

	string expected = "def comparations():\n\tif (1+2) == (2*3):";

	ASSERT_EQ(expected, visitor.getCode());
	
}

TEST_F(FunctionsTest, functionWithWHILE){

	Number c1 = Number(1);
	Number c2 = Number(2);
	Number c3 = Number(3);

	BinaryOperator add = BinaryOperator(&c1, ADD, &c2);
	BinaryOperator mul = BinaryOperator(&c2, MULT, &c3);
   
	Compare equal(&add,Eq, &mul);

	FunctionDef function = FunctionDef("iterators");

	While _if = While(&equal);
	function.addBodyNode(&_if);

	BuilderCode visitor = BuilderCode();
	visitor.visit(&function);

	string expected = "def iterators():\n\twhile((1+2) == (2*3)):";

	ASSERT_EQ(expected, visitor.getCode());
	
}


TEST_F(FunctionsTest, complexFunctionWithIFandWHILE){

	Number c1 = Number(1);
	Number c2 = Number(2);
	Number c3 = Number(3);

	BinaryOperator add = BinaryOperator(&c1, ADD, &c2);
	BinaryOperator mul = BinaryOperator(&c2, MULT, &c3);
   

	Compare equal(&add,Eq, &mul);

	BoolOp _and = BoolOp(&equal, AND, &equal);

	FunctionDef function = FunctionDef("mix");
	While _while = While(&_and);
	If _if = If(&equal);
	_while.addBody(&_if);
	function.addBodyNode(&_while);


	BuilderCode visitor = BuilderCode();
	visitor.visit(&function);

	string expected = "def mix():\n\twhile(((1+2) == (2*3) and (1+2) == (2*3))):\n\t\tif (1+2) == (2*3):";

	ASSERT_EQ(expected, visitor.getCode());
	
}

TEST_F(FunctionsTest, complexFunctionDefinition){

	Number c1 = Number(1);
	Number c2 = Number(2);
	Number c3 = Number(3);
	Number c4 = Number(4);
	Number c5 = Number(5);
	Number c6 = Number(6);
	Number c7 = Number(7);
	Number c8 = Number(8);
	Number c9 = Number(9);
	Number c0 = Number(0);
   
	Name varA = Name("a");
	Name varB = Name("b");
	Name varC = Name("c");
	Name varD = Name("d");
   
	BinaryOperator add1 = BinaryOperator(&c3, ADD, &c4);//mejorar sin Binoperator
	BinaryOperator mul1 = BinaryOperator(&c5, MULT, &c6);//mejorar
	BinaryOperator add2 = BinaryOperator(&add1, ADD, &mul1);
	BinaryOperator root = BinaryOperator(&c9, MULT, &add2);
	
	BoolOp an = BoolOp(&add1,AND, &mul1);
	
	FunctionDef function = FunctionDef("method");
	FunctionDef method = FunctionDef("method_1");
	
	Name ans = Name("answer_var");
	Assignment assing = Assignment(&ans, &root);

	Name var = Name("_var");
	Assignment assing2 = Assignment(&var, &add2);

	method.addParameter(&varD).addParameter(&varA);

	function.addParameter(&varA).addParameter(&varB).addParameter(&varC).addBodyNode(&assing).addBodyNode(&assing2).addBodyNode(&method);

	CallFunction call = function.Call();
	call.addArgument(&c1).addArgument(&c2).addArgument(&c3);

	CallFunction call_1 = method.Call();
	call_1.addArgument(&c1).addArgument(&c2);

	function.addBodyNode(&call_1);
	function.addBodyNode(&call);

	BuilderCode visitor = BuilderCode();
	visitor.visit(&function);

	string expected = "def method(a, b, c):\n\tanswer_var = (9*((3+4)+(5*6)))\n\t_var = ((3+4)+(5*6))\n\tdef method_1(d, a): pass\n\tmethod_1(1, 2)\n\tmethod(1, 2, 3)";

	ASSERT_EQ(expected, visitor.getCode());
	
}

TEST_F(FunctionsTest, simpleFunctionWithReturnWithMultipleExpressions){

	Number n1 = Number(32);
	Number n2 = Number(54);

	Name var1 = Name("test");
	Name var2 = Name("variable");

	BinaryOperator add = BinaryOperator(&n1, ADD, &n2);

	Assignment assign = Assignment(&var1, &add);

	BoolOp bool1 = BoolOp(&var2,AND,&n2);

	Tuple tuple(&bool1, &var1);

	Return ret(&tuple);

	FunctionDef function = FunctionDef("method");
	function.addParameter(&var1);
	function.addBodyNode(&assign).addBodyNode(&ret);
	tuple.AddExpression(&n1);

	BuilderCode visitor = BuilderCode();
	visitor.visit(&function);

	string expected = "def method(test):\n\ttest = (32+54)\n\treturn (variable and 54), test, 32\n";
	

	ASSERT_EQ(expected, visitor.getCode());    
}

TEST_F(FunctionsTest, GetNormalizeObservationsFilterFunction){
	BuilderCodePy py;
	FunctionDef& getNOF=py.functionDef("getNormalizeObservationsFilter")
						  .addParameter(&py.name("listExpectedObsForGame")).addParameter(&py.name("vecIndexFilter"))
								.addBodyNode(&py.assign(&py.name("vecAuxObs"),&py.list()))
								.addBodyNode(&py.forStmt(&py.name("obs"),&py.name("listExpectedObsForGame"))
											.addBody(&py.assign(&py.name("obs"),&py.call(&py.attribute(&py.name("np"),"divide"))
														   					.addArgument(&py.name("obs")).addArgument(&py.num(255.0f))))
											.addBody(&py.assign(&py.name("obs"),&py.call(&py.attribute(&py.name("np"),"delete"))
														   					.addArgument(&py.name("obs")).addArgument(&py.name("vecIndexFilter"))))
											.addBody(&py.call(&py.attribute(&py.name("vecAuxObs"),"append")).addArgument(&py.name("obs")))								)
								.addBodyNode(&py.assign(&py.name("matNormalizeObs"),&py.call(&py.attribute(&py.name("np"),"vstack"))
																					.addArgument(&py.name("vecAuxObs"))))
								.addBodyNode(&py.returnStmt(&py.tuple(&py.name("matNormalizeObs"),&py.name("vecAuxObs"))));
	py.addCode(&getNOF)
	  .addCode(&py.assign(&py.tuple(&py.name("pilaO"),&py.name("pilaA")),&py.tuple(&py.list(),&py.list())));                  
	string expected = 
R"foo(def getNormalizeObservationsFilter(listExpectedObsForGame, vecIndexFilter):
	vecAuxObs = []
	for obs in listExpectedObsForGame :
		obs = np.divide(obs, 255.0)
		obs = np.delete(obs, vecIndexFilter)
		vecAuxObs.append(obs)
	matNormalizeObs = np.vstack(vecAuxObs)
	return matNormalizeObs, vecAuxObs

pilaO, pilaA = [], [])foo";    

	ASSERT_EQ(expected, py.generateCode());    
}

TEST_F(FunctionsTest, display_arrFunction){
	BuilderCodePy py;
	FunctionDef& fda=py.functionDef("display_arr");
	fda.addParameter(&py.name("screen")).addParameter(&py.name("arr")).addParameter(&py.name("transpose"))
	   .addParameter(&py.name("video_size"))
	   .addBodyNode(&py.assign(&py.tuple(&py.name("arr_min"),&py.name("arr_max")),
	   				&py.tuple(&py.call(&py.attribute(&py.name("arr"),"min")),
	   				   			 &py.call(&py.attribute(&py.name("arr"),"max")))))
	   .addBodyNode(&py.assign(&py.name("arr"),
	   							&py.binOp(&py.binOp(&py.num(255.0f),MULT,&py.binOp(&py.name("arr"),SUB,&py.name("arr_min"))),
	   				  			DIV,&py.binOp(&py.name("arr_max"),SUB,&py.name("arr_min")))))
	   .addBodyNode(&py.assign(&py.name("pyg_img"),
	   				&py.call(&py.attribute(&py.attribute(&py.name("pygame"),"surfarray"),"make_surface"))
	   				   	  .addArgument(&py.name("arr.swapaxes(0, 1) if transpose else arr"))))
	   .addBodyNode(&py.assign(&py.name("pyg_img"),
	   				&py.call(&py.attribute(&py.attribute(&py.name("pygame"),"transform"),"scale"))
	   				   	  .addArgument(&py.name("pyg_img")).addArgument(&py.name("video_size"))))
	   .addBodyNode(&py.call(&py.attribute(&py.name("screen"),"blit")).addArgument(&py.name("pyg_img"))
	   														  .addArgument(&py.name("(0,0)")));
	py.addCode(&fda);
	string expected = 
R"foo(def display_arr(screen, arr, transpose, video_size):
	arr_min, arr_max = arr.min(), arr.max()
	arr = ((255.0*(arr-arr_min))/(arr_max-arr_min))
	pyg_img = pygame.surfarray.make_surface(arr.swapaxes(0, 1) if transpose else arr)
	pyg_img = pygame.transform.scale(pyg_img, video_size)
	screen.blit(pyg_img, (0,0)))foo";    

	ASSERT_EQ(expected, py.generateCode());    
}
