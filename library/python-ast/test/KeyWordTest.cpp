#include <limits.h>
#include "gtest/gtest.h"

class KeyWordTest : public ::testing::Test 
{
   

};

TEST_F(KeyWordTest, SigleKeyWordInCallFunction)
{
    Name self("self");
    Attribute _s(&self,"_s");
    Attribute _train(&self,"_train"); 
    FunctionDef run("run");
    CallFunction callRun=run.Call(&_s);
    Name batch("batch_feed");
    KeyWord feed_dict("feed_dict",&batch);
    callRun.addArgument(&_train).AddKeyWord(&feed_dict);    
    BuilderCode visitor = BuilderCode();
    visitor.visit(&callRun);
    string expected = "self._s.run(self._train, feed_dict=batch_feed)";
    ASSERT_EQ(expected, visitor.getCode());    
}

TEST_F(KeyWordTest, MultipleKeyWordInCallFunction)
{
    BuilderCodePy py;
    CallFunction& c=py.call(&py.attribute(&py.attribute(&py.name("tf"),"nn"),"softmax_cross_entropy_with_logits"))
                            .AddKeyWord(&py.keyword("labels",&py.name("yExpectedActions")))
                            .AddKeyWord(&py.keyword("logits",&py.name("yOperForNodes")));
    string expected = "tf.nn.softmax_cross_entropy_with_logits(labels=yExpectedActions, logits=yOperForNodes)";
    ASSERT_EQ(expected, py.generateCode(&c));    
}

TEST_F(KeyWordTest, FunctionWithOnlyKeyWords)
{
    BuilderCodePy py;
    CallFunction& c=py.call(&py.name("fullyConected"))
                        .AddKeyWord(&py.keyword("feed_dict",&py.name("batch_feed")))
                        .AddKeyWord(&py.keyword("feed_dict",&py.name("batch_feed")));

    string expected = "fullyConected(feed_dict=batch_feed, feed_dict=batch_feed)";
    ASSERT_EQ(expected, py.generateCode(&c));
}