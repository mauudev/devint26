#include <limits.h>
#include "gtest/gtest.h"

class BuilderCodePyTest: public ::testing::Test 
{

};

TEST_F(BuilderCodePyTest, generatePythonCode)
{
	BuilderCodePy py;
	py.addCode(&py.functionDef("getSummation").addParameter(&py.name("n"))
				  .addBodyNode(&py.assign(&py.name("answer"),&py.num(0)))
				  .addBodyNode(&py.whileStmt(&py.compare(&py.name("n"),Gt,&py.num(0)))
								.addBody(&py.augAssign(&py.name("answer"),ADD,&py.name("n")))
								.addBody(&py.augAssign(&py.name("n"),SUB,&py.num(1))))
								.addBodyNode(&py.returnStmt(&py.name("answer"))));

	
	py.addCode(&py.functionDef("getFactorial").addParameter(&py.name("n"))
					.addBodyNode(&py.ifStmt(&py.compare(&py.name("n"),Eq,&py.num(0)))
											.addIfNode(&py.returnStmt(&py.num(1)))
								)
					.addBodyNode(&py.returnStmt(&py.binOp(&py.name("n"),
													MULT,&py.call(&py.name("getFactorial"))
													.addArgument(&py.binOp(&py.name("n"),SUB,&py.num(1)))
													)
												)
								)
			  );
	py.addCode(&py.print().AddExpression(&py.call(&py.name("getSummation"))
						                    .addArgument(&py.call(&py.name("getFactorial"))
						                    .addArgument(&py.num(3)))
						                )
			   );

	PythonScriptGenerator pg;
	string code=py.generateCode();
	pg.GenerateScript("test","",code);

	string expected=
	R"foo(def getSummation(n):
	answer = 0
	while(n > 0):
		answer += n 
		n -= 1 
	return answer

def getFactorial(n):
	if n == 0:
		return 1

	return (n*getFactorial((n-1)))

print getSummation(getFactorial(3)))foo";
	ASSERT_EQ(expected, code);  
}

