#include <limits.h>
#include "gtest/gtest.h"

#include "AssignmentTest.cpp"
#include "BoolOpTest.cpp"
#include "ArithmeticExpressionsTest.cpp"
#include "FunctionsTest.cpp"
#include "WhileTest.cpp"
#include "ForTest.cpp"
#include "IfTest.cpp"
#include "PrintTest.cpp"
#include "StringTest.cpp"
#include "ImportTest.cpp"
#include "WithTest.cpp"
#include "DictionaryTest.cpp"
#include "ListTest.cpp"
#include "AttributeTest.cpp"
#include "PythonInjectorTest.cpp"
#include "KeyWordTest.cpp"
#include "SliceTest.cpp"
#include "IndexTest.cpp"
#include "ExtSliceTest.cpp"
#include "UnaryOpTest.cpp"
#include "SubscriptTest.cpp"
#include "CompareTest.cpp"
#include "NumTest.cpp"
#include "ModuleTest.cpp"
#include "GlobalTest.cpp"
#include "TupleTest.cpp"
#include "AugAssignTest.cpp"
#include "BuilderCodePyTest.cpp"

int main(int argc, char **argv) 
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}