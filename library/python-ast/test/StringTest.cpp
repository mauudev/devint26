#include <limits.h>
#include "gtest/gtest.h"

class StringTest : public ::testing::Test 
{
   

};

TEST_F(StringTest, SimpleString)
{
    BuilderCodePy py;
    StringExp& str = py.str("Hola mundo");
    string expected = "\"Hola mundo\"";
    ASSERT_EQ(expected, py.generateCode(&str));
    
}

TEST_F(StringTest, StringAssigment)
{
    BuilderCodePy py;
    Assignment assig = py.assign(&py.name("a"),&py.str("Hola mundo"));
    string expected = "a = \"Hola mundo\"";
    ASSERT_EQ(expected, py.generateCode(&assig));
    
}

TEST_F(StringTest, StringPrint)
{

    BuilderCodePy py;
    Print& print = py.print();
    print.AddExpression(&py.str("Pepito tiene 7 gatos"));
    string expected = "print \"Pepito tiene 7 gatos\"";
    ASSERT_EQ(expected, py.generateCode(&print));
    
}

TEST_F(StringTest, StringArgumentInACallFunction)
{

    BuilderCodePy py;
    CallFunction& call = py.call(&py.name("method"));
    call.addArgument(&py.str("Hola mundo")).addArgument(&py.num(2));
    string expected = "method(\"Hola mundo\", 2)";
    ASSERT_EQ(expected, py.generateCode(&call));
    
}
