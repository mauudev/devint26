#include <limits.h>
#include "gtest/gtest.h"

class ArithmeticExpressionsTest   : public ::testing::Test 
{

};

TEST_F(ArithmeticExpressionsTest, simpleOpeAddWithTwoNumbers)
{
    BuilderCodePy py;
    BinaryOperator& add=py.binOp(&py.num(23),ADD,&py.num(11));
    string expected = "(23+11)";

    ASSERT_EQ(expected, py.generateCode(&add));    
}

TEST_F(ArithmeticExpressionsTest, simpleOpeMulWithTwoNumbers)
{
    BuilderCodePy py;
    BinaryOperator& add=py.binOp(&py.num(23),MULT,&py.num(11));
    string expected = "(23*11)";

    ASSERT_EQ(expected, py.generateCode(&add));    
}


TEST_F(ArithmeticExpressionsTest, simpleMinWithTwoNumbers)
{

    BuilderCodePy py;
    BinaryOperator& add=py.binOp(&py.num(23),SUB,&py.num(11));
    string expected = "(23-11)";

    ASSERT_EQ(expected, py.generateCode(&add));
    
}

TEST_F(ArithmeticExpressionsTest, simpleOpeDivWithTwoNumbers)
{

    BuilderCodePy py;
    BinaryOperator& add=py.binOp(&py.num(23),DIV,&py.num(11));
    string expected = "(23/11)";

    ASSERT_EQ(expected, py.generateCode(&add));
    
}

TEST_F(ArithmeticExpressionsTest, complexOpeAdd)
{
    BuilderCodePy py;
    BinaryOperator& add=py.binOp(
                           &py.binOp(&py.num(213),
                                     ADD,
                                     &py.binOp(&py.num(23),ADD,&py.num(11))
                                     ),
                           ADD,
                           &py.binOp(&py.num(34),
                                    ADD,
                                    &py.binOp(&py.num(45),ADD,&py.num(76)))
                           );

    string expected = "((213+(23+11))+(34+(45+76)))";

    ASSERT_EQ(expected, py.generateCode(&add));    
}

TEST_F(ArithmeticExpressionsTest, complexOpeMul)
{

    Number c1 = Number(23);
    Number c2 = Number(11);
    Number c3 = Number(213);
    Number c4 = Number(45);
    Number c5 = Number(76);
    Number c6 = Number(34);

    Name nameA = Name("a");

    BinaryOperator mul = BinaryOperator(&c1, MULT, &c2);
    BinaryOperator mul2 = BinaryOperator(&c3, MULT, &mul);
    BinaryOperator mul3 = BinaryOperator(&c4, MULT, &c5);
    BinaryOperator mul4 = BinaryOperator(&c6, MULT, &mul3);
    BinaryOperator mul5 = BinaryOperator(&mul2, MULT, &mul4);
    BinaryOperator mul6 = BinaryOperator(&nameA, MULT, &mul5);

    string expected = "(a*((213*(23*11))*(34*(45*76))))";

    BuilderCode visitor = BuilderCode();
    visitor.visit(&mul6);

    ASSERT_EQ(expected, visitor.getCode());
    
}

TEST_F(ArithmeticExpressionsTest, complexOpeMin)
{

    Number c1 = Number(23);
    Number c2 = Number(11);
    Number c3 = Number(213);
    Number c4 = Number(45);
    Number c5 = Number(76);
    Number c6 = Number(34);

    Name nameA = Name("a");

    BinaryOperator min = BinaryOperator(&c1, SUB, &c2);
    BinaryOperator min2 = BinaryOperator(&c3, SUB, &min);
    BinaryOperator min3 = BinaryOperator(&c4, SUB, &c5);
    BinaryOperator min4 = BinaryOperator(&c6, SUB, &min3);
    BinaryOperator min5 = BinaryOperator(&min2, SUB, &min4);
    BinaryOperator min6 = BinaryOperator(&nameA, SUB, &min5);

    string expected = "(a-((213-(23-11))-(34-(45-76))))";

    BuilderCode visitor = BuilderCode();
    visitor.visit(&min6);

    ASSERT_EQ(expected, visitor.getCode());
    
    
}

TEST_F(ArithmeticExpressionsTest, complexOpeDiv)
{

    Number c1 = Number(23);
    Number c2 = Number(11);
    Number c3 = Number(213);
    Number c4 = Number(45);
    Number c5 = Number(76);
    Number c6 = Number(34);

    Name nameA = Name("a");

    BinaryOperator div = BinaryOperator(&c1, DIV, &c2);
    BinaryOperator div2 = BinaryOperator(&c3, DIV, &div);
    BinaryOperator div3 = BinaryOperator(&c4, DIV, &c5);
    BinaryOperator div4 = BinaryOperator(&c6, DIV, &div3);
    BinaryOperator div5 = BinaryOperator(&div2, DIV, &div4);
    BinaryOperator div6 = BinaryOperator(&nameA, DIV, &div5);

    string expected = "(a/((213/(23/11))/(34/(45/76))))";

    BuilderCode visitor = BuilderCode();
    visitor.visit(&div6);

    ASSERT_EQ(expected, visitor.getCode());   
}

TEST_F(ArithmeticExpressionsTest, complexBoolOperatorMix)
{

    Number c1 = Number(23);
    Number c2 = Number(11);
    Number c3 = Number(213);
    Number c4 = Number(45);
    Number c5 = Number(76);
    Number c6 = Number(34);

    Name nameA = Name("a");

    BinaryOperator add = BinaryOperator(&c1, ADD, &c2);
    BinaryOperator min = BinaryOperator(&c3, SUB, &add);
    BinaryOperator div = BinaryOperator(&c4, DIV, &c5);
    BinaryOperator mul = BinaryOperator(&c6, MULT, &div);
    BinaryOperator add2 = BinaryOperator(&min, ADD, &mul);
    BinaryOperator min2 = BinaryOperator(&nameA, SUB, &add2);

    string expected = "(a-((213-(23+11))+(34*(45/76))))";

    BuilderCode visitor = BuilderCode();
    visitor.visit(&min2);

    ASSERT_EQ(expected, visitor.getCode());
    
}