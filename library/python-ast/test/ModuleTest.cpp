#include <limits.h>
#include "gtest/gtest.h"

#include <BuilderCode.h>

class ModuleTest : public ::testing::Test
{
};


TEST_F(ModuleTest, addNodes)
{
	Name score("score"),letter("letter");
	Name letterA("letter = \'A\'"),letterB("letter = \'B\'");
	Name letterC("letter = \'C\'"),letterD("letter = \'D\'");
	Name letterE("letter = \'F\'");
	Number n90(90),n80(80),n70(70),n60(60);
	Compare compare90(&score, GTE, &n90),compare80(&score, GTE, &n80);
	Compare compare70(&score, GTE, &n70),compare60(&score, GTE, &n60);

	If if60 = If(&compare60);
	if60.addIfNode(&letterD).addElifNode(&letterE);
	If if70 = If(&compare70);
	if70.addIfNode(&letterC);
	If if80 = If(&compare80);
	if80.addIfNode(&letterB);
	If if90 = If(&compare90);
	if90.addIfNode(&letterA).addElifNode(&if80).addElifNode(&if70).addElifNode(&if60);

	Return ret(&letter);
	FunctionDef function("letterGrade");
	function.addParameter(&score);
	function.addBodyNode(&if90).addBodyNode(&ret);

	Number n1(32),n2(54);
    Name var("test");
    BinaryOperator add = BinaryOperator(&n1, ADD, &n2);
    Assignment assign = Assignment(&var, &add);
    FunctionDef function2 = FunctionDef("method");
    function2.addParameter(&var).addBodyNode(&assign);

    Name varA("a");
    Number c1(1),c2(2),c3(3);
    BinaryOperator add2 = BinaryOperator(&c1, ADD, &c2);
    BinaryOperator mul2 = BinaryOperator(&c2, MULT, &c3);
    Compare equal(&varA, Eq,&mul2);
    BoolOp _and = BoolOp(&equal,AND, &equal);
    FunctionDef function3 = FunctionDef("mix");
    CallFunction call = function.Call();
    call.addArgument(&varA);
    While _while = While(&_and);
    If _if = If(&equal);
    _if.addIfNode(&compare90).addIfNode(&compare80);
    _while.addBody(&_if);
    function3.addBodyNode(&_while);

	Module mod(&function);
	mod.addBody(&function2).addBody(&function3).addBody(&call);
	BuilderCode visitor = BuilderCode();
	visitor.visit(&mod);

	string expected =
R"foo(def letterGrade(score):
	if score >= 90:
		letter = 'A'
	elif score >= 80:
		letter = 'B'
	elif score >= 70:
		letter = 'C'
	elif score >= 60:
		letter = 'D'
	else:
		letter = 'F'
	return letter

def method(test):
	test = (32+54)
def mix():
	while((a == (2*3) and a == (2*3))):
		if a == (2*3):
			score >= 90
			score >= 80
letterGrade(a))foo";
	ASSERT_EQ(expected, visitor.getCode());
}
