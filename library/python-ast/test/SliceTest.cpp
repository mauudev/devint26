#include <limits.h>
#include "gtest/gtest.h"

#include <iostream>
#include <Visitor.h>
#include <BuilderCode.h>

using namespace std;

class SliceTest : public ::testing::Test 
{
   

};

TEST_F(SliceTest, TwoParameters)
{
    Name var1("a");
    Number c1 = Number(2);
    Slice slice = Slice(&var1, &c1); 
    BuilderCode visitor = BuilderCode();
    visitor.visit(&slice);
    string expected = "a : 2";
    ASSERT_EQ(expected, visitor.getCode());
    
}

TEST_F(SliceTest, ThreeParameters)
{
    Number c1(2), c2(4);
    Name var1("a"), var2("x"), var3("y");
    Slice slice(&var1, &c1);
    slice.SetStep(&c2).SetLower(&var2).SetUpper(&var3); 
    BuilderCode visitor = BuilderCode();
    visitor.visit(&slice);
    string expected = "x : y : 4";
    ASSERT_EQ(expected, visitor.getCode());
    
}
