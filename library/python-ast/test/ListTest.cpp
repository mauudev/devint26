#include <limits.h>
#include "gtest/gtest.h"

class ListTest : public ::testing::Test 
{
   

};

TEST_F(ListTest, NoExpression)
{
    List list; 
    BuilderCode visitor = BuilderCode();
    visitor.visit(&list);
    string expected = "[]";
    ASSERT_EQ(expected, visitor.getCode());
    
}

TEST_F(ListTest, SimpleExpression)
{
    Number c1 = Number(2);
    List list(&c1); 
    BuilderCode visitor = BuilderCode();
    visitor.visit(&list);
    string expected = "[2]";
    ASSERT_EQ(expected, visitor.getCode());
    
}

TEST_F(ListTest, MultipleExpression)
{
    Number c1(2), c2(4);
    StringExp str1("variable"), str2("prueba");
    Name var1 = Name("a");
    List list(&c1); 
    list.AddExpression(&str1).AddExpression(&str2).AddExpression(&c2).AddExpression(&var1); 
    BuilderCode visitor = BuilderCode();
    visitor.visit(&list);
    string expected = "[2, \"variable\", \"prueba\", 4, a]";
    ASSERT_EQ(expected, visitor.getCode());
    
}
