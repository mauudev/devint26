#include <limits.h>
#include "gtest/gtest.h"

#include <BuilderCode.h>
#include <PythonInjector.h>

class UnaryOpTest   : public ::testing::Test {

};


TEST_F(UnaryOpTest, unaryAdd)
{

	Number c1(2);
	Name var1("variable");
	Compare bool1(&var1,Eq, &c1);
	While while1 = While(&bool1);
	
	Name varA("varA");
	UnaryOp uAdd1(UADD,&varA);
	UnaryOp uAdd2(UADD,&uAdd1);
	UnaryOp uAdd3(UADD,&uAdd2);
	while1.addBody(&uAdd1).addBody(&uAdd2).addBody(&uAdd3); 

	string expected = 
R"foo(while(variable == 2):
	+ varA
	+ + varA
	+ + + varA)foo";
	
	BuilderCode visitor = BuilderCode();
	visitor.visit(&while1);
	ASSERT_EQ(expected, visitor.getCode());
	
}

TEST_F(UnaryOpTest, unarySub)
{
    BuilderCodePy py;
    For& f=py.forStmt(&py.name("variable"),&py.name("list"))
    				 .addBody(&py.unaryOp(USUB,&py.name("varA")))
    				 .addBody(&py.unaryOp(USUB,
    				 						  &py.unaryOp(USUB,&py.name("varA"))))
    				 .addBody(&py.unaryOp(USUB,
    				 						  &py.unaryOp(USUB,
    				 						  			 &py.unaryOp(USUB,&py.name("varA")))));
   
    string expected = 	
R"foo(for variable in list :
	- varA
	- - varA
	- - - varA)foo";
    ASSERT_EQ(expected, py.generateCode(&f));
}

TEST_F(UnaryOpTest, unaryInverter)
{
    BuilderCodePy py;
    FunctionDef& f=py.functionDef("method")
    				 .addBodyNode(&py.unaryOp(INVERT,&py.name("varA")))
    				 .addBodyNode(&py.unaryOp(INVERT,
    				 						  &py.unaryOp(INVERT,&py.name("varA"))))
    				 .addBodyNode(&py.unaryOp(INVERT,
    				 						  &py.unaryOp(INVERT,
    				 						  			 &py.unaryOp(INVERT,&py.name("varA")))));

    string expected = 
R"foo(def method():
	~ varA
	~ ~ varA
	~ ~ ~ varA)foo";

    ASSERT_EQ(expected, py.generateCode(&f));
}

TEST_F(UnaryOpTest, unarynot )
{
	
	Name varA("varA");
    UnaryOp uNot1(NOT,&varA);
	UnaryOp uNot2(NOT,&uNot1);
	UnaryOp uNot3(NOT,&uNot2);

    FunctionDef function = FunctionDef("method");
    function.addBodyNode(&uNot1).addBodyNode(&uNot2).addBodyNode(&uNot3);

    BuilderCode visitor = BuilderCode();
    visitor.visit(&function);

    string expected = 
R"foo(def method():
	not varA
	not not varA
	not not not varA)foo";

    ASSERT_EQ(expected, visitor.getCode());
    
}

TEST_F(UnaryOpTest, usingEnum)
{
	
	Name varA("varA");
    UnaryOp uNot1(UnaryOpType::Not,&varA);
	UnaryOp uNot2(UnaryOpType::Not,&uNot1);
	UnaryOp uNot3(UnaryOpType::Not,&uNot2);

    FunctionDef function = FunctionDef("method");
    function.addBodyNode(&uNot1).addBodyNode(&uNot2).addBodyNode(&uNot3);

    BuilderCode visitor = BuilderCode();
    visitor.visit(&function);

    string expected = 
R"foo(def method():
	not varA
	not not varA
	not not not varA)foo";

    ASSERT_EQ(expected, visitor.getCode());
    
}


