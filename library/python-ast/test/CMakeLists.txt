CMAKE_MINIMUM_REQUIRED(VERSION 3.0.0)
PROJECT(python-ast_test)

ADD_COMPILE_OPTIONS(-std=c++11)

INCLUDE_DIRECTORIES(../include)
ADD_DEFINITIONS(-DGTEST)

ADD_SUBDIRECTORY(../../../ThirdParty/googletest-master googletest)
#ENABLE_TESTING()

SET(src_program main_test.cpp
                ../source/Assignment.cpp
				../source/BinaryOperator.cpp
				../source/BoolOp.cpp
				../source/BuilderCode.cpp
				../source/NodeAST.cpp
				../source/Number.cpp
				../source/FunctionDef.cpp
				../source/CallFunction.cpp
				../source/ParameterList.cpp
				../source/Return.cpp
				../source/Break.cpp
				../source/If.cpp
				../source/While.cpp
				../source/For.cpp
				../source/Print.cpp
				../source/StringExp.cpp
				../source/Import.cpp
				../source/ImportFrom.cpp
				../source/With.cpp
				../source/Dictionary.cpp
				../source/List.cpp
				../source/Name.cpp
				../source/Attribute.cpp
				../source/PythonInjector.cpp
				../source/KeyWord.cpp
				../source/Slice.cpp
				../source/Index.cpp
				../source/ExtSlice.cpp
				../source/UnaryOp.cpp
				../source/Subscript.cpp
				../source/Compare.cpp
				../source/Module.cpp
				../source/Global.cpp
				../source/Tuple.cpp
				../source/AugAssign.cpp
				../source/BuilderCodePy.cpp
				../source/KeyGen.cpp
				../source/PythonScriptGenerator.cpp
	)

ADD_EXECUTABLE(python-ast ${src_program})

TARGET_LINK_LIBRARIES(python-ast gtest gtest_main -lpthread)

ADD_TEST(python-ast testi.exe)


