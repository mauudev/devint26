#include <limits.h>
#include "gtest/gtest.h"

#include <BuilderCode.h>

class AugAssignTest  : public ::testing::Test {

};

TEST_F(AugAssignTest, augAdd)
{
	BuilderCodePy py;
    AugAssign& augAdd = py.augAssign(&py.name("fullReward"),ADD, &py.name("reward"));
	string expected = "fullReward += reward ";
	ASSERT_EQ(expected, py.generateCode(&augAdd));
}

TEST_F(AugAssignTest, augSub)
{
	BuilderCodePy py;
    AugAssign& augSub = py.augAssign(&py.name("fullReward"),SUB, &py.name("reward"));
	string expected = "fullReward -= reward ";
	ASSERT_EQ(expected, py.generateCode(&augSub));
}

TEST_F(AugAssignTest, augMul)
{
	BuilderCodePy py;
    AugAssign& augMul = py.augAssign(&py.name("fullReward"),Mult, &py.name("reward"));
	string expected = "fullReward *= reward ";
	ASSERT_EQ(expected, py.generateCode(&augMul));
}


TEST_F(AugAssignTest, augDiv)
{
	BuilderCodePy py;
    AugAssign& augDiv = py.augAssign(&py.name("fullReward"),Div, &py.name("reward"));
	string expected = "fullReward /= reward ";
	ASSERT_EQ(expected, py.generateCode(&augDiv));
}


