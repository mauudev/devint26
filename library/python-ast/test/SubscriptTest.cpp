#include <limits.h>
#include "gtest/gtest.h"

#include <iostream>
#include <Visitor.h>
#include <BuilderCode.h>

using namespace std;

class SubscriptTest : public ::testing::Test 
{
   

};

TEST_F(SubscriptTest, IndexSliceType)
{
    Number c1(2);
    Name var1("a");
    Index index = Index(&c1);
    Subscript subscript(&var1, &index); 
    BuilderCode visitor = BuilderCode();
    visitor.visit(&subscript);
    string expected = "a[2]";
    ASSERT_EQ(expected, visitor.getCode());
    
}

TEST_F(SubscriptTest, SliceSliceType)
{
    Number c1(2);
    Name var1("a"), var2("x"), var3("y");
    Slice slice(&var2, &var3);
    slice.SetStep(&c1); 
    Subscript subscript(&var1, &slice);
    BuilderCode visitor = BuilderCode();
    visitor.visit(&subscript);
    string expected = "a[x : y : 2]";
    ASSERT_EQ(expected, visitor.getCode());
    
}

TEST_F(ExtSliceTest, ExtSliceSliceType)
{
    Number c1(2), c2(4);
    Name var1("a"), var2("x"), var3("y"), var4("z");
    Slice slice1(&var1, &c1), slice2(&var2, &var3);
    Index index = Index(&c2);
    ExtSlice slice = ExtSlice(&slice1);
    slice.AddSlice(&index).AddSlice(&slice2);
    Subscript subscript(&var4, &slice);
    BuilderCode visitor = BuilderCode();
    visitor.visit(&subscript);
    string expected = "z[a : 2, 4, x : y]";
    ASSERT_EQ(expected, visitor.getCode());
    
}
