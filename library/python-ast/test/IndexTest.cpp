#include <limits.h>
#include "gtest/gtest.h"

#include <iostream>
#include <Visitor.h>
#include <BuilderCode.h>

using namespace std;

class IndexTest : public ::testing::Test 
{
   

};

TEST_F(IndexTest, SimpleInstantiation)
{
    Name var1("a");
    Index index = Index(&var1); 
    BuilderCode visitor = BuilderCode();
    visitor.visit(&index);
    string expected = "a";
    ASSERT_EQ(expected, visitor.getCode());
    
}

TEST_F(IndexTest, ValueSet)
{
    Name var1("a"), var2("x");
    Index index = Index(&var1);
    index.SetValue(&var2); 
    BuilderCode visitor = BuilderCode();
    visitor.visit(&index);
    string expected = "x";
    ASSERT_EQ(expected, visitor.getCode());
    
}
