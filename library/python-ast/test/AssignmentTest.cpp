#include <limits.h>
#include "gtest/gtest.h"

#include <BuilderCode.h>
#include <BuilderCodePy.h>

class AssignmentTest : public ::testing::Test 
{

};

TEST_F(AssignmentTest, simpleAssignmentWithConst)
{
    BuilderCodePy py;

    Assignment& assign = py.assign(&py.name("a"),&py.num(1));

    string expected = "a = 1";

    BuilderCode visitor = BuilderCode();
    visitor.visit(&assign);

    ASSERT_EQ(expected, visitor.getCode());
    
}

TEST_F(AssignmentTest, expressionAssignment)
{
    Number c1 = Number(1);
    Number c2 = Number(2);

    BinaryOperator add = BinaryOperator(&c1, ADD, &c2);
   
    Name nameA = Name("a");

    string expected = "a = (1+2)";
    Assignment assing = Assignment(&nameA, &add);

    BuilderCode visitor = BuilderCode();
    visitor.visit(&assing);

    ASSERT_EQ(expected, visitor.getCode());
    
}

TEST_F(AssignmentTest, simpleAssignmentWithNameAndExpression)
{
    Number c2 = Number(2);
    Number c3 = Number(1);

    Name nameA = Name("a");
    Name nameB = Name("b");

    BinaryOperator mul = BinaryOperator(&c2, MULT, &c3);

    string expected = "a = (b+(2*1))";

    BinaryOperator add = BinaryOperator(&nameB, ADD, &mul);

    Assignment assing = Assignment(&nameA, &add);

    BuilderCode visitor = BuilderCode();

    visitor.visit(&assing);
    ASSERT_EQ(expected, visitor.getCode());
}



TEST_F(AssignmentTest, simpleAssignmentOfBoolOperatorToName)
{

    Number c2 = Number(2);
    Number c3 = Number(1);

    Name nameA = Name("a");
    Name nameB = Name("b");

    BinaryOperator mul = BinaryOperator(&c2, MULT, &c3);

    string expected = "a = (2*1) == b";

    Compare eq(&mul,Eq, &nameB);


    Assignment assing = Assignment(&nameA, &eq);

    BuilderCode visitor = BuilderCode();

    visitor.visit(&assing);
    ASSERT_EQ(expected, visitor.getCode());
}



TEST_F(AssignmentTest, assignmentMethodToName)
{

    Number c1 = Number(1);
    Number c2 = Number(2);

    Name nameA = Name("a");

    FunctionDef method = FunctionDef("suma");
    CallFunction call = method.Call();
    call.addArgument(&c1).addArgument(&c2);

    string expected = "a = suma(1, 2)";

    Assignment assing = Assignment(&nameA, &call);

    BuilderCode visitor = BuilderCode();

    visitor.visit(&assing);
    ASSERT_EQ(expected, visitor.getCode());
}

TEST_F(AssignmentTest, multipleAssignmentToNames)
{

    Number c1 = Number(1);
    Number c2 = Number(2);
    Number c3 = Number(3);

    Name nameA = Name("a");
    Name nameB = Name("b");
    Name nameC = Name("c");

    Tuple tuple(&c1, &c2);
    tuple.AddExpression(&c3);

    string expected = "a, b, c = 1, 2, 3";

    Assignment assing = Assignment(&nameA, &tuple);
    assing.AddTarget(&nameB);
    assing.AddTarget(&nameC);

    BuilderCode visitor = BuilderCode();

    visitor.visit(&assing);
    ASSERT_EQ(expected, visitor.getCode());
}
