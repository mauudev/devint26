cmake_minimum_required(VERSION 2.6)
set(NAME_PROJECT python-ast)
project(${NAME_PROJECT})


add_compile_options(-std=c++11)

set(libreria 

	./source/Assignment.cpp
	./source/BinaryOperator.cpp
	./source/BoolOp.cpp
	./source/BuilderCode.cpp
	./source/NodeAST.cpp
	./source/Number.cpp
	./source/FunctionDef.cpp
	./source/ClassDef.cpp
	./source/Concat.cpp
	./source/ParameterList.cpp
	./source/Return.cpp
	./source/CallFunction.cpp
	./source/While.cpp
	./source/For.cpp
	./source/Import.cpp
	./source/ImportFrom.cpp
	./source/If.cpp
	./source/Break.cpp
	./source/PythonScriptGenerator.cpp
	./source/Print.cpp
	./source/StringExp.cpp
	./source/With.cpp
	./source/Dictionary.cpp
	./source/List.cpp
	./source/Name.cpp
	./source/InsertFun.cpp
	./source/Attribute.cpp
	./source/PythonInjector.cpp
	./source/KeyWord.cpp
	./source/Slice.cpp
	./source/Index.cpp
	./source/ExtSlice.cpp
	./source/UnaryOp.cpp
	./source/Subscript.cpp
	./source/Compare.cpp
	./source/Module.cpp
	./source/Global.cpp
	./source/Tuple.cpp
	./source/AugAssign.cpp
	./source/BuilderCodePy.cpp
	./source/KeyGen.cpp
)


add_library(python-ast STATIC
    ${libreria}
)

# Specify here the include directories exported
# by this library
target_include_directories(python-ast PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/include

)