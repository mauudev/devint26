#ifndef TUPLE_H_
#define TUPLE_H_ 

#include <Expression.h>
#include <Visitor.h>
#include <string>


class Tuple : public Expression
{
public:

	Tuple();

	Tuple(Expression* exprOne, Expression* exprTwo);

	Tuple& AddExpression(Expression* expression);
		
	string toString() override;

	void accept(Visitor& visitor) override;
};

#endif