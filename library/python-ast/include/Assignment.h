#ifndef ASSIGNMENT_H_
#define ASSIGNMENT_H_

#include <Statement.h>
#include <Expression.h>
#include <Visitor.h>

class Assignment : public Statement
{
  string _equals = " = ";
  Expression* _value;

public:
  Assignment();

  Assignment(Expression* target, Expression* value);

  string getEquals();

  Assignment& AddTarget(Expression* target);

  Expression* GetExpressionList();

  string toString() override;

  void accept(Visitor &visitor) override;
};

#endif