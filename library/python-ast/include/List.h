#ifndef LIST_H_
#define LIST_H_

#include <NodeAST.h>
#include <Expression.h>
#include <Visitor.h>
#include <utility> 
#include <string>

using namespace std;

class List: public  Expression
{

public:

	List();

	List(Expression* expression);

	List& AddExpression(Expression* expression);

	string toString() override;
	
  	void accept(Visitor &visitor) override;

};

#endif