#ifndef BUILDERCODEPY_H_
#define BUILDERCODEPY_H_ 

#include <unordered_map>
#include <PythonAST.h>
#include <KeyGen.h>

using namespace std;


class BuilderCodePy
{
	unordered_map<string,Name> _nameMap; 
	unordered_map<string,InsertFun> _insertFunMap;
	unordered_map<string,Number> _numMap;
	unordered_map<string,Assignment> _assignMap;
	unordered_map<string,FunctionDef> _funcMap;
	unordered_map<string,ClassDef> _classMap;
	unordered_map<string,Concat> _concatMap;
	unordered_map<string,List> _listMap;
	unordered_map<string,For> _forMap;
	unordered_map<string,Attribute> _attrMap;
	unordered_map<string,CallFunction> _callMap;
	unordered_map<string,Subscript> _subsMap;
	unordered_map<string,Slice> _sliceMap;
	unordered_map<string,ExtSlice> _extSliceMap;
	unordered_map<string,Index> _indexMap;
	unordered_map<string,Return> _retMap;
	unordered_map<string,If> _ifMap;
	unordered_map<string,Print> _printMap;
	unordered_map<string,Break> _breakMap;
	unordered_map<string,Dictionary> _dictMap;
	unordered_map<string,StringExp> _strMap;
	unordered_map<string,Tuple> _tupleMap;
	unordered_map<string,BoolOp> _boolOpMap;
	unordered_map<string,BinaryOperator> _binOpMap;
	unordered_map<string,UnaryOp> _unaryOpMap;
	unordered_map<string,Compare> _compareMap;
	unordered_map<string,KeyWord> _keyWordMap;
	unordered_map<string,While> _whileMap;
	unordered_map<string,With> _withMap;
	unordered_map<string,Import> _importMap;
	unordered_map<string,ImportFrom> _importFromMap;
	unordered_map<string,Global> _globalMap;
	unordered_map<string,AugAssign> _augAssignMap;

	KeyGen keyGen;
	BuilderCode visitor;
	Module _mod;

public:
	Name& name(string name);
	InsertFun& ins(string insertFun);
	Number& num(int v);
	Number& num(double v);
	Number& num(float v);
	Assignment& assign(Expression* l, Expression* r);
	FunctionDef& functionDef(string name);
	ClassDef& classDef(string name);
	Concat& concat();
	List& list();
	List& list(Expression* e);
	For& forStmt(Expression* e, Expression* eIt);
	Attribute& attribute(Expression* v, string a);
	CallFunction& call(Expression* func);
	Subscript& subscript(Expression* v, SliceType* s);
	Slice& slice(Expression* l, Expression* u);
	ExtSlice& extSlice(SliceType* s);
	Index& index(Expression* v);
	Return& returnStmt(Expression* e);
	If& ifStmt(Expression* e);
	Print& print();
	Print& print(Expression* e, Expression* n);
	Break& breakStmt();
	Dictionary& dict(Expression* k, Expression* v);
	StringExp& str(string str);
	Tuple& tuple(Expression* exprOne, Expression* exprTwo);
	BoolOp& boolOp(Expression* l, Bool op, Expression* r);
	BinaryOperator& binOp(Expression* l, Operator op, Expression* r);
	UnaryOp& unaryOp(UnaryOpType u, Expression* o);
	Compare& compare(Expression* l, cmpOp op, Expression* c);
	KeyWord& keyword(const string& a, Expression* v);
	While& whileStmt(Expression* test);
	With& withStmt(Expression* e, string a);
	With& withStmt(Expression* e);
	Import&	import(string m, string a);
	Import& import(string m);
	ImportFrom& importFrom(string m);
	ImportFrom& importFrom(string rm, string i);
	ImportFrom& importFrom(string rm, string i, string a);
	Global& global(Name* n);
	AugAssign& augAssign(Expression* t, Operator o, Expression* v);
	
	BuilderCodePy& addCode(Statement* s);
	BuilderCodePy& addCode(Expression* e);
	string generateCode();
	string generateCode(Statement* s);
	string generateCode(Expression* e);
	
};

#endif