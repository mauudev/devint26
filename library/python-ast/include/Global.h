#ifndef GLOBAL_H_
#define GLOBAL_H_ 

#include <Statement.h>
#include <Visitor.h>
#include <Name.h>

class Global: public Statement
{
	vector<Name*> _list;

public:

	Global();

	Global(Name* name);

	vector<Name*> getList();

	Global& addGlobal(Name* name);

	string toString()override;

	void accept(Visitor& visitor)override;
};

#endif