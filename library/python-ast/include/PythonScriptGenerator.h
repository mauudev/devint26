#ifndef PYTHONSCRIPTGENERATOR_H_
#define PYTHONSCRIPTGENERATOR_H_
#include <string>
#include <iostream>
#include <fstream>

using namespace std;

class PythonScriptGenerator
{
	const char* _extension = ".py";
public:
	PythonScriptGenerator();
	void GenerateScript(const char* nameScript, const char* pathScript, string codeScript);
};

#endif