
enum Operator
{
    ADD,
    SUB,
    MULT,
    DIV,
    MOD,
    POW,
    LSHIFT,
    RSHIFT,
    BITOR,
    BITXOR,
    BITAND,
    FLOORDIV
};