#ifndef INDEX_H_
#define INDEX_H_

#include <SliceType.h>
#include <Expression.h>
#include <Visitor.h>
#include <string>

using namespace std;

class Index : public SliceType 
{

  Expression* _value;

public:

  Index();

  Index(Expression* value);

  Index& SetValue(Expression* value);

  Expression* GetValue();

  string toString() override;
  
  void accept(Visitor &visitor) override;
};

#endif