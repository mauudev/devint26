#ifndef BREAK_H_
#define BREAK_H_

#include <Statement.h>
#include <Visitor.h>

using namespace std;

class Break : public Statement
{
	string _break = "break";

public:

	Break();

	string toString()override;

	void accept(Visitor& visitor)override;;	
};

#endif