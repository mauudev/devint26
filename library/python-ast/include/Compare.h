#ifndef COMPARE_H_
#define COMPARE_H_ 

#include <Expression.h>
#include <vector>
#include <Visitor.h>

#define Eq cmpOp::EQ
#define NotEq cmpOp::NOTEQ
#define Lt cmpOp::LT
#define LtE cmpOp::LTE
#define Gt cmpOp::GT
#define GtE cmpOp::GTE
#define Is cmpOp::IS
#define IsNot cmpOp::ISNOT
#define In cmpOp::IN
#define NotIn cmpOp::NOTIN


enum cmpOp{EQ, NOTEQ, LT, LTE, GT, GTE, IS, ISNOT, IN, NOTIN};

static string cmpOpNames[10]{"==","!=","<","<=",">",">=","is","is not","in","not in"};

class Compare : public Expression
{
	Expression* _left;
	vector<cmpOp> _ops;
	vector<Expression*> _comparators;
public:

	Compare();

	Compare(Expression* left, vector<cmpOp> ops, vector<Expression*> comparators);

	Compare(Expression* left, cmpOp op, Expression* comparator);

	string getCmpOpName(cmpOp op);

	Expression* getLeft();

	vector<cmpOp> getOps();

	vector<Expression*> getComparators();

	string toString() override;

	void accept(Visitor& Visitor) override;
};

#endif