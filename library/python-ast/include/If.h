#ifndef IF_H_
#define IF_H_

#include <NodeAST.h>
#include <Expression.h>
#include <Statement.h>
#include <Visitor.h>
#include <Break.h>
#include <Return.h>
#include <string>

class CallFunction;

class If : public  Statement 
{
  Expression* _test;
  vector<NodeAST*> _body;
  vector<NodeAST*> _orelse;

public:

  If();

  If(Expression* test);

  If& addIfNode(Statement* body);

  If& addIfNode(Expression* body);

  If& addElifNode(Statement* orelse);

  If& addElifNode(Expression* orelse);

  Expression* GetExpression();

  vector<NodeAST*> GetBodyList();
  
  vector<NodeAST*> GetElif();

  string toString() override;

  void accept(Visitor& visitor) override;
  
};

#endif