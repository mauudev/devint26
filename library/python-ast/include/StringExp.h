#ifndef STRINGEXP_H_
#define STRINGEXP_H_

#include <Expression.h>
#include <Visitor.h>
#include <string>

using namespace std;

class StringExp : public Expression 
{
  string _string;

public:

  StringExp();

  StringExp(string str);

  string GetString();

  string toString() override;
  
  void accept(Visitor &visitor) override;
};

#endif