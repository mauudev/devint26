#ifndef BINARYOPERATOR_H_
#define BINARYOPERATOR_H_

#include <Expression.h>
#include <Visitor.h>
#include <string>
#include <Operator.h>
#include <PythonAST.h>

using namespace std;

class BinaryOperator : public Expression 
{

    string _op;
    Expression* _left;
    Expression* _right;

public:

  BinaryOperator();

  BinaryOperator(Expression* left, Operator op, Expression* right);

  void returnOp(Operator op);

  string getBinOpe();

  Expression* getLeft();

  Expression* getRight();

  string toString() override;

  void accept(Visitor &visitor) override;
  
};

#endif