#ifndef INSERTFUN_H_
#define INSERTFUN_H_

#include <Expression.h>
#include <Visitor.h>
#include <string>

using namespace std;


class InsertFun : public Expression 
{

private:

	vector<string> lines;
	string _pythonCode;
	int _numberLines;

public:

	InsertFun();

	InsertFun(string text);

	int getNumberLines();

	vector<string> getLines();

	string getCode();

	string toString() override;
	
  	void accept(Visitor &visitor) override;

private:

	void splitLines();

};

#endif