#ifndef DICTIONARY_H_
#define DICTIONARY_H_

#include <NodeAST.h>
#include <Expression.h>
#include <Visitor.h>
#include <utility> 
#include <string>

using namespace std;

class Dictionary: public  Expression
{

private:

	vector<pair<Expression*, Expression*>> _keyList;

public:

	Dictionary();

	Dictionary(Expression* key, Expression* value);

	Dictionary& AddKey(Expression* key, Expression* value);

	vector<pair<Expression*, Expression*>> GetKeyList();

	int sizeKeyList();

	string toString() override;
	
  	void accept(Visitor &visitor) override;

};

#endif