#ifndef BUILDERCODE_H_
#define BUILDERCODE_H_

#include <PythonAST.h>
#include <Visitor.h>
#include <BinaryOperator.h>
#include <Number.h>
#include <Assignment.h>
#include <BoolOp.h>
#include <FunctionDef.h>
#include <ClassDef.h>
#include <Concat.h>
#include <ParameterList.h>
#include <CallFunction.h>
#include <While.h>
#include <For.h>
#include <If.h>
#include <Print.h>
#include <StringExp.h>
#include <Import.h>
#include <ImportFrom.h>
#include <Return.h>
#include <Dictionary.h>
#include <Name.h>
#include <InsertFun.h>
#include <Attribute.h>
#include <string>
#include <PythonInjector.h>
#include <With.h>
#include <List.h>
#include <PythonInjector.h>
#include <KeyWord.h>
#include <Slice.h>
#include <Index.h>
#include <ExtSlice.h>
#include <Subscript.h>
#include <Compare.h>
#include <Break.h>
#include <Module.h>
#include <Global.h>
#include <Tuple.h>
#include <AugAssign.h>

using namespace std;

class BuilderCode : public Visitor{
private:
  string _code;
  int _tab=0;

  void visitChildNodes(BinaryOperator* bin);

  void visitChildNodes(Assignment* assing);

  void visitChildNodes(FunctionDef* funDef);

  void visitChildNodes(ClassDef* classDef);

  void visitChildNodes(Concat* concat);

  void visitChildNodes(BoolOp* boolOp);

  void visitChildNodes(ParameterList* parameterList); 

  void visitChildNodes(While* w);

  void visitChildNodes(If* ifDef);

  void visitChildNodes(For* f);

  void visitChildNodes(Print* print);

  void visitChildNodes(vector<NodeAST*> list);

  void visitChildNodes(With* w);

  void visitChildNodes(Dictionary* dict);

  void visitChildNodes(List* list);

  void visitChildNodes(ExtSlice* slice);

  void visitElse(vector<NodeAST*> bodyElse);

  string generateTabs();

  string generateTab();

public:
  BuilderCode();

  void visit(BinaryOperator* bin) override;

  void visit(Number* num) override;

  void visit(Assignment* assing) override;

  void visit(BoolOp* boolOp) override;

  void visit(FunctionDef* funDef) override;

  void visit(ClassDef* classDef) override;

  void visit(Concat* concat) override;

  void visit(ParameterList* parameterList) override;

  void visit(CallFunction* callFunction) override;

  void visit(While* w) override;

  void visit(For* f) override;

  void visit(If* ifDef) override;

  void visit(Print* print) override;

  void visit(StringExp* str) override;  

  void visit(Import* import) override;

  void visit(ImportFrom* importFrom) override;

  void visit(Return* ret) override;

  void visit(With* with) override;

  void visit(Dictionary* dict) override;

  void visit(List* list) override;

  void visit(Name* name) override;

  void visit(InsertFun* insertFun) override;

  void visit(Attribute* attribute) override;
  
  void visit(PythonInjector* python) override;

  void visit(UnaryOp* u) override;

  void visit(KeyWord* keyWord) override;

  void visit(Slice* slice) override;

  void visit(Index* index) override;

  void visit(ExtSlice* slice) override;

  void visit(Subscript* subscript) override;

  void visit(Compare* compare) override;

  void visit(Break* brk) override;

  void visit(Module* module) override;

  void visit(Global* global) override;
  
  void visit(Tuple* tuple) override;  

  void visit(AugAssign* ugAssign) override;

  string getCode();
  
  void clearCode();
};

#endif