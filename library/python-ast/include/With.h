#ifndef WITH_H_
#define WITH_H_

#include <NodeAST.h>
#include <Expression.h>
#include <Statement.h>
#include <utility> 
#include <Visitor.h>
#include <string>

using namespace std;

class With: public  Statement
{

private:
	vector<pair<Expression*, string>> _exprList;
public:

	With();

	With(Expression* expr, string alias);

	With(Expression* expr);

	With& addExpr(Expression* expr, string alias);

	With& addExpr(Expression* expr);

	With& addBody(Statement* statement);

	With& addBody(Expression* expressions);

	vector<pair<Expression*, string>> GetExprList();

	string toString() override;
	
  	void accept(Visitor &visitor) override;

private:
	void Add(Expression* expr, string alias);

};

#endif