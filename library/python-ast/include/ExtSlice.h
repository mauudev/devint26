#ifndef EXTSLICE_H_
#define EXTSLICE_H_

#include <SliceType.h>
#include <Expression.h>
#include <Visitor.h>
#include <string>

using namespace std;

class ExtSlice : public SliceType 
{

public:
	
  ExtSlice();

  ExtSlice(SliceType* slice);

  ExtSlice& AddSlice(SliceType* slice);

  string toString() override;
  
  void accept(Visitor &visitor) override;
};

#endif