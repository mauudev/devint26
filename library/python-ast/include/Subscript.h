#ifndef SUBSCRIPT_H_
#define SUBSCRIPT_H_

#include <SliceType.h>
#include <Expression.h>
#include <Visitor.h>
#include <string>

using namespace std;

class Subscript : public Expression 
{

  Expression* _value;
  SliceType* _slice;

public:

  Subscript();

  Subscript(Expression* value, SliceType* slice);

  Subscript& SetValue(Expression* value);

  Subscript& SetSlice(SliceType* slice);

  Expression* GetValue();

  SliceType* GetSlice();

  string toString() override;
  
  void accept(Visitor &visitor) override;
};

#endif