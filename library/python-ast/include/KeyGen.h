#ifndef KEYGEN_H_
#define KEYGEN_H_
#include <string>

using namespace std;

class KeyGen
{
	int d,c;
public:
	KeyGen();
	string generateKey();
};

#endif