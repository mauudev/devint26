#ifndef IMPORT_H_
#define IMPORT_H_

#include <NodeAST.h>
#include <Expression.h>
#include <Statement.h>
#include <utility> 
#include <Visitor.h>
#include <string>

using namespace std;

class Import: public  Statement
{

private:
	vector<pair<string, string>> _importList;
public:

	Import();

	Import(string module, string alias);

	Import(string module);

	Import& addImport(string module, string alias);

	Import& addImport(string module);

	vector<pair<string, string>> getImportList();

	string toString() override;
	
  	void accept(Visitor &visitor) override;

private:

	void addModule(string module, string alias);

};

#endif