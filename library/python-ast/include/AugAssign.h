#ifndef AUGASSIGN_H_
#define AUGASSIGN_H_

#include <Expression.h>
#include <Visitor.h>
#include <string>
#include <Operator.h>

using namespace std;

static string _operators[12]{"+=","-=","*=","/=","%=","&=","|=","^=","<<=",">>=","**=", "//="};

class AugAssign : public Expression 
{

    Operator _op;
    Expression* _target;
    Expression* _value;

public: 

  AugAssign();

  AugAssign(Expression* target, Operator op, Expression* value);

  string getOp();

  Expression* getTarget();

  Expression* getValue();

  string toString() override;

  void accept(Visitor &visitor) override;
  
};

#endif