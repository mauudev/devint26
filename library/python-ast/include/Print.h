#ifndef PRINT_H_
#define PRINT_H_

#include <Expression.h>
#include <Statement.h>
#include <Visitor.h>
#include <ParameterList.h>
#include <string>

class Print : public Statement
{
    string _chevron = "";
    bool _newLine = true;
    ParameterList _parameterList;

public:
    Print();

    Print(Expression* leftExpression, Expression* rightExpression);

    Print& AddExpression(Expression* expression);

    Print& AddParameter(Expression* parameter);

    string GetChevron();

    bool GetNewLine();

    void GetParameters(Visitor &visitor);

    int GetParameterSize();

    Expression* GetExpression();

    void NoNewLine();

    string toString() override;

    void accept(Visitor &visitor) override;
};

#endif