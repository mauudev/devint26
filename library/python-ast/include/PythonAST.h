#ifndef PYTHONAST_H_
#define PYTHONAST_H_

#include <NodeAST.h>
#include <Visitor.h>
#include <BuilderCode.h>
#include <Assignment.h>
#include <BuilderCode.h>
#include <BoolOp.h>
#include <BinaryOperator.h>
#include <Number.h>
#include <FunctionDef.h>
#include <ClassDef.h>
#include <Concat.h>
#include <ParameterList.h>
#include <Return.h>
#include <CallFunction.h>
#include <While.h>
#include <For.h>
#include <Import.h>
#include <ImportFrom.h>
#include <If.h>
#include <Break.h>
#include <PythonScriptGenerator.h>
#include <Print.h>
#include <StringExp.h>
#include <With.h>
#include <Dictionary.h>
#include <List.h>
#include <Name.h>
#include <InsertFun.h>
#include <Attribute.h>
#include <PythonInjector.h>
#include <KeyWord.h>
#include <Slice.h>
#include <Index.h>
#include <ExtSlice.h>
#include <UnaryOp.h>
#include <Module.h>
#include <Tuple.h>
#include <Operator.h>


//OPERADORES UNARIOS
#define NOT    UnaryOpType::Not
#define INVERT UnaryOpType::Invert
#define UADD   UnaryOpType::UAdd
#define USUB   UnaryOpType::USub

////////////
#define ADD Operator::Add
#define SUB Operator::Sub
#define MULT Operator::Mult
#define DIV Operator::Div
#define MOD Operator::Mod
#define POW Operator::Pow
#define LShift Operator::LSHIFT
#define RShift Operator::RSHIFT
#define BitOr Operator::BITOR
#define BitXor Operator::BITXOR
#define BitAnd Operator::BITAND
#define FloorDiv Operator::FLOORDIV

#endif