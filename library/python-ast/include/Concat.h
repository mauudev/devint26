#ifndef CONCAT_H
#define CONCAT_H

#include <NodeAST.h>
#include <Visitor.h>
#include <Return.h>
#include <string>
#include <CallFunction.h>
#include <Expression.h>
#include <Statement.h>


class Concat : public Statement {
protected:
  friend class CallFunction;
  vector<NodeAST*> _body;
  ParameterList _paramList;
  bool _isEmpty;

public:

  Concat(); 

  Concat& addBodyNode(Statement* statementNode);

  Concat& addBodyNode(Expression* expressionNode);

  string NoBody(); 

  string toString() override;

  void accept(Visitor& visitor) override;

  int SizeBodyList();

  vector<NodeAST*> GetBodyList();

  void addParameters(ParameterList paramList);

  CallFunction Call();

  CallFunction Call(Expression* func);

  ParameterList& getParameters();
  
};

#endif