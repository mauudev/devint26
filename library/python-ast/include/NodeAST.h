#ifndef NODEAST_H_
#define NODEAST_H_

#include <Node.h>
#include <string>
#include <iostream>
#include <vector>

using namespace std;

class NodeAST : public Node
{
public:
  vector<NodeAST*> list;

  NodeAST();

  vector<NodeAST*> getChildList();

  void addChild(NodeAST* node); 

  int  sizeChildList(); 
  
  void printChildList();
};

#endif