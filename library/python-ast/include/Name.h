#ifndef NAME_H_
#define NAME_H_ 

#include <Expression.h>
#include <Visitor.h>

class Name: public Expression
{
	string _id;	
public:
	Name();

	Name(string id);

	void setId(string id);

	string toString()override;

	void accept(Visitor& visitor)override;
};

#endif