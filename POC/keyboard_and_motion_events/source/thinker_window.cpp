#include <iostream>
#include "thinker_window.h"

ThinkerWindow::ThinkerWindow()
:m_labelInfo("Press \n CTRL LEFT and RIGHT CLICK \n on the windows \n and move with he button pushed. \n You will resize this window."),
m_mixedEventHandler{false}
{
    ThinkerWindow::initWindow();
    ThinkerWindow::loadFrame();

    m_frame.add(m_labelInfo);

    signal_event().connect(
        sigc::mem_fun(*this, &ThinkerWindow::windowsEventHandler),
        false
    );

    signal_key_release_event().connect(
        sigc::mem_fun(*this, &ThinkerWindow::windowEventKeyRelease),
        false
    );

    signal_key_press_event().connect(
        sigc::mem_fun(*this, &ThinkerWindow::windowEventKeyPress),
        false
    );

    signal_motion_notify_event().connect(
        sigc::mem_fun(*this, &ThinkerWindow::windowEventMotion),
        false
    );

    signal_button_press_event().connect(
        sigc::mem_fun(*this, &ThinkerWindow::windowEventButton),
        false
    );

    show_all_children();

}

bool ThinkerWindow::windowEventKeyRelease(GdkEventKey *p_event)
{
    m_mixedEventHandler = false;
    std::cout << "---> Some keyboard release" << "\n";
    return false;
}

bool ThinkerWindow::windowEventKeyPress(GdkEventKey *p_event)
{
    m_mixedEventHandler = false;

    if (p_event->type == GDK_KEY_PRESS &&
        p_event->keyval == GDK_KEY_Control_L )
    {
        m_mixedEventHandler = true;
    }

    std::cout << "---> Key pressed: " << p_event->hardware_keycode
        << "\n";
    return false;
}

bool ThinkerWindow::windowEventMotion(GdkEventMotion *p_event)
{

    if (!m_mixedEventHandler)
    {
        std::cout << "---> Not resized, press CTRL Left to make a change"
            << "\n";
        return false;
    }

    int widthWindow = this->get_allocated_width();
    int heightWindow = this->get_allocated_height();

    int xPointer = p_event->x + 3;
    int yPointer = p_event->y + 3;

    this->set_size_request(xPointer, yPointer);

    std::cout << "---> Resized window to -> W:"
        << xPointer << " H:" << yPointer <<"\n";
    std::cout << "---> Window -> X:"
        << widthWindow << " Y:" << heightWindow <<"\n";

    return true;
}

bool ThinkerWindow::windowEventButton(GdkEventButton *p_event)
{
    std::cout << "---> Some mouse key clicked" << "\n";
    return false;
}

bool ThinkerWindow::windowsEventHandler(GdkEvent *p_event)
{
    std::cout << "---> Event Fired" << "\n";
    return false;
}

ThinkerWindow::~ThinkerWindow()
{
}

void ThinkerWindow::initWindow()
{
    set_title("Thinker");
    set_border_width(10);
    set_default_size(150, 200);
}

void ThinkerWindow::loadFrame()
{
    add(m_frame);
    m_frame.set_shadow_type(Gtk::SHADOW_ETCHED_OUT);
}
