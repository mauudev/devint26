#include <iostream>
#include <gtkmm.h>
#include "thinker_window.h"

int main(int argc, char *argv[])
{
    std::cout << "--->  Keyboard and Motion Events" << "\n";

    Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(
        argc,
        argv,
        "org.fundacion-jala.thinker"
    );

    ThinkerWindow window;

    return app->run(window);
}
