#ifndef THINKER_WINDOW_H
#define THINKER_WINDOW_H

#include <gtkmm.h>

class ThinkerWindow : public Gtk::Window
{
private:
    bool m_mixedEventHandler;
    void initWindow();
    void loadFrame();

protected:
    Gtk::Frame m_frame;
    Gtk::Label m_labelInfo;

public:
    ThinkerWindow();
    virtual ~ThinkerWindow();
    bool windowEventKeyPress(GdkEventKey *p_event);
    bool windowEventKeyRelease(GdkEventKey *p_event);
    bool windowEventMotion(GdkEventMotion *p_event);
    bool windowEventButton(GdkEventButton *p_event);
    bool windowsEventHandler(GdkEvent *p_event);

};

#endif //THINKER_WINDOW_H
