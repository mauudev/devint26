#include "ExampleWindow.h"
#include "RemoveElementExample.h"
#include <iostream>

using namespace std;

ExampleWindow::ExampleWindow(): m_vBoxMain(Gtk::ORIENTATION_VERTICAL),
    m_vBox(Gtk::ORIENTATION_VERTICAL),
    m_frameHorizontal("Horizontal Button Boxes"),
    m_frameVertical("Vertical Button Boxes")
{
    set_title("Add & Remove elements example");
    
    add(m_vBoxMain);

    m_vBoxMain.pack_start(m_frameHorizontal, Gtk::PACK_EXPAND_WIDGET, 10);

    m_vBox.set_border_width(10);
    m_frameHorizontal.add(m_vBox);

    ExampleButtonBox *newElement = new ExampleButtonBox(true, 
        "Spread (spacing 40)", 40,Gtk::BUTTONBOX_SPREAD);

    m_vBox.pack_start(*Gtk::manage(newElement),Gtk::PACK_EXPAND_WIDGET);

    show_all_children();
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_button_clicked()
{
    hide();
}
