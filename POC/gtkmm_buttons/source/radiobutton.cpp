#include "radiobox.h"
#include <iostream>

RadioButton::RadioButton(const std::string &p_label)
:m_radioButton{p_label}
{
}

RadioButton::~RadioButton()
{
}

void RadioButton::onRadioButtonClicked() const
{
    std::cout << "The Radio "<< m_radioButton.get_label() << " change state: "
    << (m_radioButton.get_active() ? "true" : "false") <<"\n";
}

Gtk::RadioButton &RadioButton::getInstance()
{
    return m_radioButton;
}