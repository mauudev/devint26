#include "button.h"
#include <iostream>

Button::Button(const std::string &p_label)
:m_button{p_label}
{
}

Button::~Button()
{
}

void Button::onButtonClicked() const
{
    std::cout << "The " << m_button.get_label() << " was clicked.\n";
}

Gtk::Button &Button::getInstance()
{
    return m_button;
}