#ifndef FILE_CHOOSER_BUTTON_H
#define FILE_CHOOSER_BUTTON_H

#include <gtkmm.h>
#include <string>

class FileChooserButton
{
    Gtk::FileChooserButton m_fileChooserButton;
public:
    FileChooserButton(const std::string &);
    ~FileChooserButton();
    Gtk::FileChooserButton &getInstance();
};

#endif