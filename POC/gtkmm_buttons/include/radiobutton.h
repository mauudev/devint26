#ifndef RADIO_BUTTON_H
#define RADIO_BUTTON_H

#include <gtkmm/radiobutton.h>
#include <string>

class RadioButton
{
    Gtk::RadioButton m_radioButton;
public:
    RadioButton(const std::string &);
    ~RadioButton();
    void onRadioButtonClicked() const;
    Gtk::RadioButton &getInstance();
};

#endif