#ifndef RADIO_BOX_H
#define RADIO_BOX_H

#include <gtkmm/box.h>
#include "radiobutton.h"

class RadioBox
{
    Gtk::Box m_box;
public:
    RadioBox();
    ~RadioBox();
    void packStart(RadioButton &);
    void joinGroup(RadioButton &, RadioButton &);
    Gtk::Box &getBox();
};

#endif