#ifndef CHECK_BUTTON_H
#define CHECK_BUTTON_H

#include <gtkmm/checkbutton.h>
#include <string>

class CheckButton
{
    Gtk::CheckButton m_checkbutton;
public:
    CheckButton(const std::string &);
    ~CheckButton();
    void onCheckbuttonClicked() const;
    Gtk::CheckButton &getInstance();
};

#endif