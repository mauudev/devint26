#ifndef WINDOW_H
#define WINDOW_H

#include <gtkmm.h>
#include "button.h"
#include "checkbutton.h"
#include "filechooserbutton.h"
#include "radiobutton.h"
#include "radiobox.h"
#include "togglebutton.h"
#include "linkbutton.h"

class Window:public Gtk::Window
{
    Gtk::Grid m_grid;
    Gtk::Grid m_grid1;
    Gtk::Grid m_grid2;
    Button m_buttonClose{"QUIT"};
    Button m_button{"Normal Button"};
    CheckButton m_checkButton{"Check"};
    FileChooserButton m_fileChooserButton{"Choose"};
    RadioButton m_radioButton1{"Button 1"};
    RadioButton m_radioButton2{"Button 2"};
    RadioButton m_radioButton3{"Button 3"};
    RadioBox m_radioBox;
    ToggleButton m_toggleButton{"Toogle Button"};
    LinkButton m_linkButton{"https://www.google.com/","LinkButton"};
public:
    Window();
    virtual ~Window();
    void onButtonClosed();
};

#endif