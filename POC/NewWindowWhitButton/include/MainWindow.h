#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H
#include <gtkmm/window.h>
#include <gtkmm/button.h>
#include <gtkmm/box.h>
#include <gtkmm/application.h>
#include "NewWindow.h"
class MainWindow :public Gtk::Window
{
public:
    MainWindow();
    virtual ~MainWindow();

protected:
    void onButtonClicked();
    void closeNewWindow();
    Gtk::Button m_button1; 
    Gtk::Label m_label;
    Gtk::Box m_box;
    NewWindow *m_newWindow;
};
#endif