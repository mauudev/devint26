#ifndef NEW_WINDOW_H
#define NEW_WINDOW_H
#include <gtkmm.h>
class NewWindow : public Gtk::Window
{
public:
    NewWindow();
    ~NewWindow();

protected:
    Gtk::Label m_label;
};
#endif
