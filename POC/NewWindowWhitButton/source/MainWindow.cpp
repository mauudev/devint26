#include "MainWindow.h"
MainWindow::MainWindow()
{
    this->set_default_size(500, 500);
    this->set_border_width(300);
    this->add(m_box);
    m_box.pack_start(m_label);
    m_box.set_orientation(Gtk::ORIENTATION_VERTICAL);
    m_label.set_label("first Window");
    m_button1.set_label("Open New Window");
    m_button1.signal_clicked()
        .connect(sigc::mem_fun(*this, &MainWindow::onButtonClicked));
    m_box.pack_start(m_button1);
    m_newWindow = 0;
    this->show_all_children();
}

MainWindow::~MainWindow()
{
}
void MainWindow::onButtonClicked()
{
    if(m_newWindow != 0)
    {
        return;
    }
    m_newWindow = new NewWindow;
    m_newWindow->signal_hide()
        .connect(sigc::mem_fun(*this, &MainWindow::closeNewWindow));
    m_newWindow->show();
}
void MainWindow::closeNewWindow()
{
    m_newWindow = 0;
}