#include "MainWindow.h"
#include <iostream>
#include <gtkmm.h>
int main(int argc, char *argv[])
{
    auto app = Gtk::Application::create(argc, argv, "new window");
    MainWindow p_mainWindow;
    // Shows the window and returns when it is closed.
    return app->run(p_mainWindow);
}