#include <gtkmm/application.h>
#include "examplewindow.h"

int main(int argc, char *argv[])
{
    Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(argc, argv);
    ExampleWindow window;
    return app->run(window);
}
