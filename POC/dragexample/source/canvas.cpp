#include <iostream>
#include "canvas.h"

Canvas::Canvas()
:m_dragDataRequestedForDrop(false), m_dropItem(nullptr)
{
    set_app_paintable();
}

Canvas::~Canvas()
{
    while(!m_canvasItems.empty())
    {
        std::vector<Canvas::CanvasItem*>::iterator iter = m_canvasItems
            .begin();
        Canvas::CanvasItem* item = *iter;
        delete item;
        m_canvasItems.erase(iter);
    }

    delete m_dropItem;
}

void Canvas::itemDraw(const CanvasItem *p_item,
    const Cairo::RefPtr<Cairo::Context> &p_cairo,
    bool p_preview)
{
    if(!p_item || !p_item->m_pixbuf)
        return;

    const double cairoX = p_item->m_pixbuf->get_width();
    const double cairoY = p_item->m_pixbuf->get_height();

    Gdk::Cairo::set_source_pixbuf(p_cairo, p_item->m_pixbuf,
    p_item->m_x - cairoX * 0.5, p_item->m_y - cairoY * 0.5);

    if(p_preview)
    {
        p_cairo->paint_with_alpha(0.6);
    }
    else
    {
        p_cairo->paint();
    }
}

bool Canvas::on_draw(const Cairo::RefPtr<Cairo::Context>& p_cairo)
{
    p_cairo->set_source_rgb(1.0, 1.0, 1.0);
    const Gtk::Allocation allocation = get_allocation();
    p_cairo->rectangle(0, 0, allocation.get_width(), allocation.get_height());
    p_cairo->fill();

    for(typeVecItems::iterator iterator = m_canvasItems.begin();
        iterator != m_canvasItems.end(); ++iterator )
    {
        itemDraw(*iterator, p_cairo, false);
    }

    if(m_dropItem)
    {
        itemDraw (m_dropItem, p_cairo, true);
    }

    return true;
}

bool Canvas::on_drag_motion(const Glib::RefPtr<Gdk::DragContext> &p_context,
    int p_x,
    int p_y,
    guint p_time)
{
    m_dragDataRequestedForDrop = false;

    if(m_dropItem)
    {
        m_dropItem->m_x = p_x;
        m_dropItem->m_y = p_y;
        queue_draw();
        p_context->drag_status(Gdk::ACTION_COPY, p_time);
    }
    else
    {
        const Glib::ustring target = drag_dest_find_target(p_context);
        if (target.empty())
        {
            return false;
        }

        drag_get_data(p_context, target, p_time);
    }

    Gtk::DrawingArea::on_drag_motion(p_context, p_x, p_y, p_time);
    return true;
}


void Canvas::on_drag_data_received(
    const Glib::RefPtr<Gdk::DragContext> &p_context, 
    int p_x, 
    int p_y, 
    const Gtk::SelectionData& p_selectionData,
    guint p_info, guint p_time)
{
    Gtk::Widget* widget = drag_get_source_widget(p_context);

    Gtk::ToolPalette *drag_palette = dynamic_cast<Gtk::ToolPalette*>(widget);
    while(widget && !drag_palette)
    {
        widget = widget->get_parent();
        drag_palette = dynamic_cast<Gtk::ToolPalette*>(widget);
    }

    Gtk::ToolItem *drag_item = nullptr;
    if(drag_palette)
    {
        drag_item = drag_palette->get_drag_item(p_selectionData);
    }

    Gtk::ToolButton *button = dynamic_cast<Gtk::ToolButton*>(drag_item);
    if(button)
    {
        delete m_dropItem;
        m_dropItem = nullptr;
    }
    try
    {
        Canvas::CanvasItem *item = new CanvasItem(this, button, p_x, p_y);

        if(m_dragDataRequestedForDrop)
        {
            m_canvasItems.push_back(item);
            p_context->drag_finish(true, false, p_time);
        }
        else
        {
            m_dropItem = item;
            p_context->drag_status(Gdk::ACTION_COPY, p_time);
        }
        queue_draw();
    }
    catch (const Gtk::IconThemeError &errorX)
    {
        std::cerr << "IconThemeError: " << errorX.what() << std::endl;
    }

    Gtk::DrawingArea::on_drag_data_received(p_context, p_x, p_y,
        p_selectionData, p_info, p_time);
}


bool Canvas::on_drag_drop(const Glib::RefPtr<Gdk::DragContext> &p_context, 
    int p_x,
    int p_y,
    guint p_time)
{
    const Glib::ustring p_target = drag_dest_find_target(p_context);

    if (p_target.empty())
    {
        return false;
    }

    m_dragDataRequestedForDrop = true;
    drag_get_data(p_context, p_target, p_time);

    return true;
}

