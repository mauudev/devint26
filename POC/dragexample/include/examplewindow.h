#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include <gtkmm.h>
#include "canvas.h"

class ExampleWindow : public Gtk::Window
{
private:
    Gtk::Box m_vBox;
    Gtk::Box m_hBox; 
    Glib::RefPtr<Gtk::ListStore> m_refTreeModelOrientation;
    Glib::RefPtr<Gtk::ListStore> m_refTreeModelStyle;
    Gtk::ToolPalette m_toolPalette;
    Gtk::ScrolledWindow m_scrolledWindowPalette;
    Gtk::ScrolledWindow m_scrolledWindowCanvas;
    Canvas m_canvas;

    void loadIconItems();

    class ModelColumnsOrientation : public Gtk::TreeModel::ColumnRecord
    {   

    public:
        Gtk::TreeModelColumn<Gtk::Orientation> m_colValue;
        Gtk::TreeModelColumn<Glib::ustring> m_colName;
        ModelColumnsOrientation()
        { add(m_colValue); add(m_colName); }

    };
        ModelColumnsOrientation m_columnsOrientation;

   
    public:
    ExampleWindow();
    virtual ~ExampleWindow();

};

#endif 
