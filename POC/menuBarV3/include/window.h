#ifndef WINDOWS_H
#define WINDOWS_H
#include <iostream>
#include <gtkmm.h>
#include "menubar.h"
class Window: public Gtk::Window
{
public:
    Window();
    ~Window();
    Gtk::Box *vbox;
    MenuBar *menubar = new MenuBar();
    Gtk::VBox m_layout;
};
#endif

