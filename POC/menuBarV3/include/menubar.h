#ifndef MENUBAR2_H
#define MENUBAR2_H
#include <iostream>
#include <gtkmm.h>
#include <string>
using namespace std;

class MenuBar
{
protected:
    Gtk::MenuBar *m_menuBar;
    Gtk::MenuItem *m_menuItemDefault;
    Gtk::Menu *m_menuDefault;
    Gtk::Menu *m_subMenu;
public:
    MenuBar();
    ~MenuBar();
    Gtk::MenuBar *createMenuBar();
    Gtk::Menu *createMenu(const string&, Gtk::MenuBar*);
    Gtk::MenuItem *createSubmenu(Gtk::Menu*, const string&);
    void createNestedSubMenu(Gtk::MenuItem*, const string&);
};
#endif
