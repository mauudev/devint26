#include "window.h"

Window::Window()
{
    set_title("Thinker");
    set_default_size(400,400);
    vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 0));
    add(*vbox);
    Gtk::MenuBar *menuBar = menubar->createMenuBar();
    vbox->pack_start(*menuBar, Gtk::PACK_SHRINK, 0);
    Gtk::Menu *menu1 = menubar->createMenu("File",menuBar);
    Gtk::MenuItem *submenu1 = menubar->createSubmenu(menu1,"new");
    Gtk::MenuItem *submenu2 = menubar->createSubmenu(menu1,"open");
    Gtk::MenuItem *submenu3 = menubar->createSubmenu(menu1,"delete");
    Gtk::Menu *menu2 = menubar->createMenu("Edit",menuBar);
    Gtk::MenuItem *submenu4 = menubar->createSubmenu(menu2,"new");
    Gtk::Menu *menu3 = menubar->createMenu("About",menuBar);
    Gtk::MenuItem *submenu5 = menubar->createSubmenu(menu3,"option1");
    Gtk::MenuItem *submenu6 = menubar->createSubmenu(menu3,"option2");
    menubar->createNestedSubMenu(submenu6,"new");
    menubar->createNestedSubMenu(submenu6,"save");
    menubar->createNestedSubMenu(submenu6,"open");
    show_all();
}

Window::~Window()
{
    delete menubar;
}
