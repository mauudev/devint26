#include "menubar.h"

MenuBar::MenuBar()
{
    m_menuBar = new Gtk::MenuBar();
    m_menuItemDefault= new Gtk::MenuItem();
    m_menuDefault= new Gtk::Menu();
    m_subMenu = new Gtk::Menu();
}

Gtk::MenuBar *MenuBar::createMenuBar()
{
    m_menuBar = Gtk::manage(new Gtk::MenuBar());
    return m_menuBar;
}

Gtk::Menu *MenuBar::createMenu(const string &p_label, Gtk::MenuBar *p_menubar)
{
    m_menuItemDefault = Gtk::manage(new Gtk::MenuItem(p_label, true));
    p_menubar->append(*m_menuItemDefault);
    m_menuDefault = Gtk::manage(new Gtk::Menu());
    m_menuItemDefault->set_submenu(*m_menuDefault);
    return m_menuDefault;
}

void MenuBar::createNestedSubMenu(Gtk::MenuItem *p_mItem, const string &p_label)
{
     m_menuItemDefault = Gtk::manage(new Gtk::MenuItem(p_label, true));
     p_mItem->set_submenu(*m_subMenu);
     m_subMenu->append(*m_menuItemDefault);
}

Gtk::MenuItem *MenuBar::createSubmenu(Gtk::Menu *p_menu, const string &p_label)
{
    m_menuItemDefault = Gtk::manage(new Gtk::MenuItem(p_label, true));
    p_menu->append(*m_menuItemDefault);
    return m_menuItemDefault;
}

MenuBar::~MenuBar()
{
    delete m_menuBar;
    delete m_menuItemDefault;
    delete m_menuDefault;
    delete m_subMenu;
}
