#ifndef GTKMM_GUI_H
#define GTKMM_GUI_H
#include <gtkmm.h>
#include <iostream>
using namespace std;

class GUI: public Gtk::ApplicationWindow
{
protected:
    Gtk::VBox m_mainLayout;
    Gtk::MenuBar m_menuBar;
    Gtk::Menu m_subMenuFiles;
    Gtk::Menu m_subMenuEdit;
    Gtk::MenuItem m_menuFiles;
    Gtk::MenuItem m_menuEdit;
    Gtk::MenuItem m_newProject;
    Gtk::MenuItem m_openProject;
    Gtk::MenuItem m_saveProject;
    Gtk::MenuItem m_quit;
    Gtk::SeparatorMenuItem m_line;
    Gtk::MenuItem m_menuCut;
    Gtk::MenuItem m_menuCopy;
    Gtk::MenuItem m_menuPaste;
    Gtk::MenuItem m_menuQuit;
    Gtk::MenuItem m_menuExample;
    Gtk::Window *m_window;
public:
    GUI();
    ~GUI();
    void onStartUp();
    void onButtonClickedNew();
    void onButtonClickedOpen();
    void onButtonClickedSave();
    void onButtonClickedClose();
    void otherButtonClicked();
    void onButtonClickedQuit();
};
#endif
