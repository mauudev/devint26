#include "GUI.h"

GUI::GUI()
: Gtk::ApplicationWindow()
{
    set_title("Thinker");
    set_default_size(400,400);
    add(m_mainLayout);
    m_mainLayout.pack_start(m_menuBar, Gtk::PACK_SHRINK);
    GUI::onStartUp();
    show_all();
}

GUI::~GUI()
{
    delete m_window;
}

void GUI::onStartUp()
{
    m_menuFiles.set_label("Files");
    m_menuBar.append(m_menuFiles);
    m_menuFiles.set_submenu(m_subMenuFiles);
    m_newProject.set_label("New Project");
    m_newProject.signal_activate()
        .connect(sigc::mem_fun(*this, &GUI::onButtonClickedNew));
    m_subMenuFiles.append(m_newProject);
    m_openProject.set_label("Open Project");
    m_openProject.signal_activate()
        .connect(sigc::mem_fun(*this, &GUI::onButtonClickedOpen));
    m_subMenuFiles.append(m_openProject);
    m_saveProject.set_label("Save Project");
    m_saveProject.signal_activate()
        .connect(sigc::mem_fun(*this, &GUI::onButtonClickedSave));
    m_subMenuFiles.append(m_saveProject);
    m_subMenuFiles.append(m_line);
    m_menuQuit.set_label("Quit");
    m_menuQuit.signal_activate()
        .connect(sigc::mem_fun(*this, &GUI::close));
    m_subMenuFiles.append(m_menuQuit);
    m_menuEdit.set_label("Edit");
    m_menuBar.append(m_menuEdit);
    m_menuEdit.set_submenu(m_subMenuEdit);
    m_menuCut.set_label("Cut");
    m_menuCut.signal_activate()
        .connect(sigc::mem_fun(*this, &GUI::otherButtonClicked));
    m_subMenuEdit.append(m_menuCut);
    m_menuCopy.set_label("Copy");
    m_menuCopy.signal_activate()
        .connect(sigc::mem_fun(*this, &GUI::otherButtonClicked));
    m_subMenuEdit.append(m_menuCopy);
    m_menuPaste.set_label("Paste");
    m_menuPaste.signal_activate()
        .connect(sigc::mem_fun(*this, &GUI::otherButtonClicked));
    m_subMenuEdit.append(m_menuPaste);
    m_subMenuEdit.append(m_menuExample);
}

void GUI::onButtonClickedNew()
{
    cout << "new Project clicked" << '\n';
    m_window = new Gtk::Window();
    m_window->set_title("New Project");
    m_window->set_default_size(300, 150);
    m_window->show_all();
}

void GUI::onButtonClickedOpen()
{
    cout<<"open clicked\n";
    Gtk::FileChooserDialog dialog("Please choose a file",
        Gtk::FILE_CHOOSER_ACTION_OPEN);
    dialog.set_transient_for(*this);
    dialog.set_default_size(300,300);
    dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
    dialog.add_button("_Open", Gtk::RESPONSE_OK);
    Glib::RefPtr<Gtk::FileFilter> filter_text = Gtk::FileFilter::create();
    filter_text->set_name("Text files");
    filter_text->add_mime_type("text/plain");
    dialog.add_filter(filter_text);
    Glib::RefPtr<Gtk::FileFilter> filter_cpp = Gtk::FileFilter::create();
    filter_cpp->set_name("C/C++ files");
    filter_cpp->add_mime_type("text/x-c");
    filter_cpp->add_mime_type("text/x-c++");
    filter_cpp->add_mime_type("text/x-c-header");
    dialog.add_filter(filter_cpp);
    Glib::RefPtr<Gtk::FileFilter> filter_any = Gtk::FileFilter::create();
    filter_any->set_name("Any files");
    filter_any->add_pattern("*");
    dialog.add_filter(filter_any);
    int result = dialog.run();
    switch(result)
    {
        case(Gtk::RESPONSE_OK):
        {
            cout << "Open clicked." << "\n";
            string fileName = dialog.get_filename();
            cout << "File selected: " <<  fileName << "\n";
            break;
        }
        case(Gtk::RESPONSE_CANCEL):
        {
            cout << "Cancel clicked." << "\n";
            break;
        }
        default:
        {
            cout << "Unexpected button clicked." << "\n";
            break;
        }
    }
}

void GUI::onButtonClickedSave()
{
    cout<<"save clicked\n";
}

void GUI::onButtonClickedClose()
{
    cout<<"close clicked\n";
}

void GUI::otherButtonClicked()
{
    cout << "other button clicked" << '\n';
}
