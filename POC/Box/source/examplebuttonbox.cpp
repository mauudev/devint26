#include "examplebuttonbox.h"

ExampleButtonBox::ExampleButtonBox(bool p_horizontal, const 
    Glib::ustring &p_title, gint p_spacing, Gtk::ButtonBoxStyle p_layout)
: Gtk::Frame(p_title)
{
    Gtk::ButtonBox *bottonBox = nullptr;
    if(p_horizontal)
    {
        bottonBox = Gtk::manage(new Gtk::ButtonBox
            (Gtk::ORIENTATION_HORIZONTAL));
    }
    else
    {
        bottonBox = Gtk::manage(new Gtk::ButtonBox
            (Gtk::ORIENTATION_VERTICAL));
    }

    bottonBox->set_border_width(5);
    add(*bottonBox);
    bottonBox->set_layout(p_layout);
    bottonBox->set_spacing(p_spacing);
}