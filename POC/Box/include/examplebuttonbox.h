#ifndef GTKMM_EXAMPLE_BUTTONBOX_H
#define GTKMM_EXAMPLE_BUTTONBOX_H

#include <gtkmm.h>

class ExampleButtonBox : public Gtk::Frame
{
public:
    ExampleButtonBox(bool, const Glib::ustring&, gint, Gtk::ButtonBoxStyle);
};

#endif