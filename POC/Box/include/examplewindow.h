#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include <gtkmm.h>

class ExampleWindow : public Gtk::Window
{
    public:
    ExampleWindow();
    virtual ~ExampleWindow();
    Gtk::Box m_vBoxMain;
    Gtk::Box m_vBox;
    Gtk::Box m_hBox;
    Gtk::Frame m_menuBar;
    Gtk::Frame m_toolBoxCanvas;
    Gtk::Frame m_infoBar;
};

#endif