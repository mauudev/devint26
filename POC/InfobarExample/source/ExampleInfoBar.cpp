#include "ExampleInfoBar.h"

ExampleWindow::ExampleWindow()
:m_vBox{Gtk::ORIENTATION_VERTICAL,6}, 
m_buttonQuit{"_Quit", true},
m_buttonClear{"_Clear", true}, 
m_buttonClick{"_Click",true}, 
m_buttonError{"_Error",true}, 
m_buttonWarning{"_Warning", true}, 
m_buttonInfo{"_Info", true}
{
    set_title("InfoBar Example");
    set_border_width(6);
    set_default_size(550, 300);
    add(m_vBox);

    Gtk::Container *infoBarContainer = static_cast<Gtk::Container*>(
    m_infoBar.get_content_area());

    infoBarContainer->add(m_messageLabel);
    m_infoBar.add_button("_OK", 0);
    m_infoBar.add_button("_Hide", 1);

    m_vBox.pack_start(m_infoBar, Gtk::PACK_SHRINK);
    m_infoBar.set_message_type(Gtk::MESSAGE_OTHER);

    m_refTextBuffer = Gtk::TextBuffer::create();
    m_textView.set_buffer(m_refTextBuffer);

    m_scrolledWindow.add(m_textView);

    m_scrolledWindow.set_policy(Gtk::POLICY_AUTOMATIC, 
        Gtk::POLICY_AUTOMATIC);
    m_vBox.pack_start(m_scrolledWindow);
    m_vBox.pack_start(m_buttonBox, Gtk::PACK_SHRINK);

    m_buttonBox.pack_start(m_buttonClear, Gtk::PACK_SHRINK);
    m_buttonBox.pack_start(m_buttonInfo, Gtk::PACK_SHRINK);
    m_buttonBox.pack_start(m_buttonClick, Gtk::PACK_SHRINK);
    m_buttonBox.pack_start(m_buttonWarning, Gtk::PACK_SHRINK);
    m_buttonBox.pack_start(m_buttonError, Gtk::PACK_SHRINK);
    m_buttonBox.pack_start(m_buttonQuit, Gtk::PACK_SHRINK);
    m_buttonBox.set_spacing(6);
    m_buttonBox.set_layout(Gtk::BUTTONBOX_END);

    m_infoBar.signal_response().connect(sigc::mem_fun(*this, 
        &ExampleWindow::onInfobarResponse));
    m_buttonQuit.signal_clicked().connect(sigc::mem_fun(*this, 
        &ExampleWindow::onButtonQuit));
    m_buttonClear.signal_clicked().connect(sigc::mem_fun(*this, 
        &ExampleWindow::onButtonClear));
    m_refTextBuffer -> signal_changed().connect(sigc::mem_fun(*this, 
        &ExampleWindow::onTextbufferChanged));
    m_buttonClick.signal_clicked().connect(sigc::mem_fun(*this, 
        &ExampleWindow::onButtonClick));
    m_buttonError.signal_clicked().connect(sigc::mem_fun(*this, 
        &ExampleWindow::onButtonError));
    m_buttonWarning.signal_clicked().connect(sigc::mem_fun(*this, 
        &ExampleWindow::onButtonWarning));
    m_buttonInfo.signal_clicked().connect(sigc::mem_fun(*this, 
        &ExampleWindow::onButtonInfo));

    show_all();

    m_buttonClear.set_sensitive(false);
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::onInfobarResponse(int p_response)
{
    if(p_response == 0)
    {
        m_messageLabel.set_text("");
        m_infoBar.set_message_type(Gtk::MESSAGE_OTHER);
    }
    else
    {
        m_messageLabel.set_text("");
        m_infoBar.set_message_type(Gtk::MESSAGE_OTHER);
        m_infoBar.hide();
    }
}

void ExampleWindow::onButtonQuit()
{
    hide();
}

void ExampleWindow::onButtonClear()
{
    m_refTextBuffer->set_text("");
    m_messageLabel.set_text("Text area cleared");
    m_infoBar.set_message_type(Gtk::MESSAGE_INFO);
    m_infoBar.show();
}

void ExampleWindow::onButtonClick()
{
    m_messageLabel.set_text("INFO: Action sucessfully performed!");
    m_infoBar.set_message_type(Gtk::MESSAGE_INFO);
    m_infoBar.show();
}

void ExampleWindow::onButtonError()
{
    m_messageLabel.set_text("ERROR: Can not perfom this action!");
    m_infoBar.set_message_type(Gtk::MESSAGE_ERROR);
    m_infoBar.show();
}

void ExampleWindow::onButtonWarning()
{
    m_messageLabel.set_text("WARNING: Be careful with this action please!");
    m_infoBar.set_message_type(Gtk::MESSAGE_WARNING);
    m_infoBar.show();
}

void ExampleWindow::onButtonInfo()
{
    m_refTextBuffer->set_text(
        "*****  Write 'dev26' here, but before clean the window  *****");
    m_messageLabel.set_text("Info: write dev26 in a clean window");
    m_infoBar.set_message_type(Gtk::MESSAGE_INFO);
    m_infoBar.show();
}

void  ExampleWindow::onTextbufferChanged()
{
    m_infoBar.show();

    if (m_refTextBuffer->size() > 0)
    {
        m_buttonClear.set_sensitive(true);
        m_messageLabel.set_text("Writing a text");
        m_infoBar.set_message_type(Gtk::MESSAGE_OTHER);
    }
    else
    {
        m_buttonClear.set_sensitive(false);
        m_messageLabel.set_text("");
        m_infoBar.set_message_type(Gtk::MESSAGE_OTHER);
    }

    if(m_refTextBuffer->get_text() == "dev26")
    {
        m_messageLabel.set_text("Best Developers Team!!!");
        m_infoBar.set_message_type(Gtk::MESSAGE_INFO);
        m_infoBar.show();
    }
}