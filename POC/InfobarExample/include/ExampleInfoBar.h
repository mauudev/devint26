#ifndef GTKMM_EXAMPLEINFOBAR_H
#define GTKMM_EXAMPLEINFOBAR_H
#include <gtkmm.h>

class ExampleWindow : public Gtk::Window
{
protected:
    void onInfobarResponse(int);
    void onButtonQuit();
    void onButtonClear();
    void onButtonClick();
    void onButtonWarning();
    void onTextbufferChanged();
    void onButtonError();
    void onButtonInfo();

    Gtk::Box m_vBox;
    Gtk::ScrolledWindow m_scrolledWindow;
    Gtk::TextView m_textView;

    Glib::RefPtr<Gtk::TextBuffer> m_refTextBuffer;
    Gtk::InfoBar m_infoBar;
    Gtk::Label m_messageLabel;

    Gtk::ButtonBox m_buttonBox;
    Gtk::Button m_buttonQuit;
    Gtk::Button m_buttonClear;
    Gtk::Button m_buttonClick;
    Gtk::Button m_buttonWarning;
    Gtk::Button m_buttonError;
    Gtk::Button m_buttonInfo;

public:
    ExampleWindow();
    virtual ~ExampleWindow();
};

#endif