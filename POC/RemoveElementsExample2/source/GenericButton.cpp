#include <iostream>
#include "GenericButton.h"
#include <string>

using namespace std;

GenericButton::GenericButton(int p_buttonId, string p_label)
    :Gtk::ToggleButton(p_label),m_buttonId{p_buttonId}
{

}

GenericButton::~GenericButton()
{

}

int GenericButton::getButtonId()
{
    return m_buttonId;
}
