#ifndef EXAMPLE_WINDOW_H
#define EXAMPLE_WINDOW_H

#include <gtkmm.h>

class ExampleWindow : public Gtk::Window
{
public:
    ExampleWindow();
    virtual ~ExampleWindow();

protected:
    void on_button_clicked();

    Gtk::Box m_vBoxMain;
    Gtk::Box m_vBox;
    Gtk::Box m_hBox;
    Gtk::Frame m_frameHorizontal;
    Gtk::Frame m_frameVertical;
};
#endif
