#ifndef GTKMM_WINDOWMENUBAR_H
#define GTKMM_WINDOWMENUBAR_H
#include <iostream>
#include <gtkmm.h>
#include "menubar.h"

class WindowMenuBar
{
public:
    WindowMenuBar();
    ~WindowMenuBar();
    Gtk::Box *vbox;
    MenuBar *menubar = new MenuBar();
    Gtk::VBox m_layout;
};
#endif
