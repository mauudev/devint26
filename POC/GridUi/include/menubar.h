#ifndef MENUBAR2_H
#define MENUBAR2_H
#include <iostream>
#include <gtkmm.h>
#include <string>
using namespace std;

class MenuBar
{
protected:
    Gtk::MenuBar *menubar;
    Gtk::MenuItem *menuitemDefault;
    Gtk::Menu *menuDefault;
    Gtk::Grid m_grid;

public:
    MenuBar();
    ~MenuBar();
    Gtk::MenuBar* createMenuBar();
    Gtk::Menu* createMenu(const string&, Gtk::MenuBar*);
    Gtk::Grid& getGridMenuBar();

    void createNestedSubMenu(Gtk::Menu*, const string&);
};
#endif
