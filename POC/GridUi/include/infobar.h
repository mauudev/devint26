#ifndef INFOBAR_H
#define INFOBAR_H
#include <gtkmm/infobar.h>
#include <gtkmm/label.h>
#include <string>

class InfoBar
{
private:
    Gtk::InfoBar m_infoBar;
    Gtk::Label m_messageLabel;
public:
    InfoBar();
    virtual ~InfoBar();
    void onInfobarResponse(int);
    void setMessage(const std::string &,Gtk::MessageType &);
    Gtk::InfoBar &getInstance();
};

#endif