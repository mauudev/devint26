#ifndef CANVAS_TOOL_BOX_H
#define CANVAS_TOOL_BOX_H
#include<gtkmm.h>
#include "ActionTool.h"

class CanvasToolBox:public Gtk::Box
{
private:
    ActionTool m_actionToolSelect;
    ActionTool m_actionToolRemove;
    ActionTool m_actionToolSelectArea;
    ActionTool m_actionToolLupa;
    ActionTool m_actionToolLink;

public:
    CanvasToolBox();
    // bool getStateActionTool(const std::string& p_tool);
    // void changeStateActionTool(const std::string& p_tool);
};

#endif