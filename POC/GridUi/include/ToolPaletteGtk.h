#ifndef GTKMM_TOOLPALETTE_H
#define GTKMM_TOOLPALETTE_H
#include <gtkmm.h>

class ToolPaletteGtk
{
private:
    Gtk::ToolPalette m_ToolPalette;
public:
    ToolPaletteGtk();
    ~ToolPaletteGtk();
    void loadItems();
    Gtk::ToolPalette &getToolPalette();

};


#endif