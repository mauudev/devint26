#include "Canvas.h"
#include <iostream>

Canvas::Canvas()
: m_drag_data_requested_for_drop(false),
  m_drop_item(nullptr)
{
    set_app_paintable();
}

Canvas::~Canvas()
{
    while(!m_canvas_items.empty())
    {
        auto iter = m_canvas_items.begin();
        auto item = *iter;
        delete item;
        m_canvas_items.erase(iter);
    }
    delete m_drop_item;
}

void Canvas::item_draw(const CanvasItem *p_item,
        const Cairo::RefPtr<Cairo::Context>& p_cr,
        bool p_preview)
{
    if(!p_item || !p_item->m_pixbuf)
        return;

    const double cx = p_item->m_pixbuf->get_width();
    const double cy = p_item->m_pixbuf->get_height();

    Gdk::Cairo::set_source_pixbuf(p_cr,
        p_item->m_pixbuf,
        p_item->m_x - cx * 0.5, p_item->m_y - cy * 0.5);

    if(p_preview)
    {
        p_cr->paint_with_alpha(0.6);
    }
    else
    {
       p_cr->paint();
    }
}

bool Canvas::on_draw(const Cairo::RefPtr<Cairo::Context>& p_cr)
{
    p_cr->set_source_rgb(1.0, 1.0, 1.0);
    const Gtk::Allocation allocation = get_allocation();
    p_cr->rectangle(0, 0, allocation.get_width(), allocation.get_height());
    p_cr->fill();

    for(type_vec_items::iterator iter = m_canvas_items.begin();
        iter != m_canvas_items.end(); 
        ++iter)
    {
        item_draw(*iter, p_cr, false);
    }

    if(m_drop_item)
    {
        item_draw (m_drop_item, p_cr, true);
    }
    return true;
}

bool Canvas::on_drag_motion(const Glib::RefPtr<Gdk::DragContext>& p_context,
  int p_x, int p_y, guint p_time)
{
    m_drag_data_requested_for_drop = false; 
    if(m_drop_item)
    {
        m_drop_item->m_x = p_x;
        m_drop_item->m_y = p_y;
        queue_draw();
        p_context->drag_status(Gdk::ACTION_COPY, p_time);
    }
    else
    {
        const Glib::ustring target = drag_dest_find_target(p_context);
        if (target.empty())
        {
            return false;
        }
        drag_get_data(p_context, target, p_time);
    }
    Gtk::DrawingArea::on_drag_motion(p_context, p_x, p_y, p_time);
    return true;
}


void Canvas::on_drag_data_received(const Glib::RefPtr<Gdk::DragContext>& p_context,
  int x, int y, const Gtk::SelectionData& p_selectionData, guint info, guint time)
{
    auto widget = drag_get_source_widget(p_context);

    auto drag_palette = dynamic_cast<Gtk::ToolPalette*>(widget);
    while(widget && !drag_palette)
    {
      widget = widget->get_parent();
      drag_palette = dynamic_cast<Gtk::ToolPalette*>(widget);
    }

    Gtk::ToolItem* drag_item = nullptr;
    if(drag_palette)
      drag_item = drag_palette->get_drag_item(p_selectionData);

    // Create a drop indicator when a tool button was found:
    auto button = dynamic_cast<Gtk::ToolButton*>(drag_item);
    if(!button)
      return;

    delete m_drop_item;
    m_drop_item = nullptr;

    try
    {
      auto item = new CanvasItem(this, button, x, y);

      if(m_drag_data_requested_for_drop)
      {
        m_canvas_items.push_back(item);

        // Signal that the item was accepted and then redraw.
        p_context->drag_finish(true /* success */, false /* del */, time);
      }
      else
      {
        m_drop_item = item;

        // We are getting this data due to a request in drag_motion,
        // rather than due to a request in drag_drop, so we are just
        // supposed to call gdk_drag_status (), not actually paste in
        // the data.
        p_context->drag_status(Gdk::ACTION_COPY, time);
      }

      queue_draw();
    }
    catch (const Gtk::IconThemeError& ex)
    {
      std::cerr << "IconThemeError: " << ex.what() << std::endl;
    }

    Gtk::DrawingArea::on_drag_data_received(p_context, x, y, p_selectionData, info, time);
}


bool Canvas::on_drag_drop(const Glib::RefPtr<Gdk::DragContext>& context, int /* x */, int /* y */, guint time)
{
  // Request DnD data for creating a dopped item.
  // This will cause on_drag_data_received() to be called.
  const Glib::ustring target = drag_dest_find_target(context);

  if (target.empty())
    return false;

  m_drag_data_requested_for_drop = true;
  drag_get_data(context, target, time);

  return true;
}

void Canvas::on_drag_leave(const Glib::RefPtr<Gdk::DragContext>& context, guint time)
{
  if(!m_drop_item)
    return;

  delete m_drop_item;
  m_drop_item = nullptr;


  queue_draw();

  Gtk::DrawingArea::on_drag_leave(context, time);
}