#include "ActionTool.h"

ActionTool::ActionTool(){}
ActionTool::ActionTool(const std::string& m_file)
:ToggleButton(),m_img{m_file}
{
    this->set_image(m_img);
}

bool ActionTool::isActive()
{
    return get_active();
}

void ActionTool::changeStatus()
{
    if(get_active())
    {
        set_active(false);
    }
    else
    {
        set_active(true);
    }
}