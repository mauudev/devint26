#include "GridWindow.h"
#include <gtkmm/application.h>
#include <gtkmm.h>

int main(int argc, char *argv[])
{
  auto app = Gtk::Application::create(argc, argv, "org.gtkmm.example");

  GridWindow window;

  return app->run(window);
}