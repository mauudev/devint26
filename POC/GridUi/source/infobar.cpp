#include "infobar.h"
#include <iostream>

InfoBar::InfoBar()
{

    Gtk::Container *infoBarContainer = static_cast<Gtk::Container*>(
    m_infoBar.get_content_area());

    infoBarContainer->add(m_messageLabel);
    m_infoBar.add_button("Hide", 0);
    m_infoBar.set_message_type(Gtk::MESSAGE_WARNING);

    m_messageLabel.set_text("MOCKUP's Demostration Example");

    m_infoBar.signal_response().connect(sigc::mem_fun(*this, 
        &InfoBar::onInfobarResponse));
}

InfoBar::~InfoBar()
{
}

void InfoBar::onInfobarResponse(int p_response)
{
    m_messageLabel.set_text("");
    m_infoBar.set_message_type(Gtk::MESSAGE_OTHER);
    m_infoBar.hide();
}

void InfoBar::setMessage(const std::string &p_label, Gtk::MessageType &p_message)
{
    m_infoBar.show();
    m_messageLabel.set_text(p_label);
    m_infoBar.set_message_type(p_message);
}

Gtk::InfoBar &InfoBar::getInstance()
{
    return m_infoBar;
}