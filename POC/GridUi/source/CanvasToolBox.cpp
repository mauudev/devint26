#include"CanvasToolBox.h"
#include "ActionTool.h"

CanvasToolBox::CanvasToolBox()
:Gtk::Box(Gtk::ORIENTATION_VERTICAL, 2),
m_actionToolSelect{"../resources/cursor.svg"},
m_actionToolRemove{"../resources/empty_trash.svg"},
m_actionToolSelectArea{"../resources/add_row.svg"},
m_actionToolLupa{"../resources/search.svg"},
m_actionToolLink{"../resources/serial_tasks.svg"}
{
    pack_start(m_actionToolSelect);
    pack_start(m_actionToolRemove);
    pack_start(m_actionToolSelectArea);
    pack_start(m_actionToolLupa);
    pack_start(m_actionToolLink);
}

// bool CanvasToolBox::getStateActionTool(const std::string& p_tool)
// {
//     switch()
//     {
        
//     }
// }
// void CanvasToolBox::getStateActionTool(const std::string& p_tool)
// {

// }