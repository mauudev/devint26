#include <cairomm/context.h>
#include <giomm/resource.h>
#include <gdkmm/general.h>
#include <glibmm/fileutils.h>
#include <iostream>
#include"drawing_canvas.h"

DrawingCanvas::DrawingCanvas()
:m_flag{0},m_axisX{0},m_axisY{0},m_axisW{50},m_axisZ{50},
    m_imageFlag1{0},m_imageFlag2{0}
{
    add_events(Gdk::BUTTON_PRESS_MASK | Gdk::BUTTON_RELEASE_MASK | 
        Gdk::POINTER_MOTION_MASK);

    try
    {
        m_image1 = 
            Gdk::Pixbuf::create_from_file("../resources/devil-icon.png",50,50);
        m_image2 = 
            Gdk::Pixbuf::create_from_file("../resources/hellboy.png",50,50);
        m_axisXWidth = m_axisX + 50;
        m_axisYHeight = m_axisY + 50;
        m_axisWWidth = m_axisW + 50;
        m_axisZHeight = m_axisZ + 50;
    }
    catch(const Gio::ResourceError& p_error)
    {
        std::cerr << "ResourceError: " << p_error.what() << std::endl;
    }
    catch(const Gdk::PixbufError& p_error)
    {
        std::cerr << "PixbufError: " << p_error.what() << std::endl;
    }
}

DrawingCanvas::~DrawingCanvas()
{
}

bool DrawingCanvas::on_draw(const Cairo::RefPtr<Cairo::Context>& p_cairo)
{
    if(!m_image1)
    {
        return false;
    }

    Gdk::Cairo::set_source_pixbuf(p_cairo, m_image1,m_axisX ,m_axisY );
    p_cairo->paint();
    Gdk::Cairo::set_source_pixbuf(p_cairo, m_image2,m_axisW ,m_axisZ);
    p_cairo->paint();

    return true;
}

bool DrawingCanvas::on_button_press_event(GdkEventButton *p_event)
{
    if(p_event->button == 1)
    {
        m_flag = true;

        int xMouse = p_event->x;
        int yMouse = p_event->y;

        if(xMouse >= m_axisX && xMouse <= m_axisXWidth && 
            yMouse >= m_axisY && yMouse <= m_axisYHeight )
        {
            m_imageFlag1 = true;
        }

        if(xMouse >= m_axisW && xMouse <= m_axisWWidth && 
            yMouse >= m_axisZ && yMouse <= m_axisZHeight )
        {
            m_imageFlag2 = true;

        }
        return true;
    }
    return false;
}

bool DrawingCanvas::on_button_release_event(GdkEventButton *p_event)
{
    if (p_event->button == 1)
    {
        m_flag = false;

        if(m_imageFlag1)
        {
            m_imageFlag1 = false;
        }

        if(m_imageFlag2)
        {
            m_imageFlag2 = false;
        }

        return true;
    }
    return false;
}

bool DrawingCanvas::on_motion_notify_event(GdkEventMotion*p_event)
{
    if(m_flag)
    {
        int xMouse = p_event->x;
        int yMouse = p_event->y;		

        if(m_imageFlag1)
        {
            m_axisX = xMouse;
            m_axisXWidth = m_axisX + 50;
            m_axisY = yMouse;
            m_axisYHeight = m_axisY + 50;
        }

        if(m_imageFlag2)
        {
            m_axisW = xMouse;
            m_axisWWidth = m_axisW + 50;
            m_axisZ = yMouse;
            m_axisZHeight = m_axisZ + 50;
        }
        queue_draw();
    }
    return true;
}