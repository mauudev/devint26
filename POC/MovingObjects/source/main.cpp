#include <gtkmm/application.h>
#include "window_canvas.h"
#include "drawing_canvas.h"

int main(int argc, char **argv)
{
    Glib::RefPtr<Gtk::Application> app =
        Gtk::Application::create(argc, argv, "");
    WindowCanvas winCanvas;
    DrawingCanvas drawCanvas;
    winCanvas.add(drawCanvas);
    drawCanvas.show();
    return app->run(winCanvas);
}