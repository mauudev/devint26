#ifndef GTKMM_DRAWING_CANVAS_H
#define GTKMM_DRAWING_CANVAS_H

#include <gtkmm/drawingarea.h>
#include <gdkmm/pixbuf.h>

class DrawingCanvas:public Gtk::DrawingArea
{
private:
    bool m_flag;
    bool m_imageFlag1;
    bool m_imageFlag2;
    int m_axisX;
    int m_axisY;
    int m_axisXWidth;
    int m_axisYHeight;
    int m_axisW;
    int m_axisZ;
    int m_axisWWidth;
    int m_axisZHeight;

protected:
    bool on_draw(const Cairo::RefPtr<Cairo::Context>& p_cairo) override;

    bool on_button_press_event(GdkEventButton *p_event);
    bool on_button_release_event(GdkEventButton *p_event);
    bool on_motion_notify_event(GdkEventMotion *p_event);

    Glib::RefPtr<Gdk::Pixbuf> m_image1;
    Glib::RefPtr<Gdk::Pixbuf> m_image2;

public:
    DrawingCanvas();
    virtual ~DrawingCanvas();
};

#endif