# This is the CMakeCache file.
# For build in directory: /home/maudev/Escritorio/devint26/bin
# It was generated by CMake: /usr/bin/cmake
# You can edit this file to change values found and used by cmake.
# If you do not want to change any of the values, simply exit the editor.
# If you do want to change a value, simply edit, save, and exit the editor.
# The syntax for the file is as follows:
# KEY:TYPE=VALUE
# KEY is the name of a variable in the cache.
# TYPE is a hint to GUIs for the type of VALUE, DO NOT EDIT TYPE!.
# VALUE is the current value for the KEY.

########################
# EXTERNAL cache entries
########################

//Builds the googlemock subproject
BUILD_GMOCK:BOOL=ON

//Builds the googletest subproject
BUILD_GTEST:BOOL=OFF

//Build shared libraries (DLLs).
BUILD_SHARED_LIBS:BOOL=OFF

//Path to a program.
CMAKE_AR:FILEPATH=/usr/bin/ar

//Choose the type of build, options are: None(CMAKE_CXX_FLAGS or
// CMAKE_C_FLAGS used) Debug Release RelWithDebInfo MinSizeRel.
CMAKE_BUILD_TYPE:STRING=

//Enable/Disable color output during build.
CMAKE_COLOR_MAKEFILE:BOOL=ON

//CXX compiler
CMAKE_CXX_COMPILER:FILEPATH=/usr/bin/c++

//A wrapper around 'ar' adding the appropriate '--plugin' option
// for the GCC compiler
CMAKE_CXX_COMPILER_AR:FILEPATH=/usr/bin/gcc-ar-7

//A wrapper around 'ranlib' adding the appropriate '--plugin' option
// for the GCC compiler
CMAKE_CXX_COMPILER_RANLIB:FILEPATH=/usr/bin/gcc-ranlib-7

//Flags used by the compiler during all build types.
CMAKE_CXX_FLAGS:STRING=

//Flags used by the compiler during debug builds.
CMAKE_CXX_FLAGS_DEBUG:STRING=-g

//Flags used by the compiler during release builds for minimum
// size.
CMAKE_CXX_FLAGS_MINSIZEREL:STRING=-Os -DNDEBUG

//Flags used by the compiler during release builds.
CMAKE_CXX_FLAGS_RELEASE:STRING=-O3 -DNDEBUG

//Flags used by the compiler during release builds with debug info.
CMAKE_CXX_FLAGS_RELWITHDEBINFO:STRING=-O2 -g -DNDEBUG

//C compiler
CMAKE_C_COMPILER:FILEPATH=/usr/bin/cc

//A wrapper around 'ar' adding the appropriate '--plugin' option
// for the GCC compiler
CMAKE_C_COMPILER_AR:FILEPATH=/usr/bin/gcc-ar-7

//A wrapper around 'ranlib' adding the appropriate '--plugin' option
// for the GCC compiler
CMAKE_C_COMPILER_RANLIB:FILEPATH=/usr/bin/gcc-ranlib-7

//Flags used by the compiler during all build types.
CMAKE_C_FLAGS:STRING=

//Flags used by the compiler during debug builds.
CMAKE_C_FLAGS_DEBUG:STRING=-g

//Flags used by the compiler during release builds for minimum
// size.
CMAKE_C_FLAGS_MINSIZEREL:STRING=-Os -DNDEBUG

//Flags used by the compiler during release builds.
CMAKE_C_FLAGS_RELEASE:STRING=-O3 -DNDEBUG

//Flags used by the compiler during release builds with debug info.
CMAKE_C_FLAGS_RELWITHDEBINFO:STRING=-O2 -g -DNDEBUG

//Generate debug library name with a postfix.
CMAKE_DEBUG_POSTFIX:STRING=d

//Flags used by the linker.
CMAKE_EXE_LINKER_FLAGS:STRING=

//Flags used by the linker during debug builds.
CMAKE_EXE_LINKER_FLAGS_DEBUG:STRING=

//Flags used by the linker during release minsize builds.
CMAKE_EXE_LINKER_FLAGS_MINSIZEREL:STRING=

//Flags used by the linker during release builds.
CMAKE_EXE_LINKER_FLAGS_RELEASE:STRING=

//Flags used by the linker during Release with Debug Info builds.
CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO:STRING=

//Enable/Disable output of compile commands during generation.
CMAKE_EXPORT_COMPILE_COMMANDS:BOOL=OFF

//User executables (bin)
CMAKE_INSTALL_BINDIR:PATH=bin

//Read-only architecture-independent data (DATAROOTDIR)
CMAKE_INSTALL_DATADIR:PATH=

//Read-only architecture-independent data root (share)
CMAKE_INSTALL_DATAROOTDIR:PATH=share

//Documentation root (DATAROOTDIR/doc/PROJECT_NAME)
CMAKE_INSTALL_DOCDIR:PATH=

//C header files (include)
CMAKE_INSTALL_INCLUDEDIR:PATH=include

//Info documentation (DATAROOTDIR/info)
CMAKE_INSTALL_INFODIR:PATH=

//Object code libraries (lib)
CMAKE_INSTALL_LIBDIR:PATH=lib

//Program executables (libexec)
CMAKE_INSTALL_LIBEXECDIR:PATH=libexec

//Locale-dependent data (DATAROOTDIR/locale)
CMAKE_INSTALL_LOCALEDIR:PATH=

//Modifiable single-machine data (var)
CMAKE_INSTALL_LOCALSTATEDIR:PATH=var

//Man documentation (DATAROOTDIR/man)
CMAKE_INSTALL_MANDIR:PATH=

//C header files for non-gcc (/usr/include)
CMAKE_INSTALL_OLDINCLUDEDIR:PATH=/usr/include

//Install path prefix, prepended onto install directories.
CMAKE_INSTALL_PREFIX:PATH=/usr/local

//Run-time variable data (LOCALSTATEDIR/run)
CMAKE_INSTALL_RUNSTATEDIR:PATH=

//System admin executables (sbin)
CMAKE_INSTALL_SBINDIR:PATH=sbin

//Modifiable architecture-independent data (com)
CMAKE_INSTALL_SHAREDSTATEDIR:PATH=com

//Read-only single-machine data (etc)
CMAKE_INSTALL_SYSCONFDIR:PATH=etc

//Path to a program.
CMAKE_LINKER:FILEPATH=/usr/bin/ld

//Path to a program.
CMAKE_MAKE_PROGRAM:FILEPATH=/usr/bin/make

//Flags used by the linker during the creation of modules.
CMAKE_MODULE_LINKER_FLAGS:STRING=

//Flags used by the linker during debug builds.
CMAKE_MODULE_LINKER_FLAGS_DEBUG:STRING=

//Flags used by the linker during release minsize builds.
CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL:STRING=

//Flags used by the linker during release builds.
CMAKE_MODULE_LINKER_FLAGS_RELEASE:STRING=

//Flags used by the linker during Release with Debug Info builds.
CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO:STRING=

//Path to a program.
CMAKE_NM:FILEPATH=/usr/bin/nm

//Path to a program.
CMAKE_OBJCOPY:FILEPATH=/usr/bin/objcopy

//Path to a program.
CMAKE_OBJDUMP:FILEPATH=/usr/bin/objdump

//Value Computed by CMake
CMAKE_PROJECT_NAME:STATIC=tests

//Path to a program.
CMAKE_RANLIB:FILEPATH=/usr/bin/ranlib

//Flags used by the linker during the creation of dll's.
CMAKE_SHARED_LINKER_FLAGS:STRING=

//Flags used by the linker during debug builds.
CMAKE_SHARED_LINKER_FLAGS_DEBUG:STRING=

//Flags used by the linker during release minsize builds.
CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL:STRING=

//Flags used by the linker during release builds.
CMAKE_SHARED_LINKER_FLAGS_RELEASE:STRING=

//Flags used by the linker during Release with Debug Info builds.
CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO:STRING=

//If set, runtime paths are not added when installing shared libraries,
// but are added when building.
CMAKE_SKIP_INSTALL_RPATH:BOOL=NO

//If set, runtime paths are not added when using shared libraries.
CMAKE_SKIP_RPATH:BOOL=NO

//Flags used by the linker during the creation of static libraries.
CMAKE_STATIC_LINKER_FLAGS:STRING=

//Flags used by the linker during debug builds.
CMAKE_STATIC_LINKER_FLAGS_DEBUG:STRING=

//Flags used by the linker during release minsize builds.
CMAKE_STATIC_LINKER_FLAGS_MINSIZEREL:STRING=

//Flags used by the linker during release builds.
CMAKE_STATIC_LINKER_FLAGS_RELEASE:STRING=

//Flags used by the linker during Release with Debug Info builds.
CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO:STRING=

//Path to a program.
CMAKE_STRIP:FILEPATH=/usr/bin/strip

//If this value is on, makefiles will be generated without the
// .SILENT directive, and all commands will be echoed to the console
// during the make.  This is useful for debugging only. With Visual
// Studio IDE projects all commands are done without /nologo.
CMAKE_VERBOSE_MAKEFILE:BOOL=FALSE

//Enable installation of googlemock. (Projects embedding googlemock
// may want to turn this OFF.)
INSTALL_GMOCK:BOOL=ON

//Enable installation of googletest. (Projects embedding googletest
// may want to turn this OFF.)
INSTALL_GTEST:BOOL=ON

//pkg-config executable
PKG_CONFIG_EXECUTABLE:FILEPATH=/usr/bin/pkg-config

//Path to a program.
PYTHON_EXECUTABLE:FILEPATH=/usr/bin/python

//Path to a file.
X11_ICE_INCLUDE_PATH:PATH=/usr/include

//Path to a library.
X11_ICE_LIB:FILEPATH=/usr/lib/x86_64-linux-gnu/libICE.so

//Path to a file.
X11_SM_INCLUDE_PATH:PATH=/usr/include

//Path to a library.
X11_SM_LIB:FILEPATH=/usr/lib/x86_64-linux-gnu/libSM.so

//Path to a file.
X11_X11_INCLUDE_PATH:PATH=/usr/include

//Path to a library.
X11_X11_LIB:FILEPATH=/usr/lib/x86_64-linux-gnu/libX11.so

//Path to a file.
X11_XRes_INCLUDE_PATH:PATH=X11_XRes_INCLUDE_PATH-NOTFOUND

//Path to a library.
X11_XRes_LIB:FILEPATH=X11_XRes_LIB-NOTFOUND

//Path to a file.
X11_XShm_INCLUDE_PATH:PATH=/usr/include

//Path to a file.
X11_XSync_INCLUDE_PATH:PATH=/usr/include

//Path to a file.
X11_XTest_INCLUDE_PATH:PATH=/usr/include

//Path to a library.
X11_XTest_LIB:FILEPATH=/usr/lib/x86_64-linux-gnu/libXtst.so

//Path to a file.
X11_Xaccessrules_INCLUDE_PATH:PATH=X11_Xaccessrules_INCLUDE_PATH-NOTFOUND

//Path to a file.
X11_Xaccessstr_INCLUDE_PATH:PATH=/usr/include

//Path to a file.
X11_Xau_INCLUDE_PATH:PATH=/usr/include

//Path to a library.
X11_Xau_LIB:FILEPATH=/usr/lib/x86_64-linux-gnu/libXau.so

//Path to a file.
X11_Xcomposite_INCLUDE_PATH:PATH=/usr/include

//Path to a library.
X11_Xcomposite_LIB:FILEPATH=/usr/lib/x86_64-linux-gnu/libXcomposite.so

//Path to a file.
X11_Xcursor_INCLUDE_PATH:PATH=/usr/include

//Path to a library.
X11_Xcursor_LIB:FILEPATH=/usr/lib/x86_64-linux-gnu/libXcursor.so

//Path to a file.
X11_Xdamage_INCLUDE_PATH:PATH=/usr/include

//Path to a library.
X11_Xdamage_LIB:FILEPATH=/usr/lib/x86_64-linux-gnu/libXdamage.so

//Path to a file.
X11_Xdmcp_INCLUDE_PATH:PATH=/usr/include

//Path to a library.
X11_Xdmcp_LIB:FILEPATH=/usr/lib/x86_64-linux-gnu/libXdmcp.so

//Path to a library.
X11_Xext_LIB:FILEPATH=/usr/lib/x86_64-linux-gnu/libXext.so

//Path to a file.
X11_Xfixes_INCLUDE_PATH:PATH=/usr/include

//Path to a library.
X11_Xfixes_LIB:FILEPATH=/usr/lib/x86_64-linux-gnu/libXfixes.so

//Path to a file.
X11_Xft_INCLUDE_PATH:PATH=/usr/include

//Path to a library.
X11_Xft_LIB:FILEPATH=/usr/lib/x86_64-linux-gnu/libXft.so

//Path to a file.
X11_Xi_INCLUDE_PATH:PATH=/usr/include

//Path to a library.
X11_Xi_LIB:FILEPATH=/usr/lib/x86_64-linux-gnu/libXi.so

//Path to a file.
X11_Xinerama_INCLUDE_PATH:PATH=/usr/include

//Path to a library.
X11_Xinerama_LIB:FILEPATH=/usr/lib/x86_64-linux-gnu/libXinerama.so

//Path to a file.
X11_Xinput_INCLUDE_PATH:PATH=/usr/include

//Path to a library.
X11_Xinput_LIB:FILEPATH=/usr/lib/x86_64-linux-gnu/libXi.so

//Path to a file.
X11_Xkb_INCLUDE_PATH:PATH=/usr/include

//Path to a file.
X11_Xkbfile_INCLUDE_PATH:PATH=X11_Xkbfile_INCLUDE_PATH-NOTFOUND

//Path to a library.
X11_Xkbfile_LIB:FILEPATH=X11_Xkbfile_LIB-NOTFOUND

//Path to a file.
X11_Xkblib_INCLUDE_PATH:PATH=/usr/include

//Path to a file.
X11_Xlib_INCLUDE_PATH:PATH=/usr/include

//Path to a file.
X11_Xmu_INCLUDE_PATH:PATH=X11_Xmu_INCLUDE_PATH-NOTFOUND

//Path to a library.
X11_Xmu_LIB:FILEPATH=X11_Xmu_LIB-NOTFOUND

//Path to a file.
X11_Xpm_INCLUDE_PATH:PATH=X11_Xpm_INCLUDE_PATH-NOTFOUND

//Path to a library.
X11_Xpm_LIB:FILEPATH=X11_Xpm_LIB-NOTFOUND

//Path to a file.
X11_Xrandr_INCLUDE_PATH:PATH=/usr/include

//Path to a library.
X11_Xrandr_LIB:FILEPATH=/usr/lib/x86_64-linux-gnu/libXrandr.so

//Path to a file.
X11_Xrender_INCLUDE_PATH:PATH=/usr/include

//Path to a library.
X11_Xrender_LIB:FILEPATH=/usr/lib/x86_64-linux-gnu/libXrender.so

//Path to a file.
X11_Xscreensaver_INCLUDE_PATH:PATH=X11_Xscreensaver_INCLUDE_PATH-NOTFOUND

//Path to a library.
X11_Xscreensaver_LIB:FILEPATH=X11_Xscreensaver_LIB-NOTFOUND

//Path to a file.
X11_Xshape_INCLUDE_PATH:PATH=/usr/include

//Path to a file.
X11_Xt_INCLUDE_PATH:PATH=X11_Xt_INCLUDE_PATH-NOTFOUND

//Path to a library.
X11_Xt_LIB:FILEPATH=X11_Xt_LIB-NOTFOUND

//Path to a file.
X11_Xutil_INCLUDE_PATH:PATH=/usr/include

//Path to a file.
X11_Xv_INCLUDE_PATH:PATH=X11_Xv_INCLUDE_PATH-NOTFOUND

//Path to a library.
X11_Xv_LIB:FILEPATH=X11_Xv_LIB-NOTFOUND

//Path to a library.
X11_Xxf86misc_LIB:FILEPATH=X11_Xxf86misc_LIB-NOTFOUND

//Path to a library.
X11_Xxf86vm_LIB:FILEPATH=/usr/lib/x86_64-linux-gnu/libXxf86vm.so

//Path to a file.
X11_dpms_INCLUDE_PATH:PATH=/usr/include

//Path to a file.
X11_xf86misc_INCLUDE_PATH:PATH=X11_xf86misc_INCLUDE_PATH-NOTFOUND

//Path to a file.
X11_xf86vmode_INCLUDE_PATH:PATH=/usr/include

//Value Computed by CMake
gmock_BINARY_DIR:STATIC=/home/maudev/Escritorio/devint26/bin/thirdparty/googletest/googlemock

//Dependencies for the target
gmock_LIB_DEPENDS:STATIC=general;-pthread;

//Value Computed by CMake
gmock_SOURCE_DIR:STATIC=/home/maudev/Escritorio/devint26/thirdparty/googletest/googlemock

//Build all of Google Mock's own tests.
gmock_build_tests:BOOL=OFF

//Dependencies for the target
gmock_main_LIB_DEPENDS:STATIC=general;-pthread;

//Value Computed by CMake
googletest-distribution_BINARY_DIR:STATIC=/home/maudev/Escritorio/devint26/bin/thirdparty/googletest

//Value Computed by CMake
googletest-distribution_SOURCE_DIR:STATIC=/home/maudev/Escritorio/devint26/thirdparty/googletest

//Value Computed by CMake
gtest_BINARY_DIR:STATIC=/home/maudev/Escritorio/devint26/bin/thirdparty/googletest/googlemock/gtest

//Dependencies for the target
gtest_LIB_DEPENDS:STATIC=general;-pthread;

//Value Computed by CMake
gtest_SOURCE_DIR:STATIC=/home/maudev/Escritorio/devint26/thirdparty/googletest/googletest

//Build gtest's sample programs.
gtest_build_samples:BOOL=OFF

//Build all of gtest's own tests.
gtest_build_tests:BOOL=OFF

//Disable uses of pthreads in gtest.
gtest_disable_pthreads:BOOL=OFF

//Use shared (DLL) run-time lib even when Google Test is built
// as static lib.
gtest_force_shared_crt:BOOL=OFF

//Build gtest with internal symbols hidden in shared libraries.
gtest_hide_internal_symbols:BOOL=OFF

//Dependencies for the target
gtest_main_LIB_DEPENDS:STATIC=general;-pthread;general;gtest;

//Value Computed by CMake
tests_BINARY_DIR:STATIC=/home/maudev/Escritorio/devint26/bin

//Value Computed by CMake
tests_SOURCE_DIR:STATIC=/home/maudev/Escritorio/devint26

//Value Computed by CMake
thinker_BINARY_DIR:STATIC=/home/maudev/Escritorio/devint26/bin

//Value Computed by CMake
thinker_SOURCE_DIR:STATIC=/home/maudev/Escritorio/devint26


########################
# INTERNAL cache entries
########################

//ADVANCED property for variable: CMAKE_AR
CMAKE_AR-ADVANCED:INTERNAL=1
//This is the directory where this CMakeCache.txt was created
CMAKE_CACHEFILE_DIR:INTERNAL=/home/maudev/Escritorio/devint26/bin
//Major version of cmake used to create the current loaded cache
CMAKE_CACHE_MAJOR_VERSION:INTERNAL=3
//Minor version of cmake used to create the current loaded cache
CMAKE_CACHE_MINOR_VERSION:INTERNAL=10
//Patch version of cmake used to create the current loaded cache
CMAKE_CACHE_PATCH_VERSION:INTERNAL=2
//ADVANCED property for variable: CMAKE_COLOR_MAKEFILE
CMAKE_COLOR_MAKEFILE-ADVANCED:INTERNAL=1
//Path to CMake executable.
CMAKE_COMMAND:INTERNAL=/usr/bin/cmake
//Path to cpack program executable.
CMAKE_CPACK_COMMAND:INTERNAL=/usr/bin/cpack
//Path to ctest program executable.
CMAKE_CTEST_COMMAND:INTERNAL=/usr/bin/ctest
//ADVANCED property for variable: CMAKE_CXX_COMPILER
CMAKE_CXX_COMPILER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_COMPILER_AR
CMAKE_CXX_COMPILER_AR-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_COMPILER_RANLIB
CMAKE_CXX_COMPILER_RANLIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS
CMAKE_CXX_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_DEBUG
CMAKE_CXX_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_MINSIZEREL
CMAKE_CXX_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_RELEASE
CMAKE_CXX_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_RELWITHDEBINFO
CMAKE_CXX_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_COMPILER
CMAKE_C_COMPILER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_COMPILER_AR
CMAKE_C_COMPILER_AR-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_COMPILER_RANLIB
CMAKE_C_COMPILER_RANLIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS
CMAKE_C_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_DEBUG
CMAKE_C_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_MINSIZEREL
CMAKE_C_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_RELEASE
CMAKE_C_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_RELWITHDEBINFO
CMAKE_C_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//Executable file format
CMAKE_EXECUTABLE_FORMAT:INTERNAL=ELF
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS
CMAKE_EXE_LINKER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS_DEBUG
CMAKE_EXE_LINKER_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS_MINSIZEREL
CMAKE_EXE_LINKER_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS_RELEASE
CMAKE_EXE_LINKER_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO
CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXPORT_COMPILE_COMMANDS
CMAKE_EXPORT_COMPILE_COMMANDS-ADVANCED:INTERNAL=1
//Name of external makefile project generator.
CMAKE_EXTRA_GENERATOR:INTERNAL=
//Name of generator.
CMAKE_GENERATOR:INTERNAL=Unix Makefiles
//Name of generator platform.
CMAKE_GENERATOR_PLATFORM:INTERNAL=
//Name of generator toolset.
CMAKE_GENERATOR_TOOLSET:INTERNAL=
//Have function connect
CMAKE_HAVE_CONNECT:INTERNAL=1
//Have function gethostbyname
CMAKE_HAVE_GETHOSTBYNAME:INTERNAL=1
//Have symbol pthread_create
CMAKE_HAVE_LIBC_CREATE:INTERNAL=
//Have include pthread.h
CMAKE_HAVE_PTHREAD_H:INTERNAL=1
//Have function remove
CMAKE_HAVE_REMOVE:INTERNAL=1
//Have function shmat
CMAKE_HAVE_SHMAT:INTERNAL=1
//Source directory with the top level CMakeLists.txt file for this
// project
CMAKE_HOME_DIRECTORY:INTERNAL=/home/maudev/Escritorio/devint26
//ADVANCED property for variable: CMAKE_INSTALL_BINDIR
CMAKE_INSTALL_BINDIR-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_INSTALL_DATADIR
CMAKE_INSTALL_DATADIR-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_INSTALL_DATAROOTDIR
CMAKE_INSTALL_DATAROOTDIR-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_INSTALL_DOCDIR
CMAKE_INSTALL_DOCDIR-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_INSTALL_INCLUDEDIR
CMAKE_INSTALL_INCLUDEDIR-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_INSTALL_INFODIR
CMAKE_INSTALL_INFODIR-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_INSTALL_LIBDIR
CMAKE_INSTALL_LIBDIR-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_INSTALL_LIBEXECDIR
CMAKE_INSTALL_LIBEXECDIR-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_INSTALL_LOCALEDIR
CMAKE_INSTALL_LOCALEDIR-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_INSTALL_LOCALSTATEDIR
CMAKE_INSTALL_LOCALSTATEDIR-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_INSTALL_MANDIR
CMAKE_INSTALL_MANDIR-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_INSTALL_OLDINCLUDEDIR
CMAKE_INSTALL_OLDINCLUDEDIR-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_INSTALL_RUNSTATEDIR
CMAKE_INSTALL_RUNSTATEDIR-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_INSTALL_SBINDIR
CMAKE_INSTALL_SBINDIR-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_INSTALL_SHAREDSTATEDIR
CMAKE_INSTALL_SHAREDSTATEDIR-ADVANCED:INTERNAL=1
//Install .so files without execute permission.
CMAKE_INSTALL_SO_NO_EXE:INTERNAL=1
//ADVANCED property for variable: CMAKE_INSTALL_SYSCONFDIR
CMAKE_INSTALL_SYSCONFDIR-ADVANCED:INTERNAL=1
//Have library ICE
CMAKE_LIB_ICE_HAS_ICECONNECTIONNUMBER:INTERNAL=1
//ADVANCED property for variable: CMAKE_LINKER
CMAKE_LINKER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MAKE_PROGRAM
CMAKE_MAKE_PROGRAM-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS
CMAKE_MODULE_LINKER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS_DEBUG
CMAKE_MODULE_LINKER_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL
CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS_RELEASE
CMAKE_MODULE_LINKER_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO
CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_NM
CMAKE_NM-ADVANCED:INTERNAL=1
//number of local generators
CMAKE_NUMBER_OF_MAKEFILES:INTERNAL=4
//ADVANCED property for variable: CMAKE_OBJCOPY
CMAKE_OBJCOPY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_OBJDUMP
CMAKE_OBJDUMP-ADVANCED:INTERNAL=1
//Platform information initialized
CMAKE_PLATFORM_INFO_INITIALIZED:INTERNAL=1
//ADVANCED property for variable: CMAKE_RANLIB
CMAKE_RANLIB-ADVANCED:INTERNAL=1
//Path to CMake installation.
CMAKE_ROOT:INTERNAL=/usr/share/cmake-3.10
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS
CMAKE_SHARED_LINKER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS_DEBUG
CMAKE_SHARED_LINKER_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL
CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS_RELEASE
CMAKE_SHARED_LINKER_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO
CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SKIP_INSTALL_RPATH
CMAKE_SKIP_INSTALL_RPATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SKIP_RPATH
CMAKE_SKIP_RPATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS
CMAKE_STATIC_LINKER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS_DEBUG
CMAKE_STATIC_LINKER_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS_MINSIZEREL
CMAKE_STATIC_LINKER_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS_RELEASE
CMAKE_STATIC_LINKER_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO
CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STRIP
CMAKE_STRIP-ADVANCED:INTERNAL=1
//uname command
CMAKE_UNAME:INTERNAL=/bin/uname
//ADVANCED property for variable: CMAKE_VERBOSE_MAKEFILE
CMAKE_VERBOSE_MAKEFILE-ADVANCED:INTERNAL=1
//Details about finding PkgConfig
FIND_PACKAGE_MESSAGE_DETAILS_PkgConfig:INTERNAL=[/usr/bin/pkg-config][v0.29.1()]
//Details about finding PythonInterp
FIND_PACKAGE_MESSAGE_DETAILS_PythonInterp:INTERNAL=[/usr/bin/python][v2.7.15()]
//Details about finding Threads
FIND_PACKAGE_MESSAGE_DETAILS_Threads:INTERNAL=[TRUE][v()]
//Details about finding X11
FIND_PACKAGE_MESSAGE_DETAILS_X11:INTERNAL=[/usr/lib/x86_64-linux-gnu/libX11.so][/usr/include]
GLIB_CFLAGS:INTERNAL=-I/usr/include/glib-2.0;-I/usr/lib/x86_64-linux-gnu/glib-2.0/include
GLIB_CFLAGS_I:INTERNAL=
GLIB_CFLAGS_OTHER:INTERNAL=
GLIB_FOUND:INTERNAL=1
GLIB_INCLUDEDIR:INTERNAL=/usr/include
GLIB_INCLUDE_DIRS:INTERNAL=/usr/include/glib-2.0;/usr/lib/x86_64-linux-gnu/glib-2.0/include
GLIB_LDFLAGS:INTERNAL=-lglib-2.0
GLIB_LDFLAGS_OTHER:INTERNAL=
GLIB_LIBDIR:INTERNAL=/usr/lib/x86_64-linux-gnu
GLIB_LIBRARIES:INTERNAL=glib-2.0
GLIB_LIBRARY_DIRS:INTERNAL=
GLIB_LIBS:INTERNAL=
GLIB_LIBS_L:INTERNAL=
GLIB_LIBS_OTHER:INTERNAL=
GLIB_LIBS_PATHS:INTERNAL=
GLIB_PREFIX:INTERNAL=/usr
GLIB_STATIC_CFLAGS:INTERNAL=-I/usr/include/glib-2.0;-I/usr/lib/x86_64-linux-gnu/glib-2.0/include
GLIB_STATIC_CFLAGS_I:INTERNAL=
GLIB_STATIC_CFLAGS_OTHER:INTERNAL=
GLIB_STATIC_INCLUDE_DIRS:INTERNAL=/usr/include/glib-2.0;/usr/lib/x86_64-linux-gnu/glib-2.0/include
GLIB_STATIC_LDFLAGS:INTERNAL=-lglib-2.0;-pthread;-lpcre;-pthread
GLIB_STATIC_LDFLAGS_OTHER:INTERNAL=-pthread
GLIB_STATIC_LIBDIR:INTERNAL=
GLIB_STATIC_LIBRARIES:INTERNAL=glib-2.0;pcre
GLIB_STATIC_LIBRARY_DIRS:INTERNAL=
GLIB_STATIC_LIBS:INTERNAL=
GLIB_STATIC_LIBS_L:INTERNAL=
GLIB_STATIC_LIBS_OTHER:INTERNAL=
GLIB_STATIC_LIBS_PATHS:INTERNAL=
GLIB_VERSION:INTERNAL=2.56.1
GLIB_glib-2.0_INCLUDEDIR:INTERNAL=
GLIB_glib-2.0_LIBDIR:INTERNAL=
GLIB_glib-2.0_PREFIX:INTERNAL=
GLIB_glib-2.0_VERSION:INTERNAL=
GTKMM_CFLAGS:INTERNAL=-pthread;-I/usr/include/gtkmm-3.0;-I/usr/lib/x86_64-linux-gnu/gtkmm-3.0/include;-I/usr/include/atkmm-1.6;-I/usr/include/gtk-3.0/unix-print;-I/usr/include/gdkmm-3.0;-I/usr/lib/x86_64-linux-gnu/gdkmm-3.0/include;-I/usr/include/giomm-2.4;-I/usr/lib/x86_64-linux-gnu/giomm-2.4/include;-I/usr/include/pangomm-1.4;-I/usr/lib/x86_64-linux-gnu/pangomm-1.4/include;-I/usr/include/glibmm-2.4;-I/usr/lib/x86_64-linux-gnu/glibmm-2.4/include;-I/usr/include/gtk-3.0;-I/usr/include/at-spi2-atk/2.0;-I/usr/include/at-spi-2.0;-I/usr/include/dbus-1.0;-I/usr/lib/x86_64-linux-gnu/dbus-1.0/include;-I/usr/include/gtk-3.0;-I/usr/include/gio-unix-2.0/;-I/usr/include/cairo;-I/usr/include/pango-1.0;-I/usr/include/harfbuzz;-I/usr/include/pango-1.0;-I/usr/include/atk-1.0;-I/usr/include/cairo;-I/usr/include/cairomm-1.0;-I/usr/lib/x86_64-linux-gnu/cairomm-1.0/include;-I/usr/include/cairo;-I/usr/include/pixman-1;-I/usr/include/freetype2;-I/usr/include/libpng16;-I/usr/include/freetype2;-I/usr/include/libpng16;-I/usr/include/sigc++-2.0;-I/usr/lib/x86_64-linux-gnu/sigc++-2.0/include;-I/usr/include/gdk-pixbuf-2.0;-I/usr/include/libpng16;-I/usr/include/glib-2.0;-I/usr/lib/x86_64-linux-gnu/glib-2.0/include
GTKMM_CFLAGS_I:INTERNAL=
GTKMM_CFLAGS_OTHER:INTERNAL=-pthread
GTKMM_FOUND:INTERNAL=1
GTKMM_INCLUDEDIR:INTERNAL=/usr/include
GTKMM_INCLUDE_DIRS:INTERNAL=/usr/include/gtkmm-3.0;/usr/lib/x86_64-linux-gnu/gtkmm-3.0/include;/usr/include/atkmm-1.6;/usr/include/gtk-3.0/unix-print;/usr/include/gdkmm-3.0;/usr/lib/x86_64-linux-gnu/gdkmm-3.0/include;/usr/include/giomm-2.4;/usr/lib/x86_64-linux-gnu/giomm-2.4/include;/usr/include/pangomm-1.4;/usr/lib/x86_64-linux-gnu/pangomm-1.4/include;/usr/include/glibmm-2.4;/usr/lib/x86_64-linux-gnu/glibmm-2.4/include;/usr/include/gtk-3.0;/usr/include/at-spi2-atk/2.0;/usr/include/at-spi-2.0;/usr/include/dbus-1.0;/usr/lib/x86_64-linux-gnu/dbus-1.0/include;/usr/include/gtk-3.0;/usr/include/gio-unix-2.0/;/usr/include/cairo;/usr/include/pango-1.0;/usr/include/harfbuzz;/usr/include/pango-1.0;/usr/include/atk-1.0;/usr/include/cairo;/usr/include/cairomm-1.0;/usr/lib/x86_64-linux-gnu/cairomm-1.0/include;/usr/include/cairo;/usr/include/pixman-1;/usr/include/freetype2;/usr/include/libpng16;/usr/include/freetype2;/usr/include/libpng16;/usr/include/sigc++-2.0;/usr/lib/x86_64-linux-gnu/sigc++-2.0/include;/usr/include/gdk-pixbuf-2.0;/usr/include/libpng16;/usr/include/glib-2.0;/usr/lib/x86_64-linux-gnu/glib-2.0/include
GTKMM_LDFLAGS:INTERNAL=-lgtkmm-3.0;-latkmm-1.6;-lgdkmm-3.0;-lgiomm-2.4;-lpangomm-1.4;-lglibmm-2.4;-lgtk-3;-lgdk-3;-lpangocairo-1.0;-lpango-1.0;-latk-1.0;-lcairo-gobject;-lgio-2.0;-lcairomm-1.0;-lcairo;-lsigc-2.0;-lgdk_pixbuf-2.0;-lgobject-2.0;-lglib-2.0
GTKMM_LDFLAGS_OTHER:INTERNAL=
GTKMM_LIBDIR:INTERNAL=/usr/lib/x86_64-linux-gnu
GTKMM_LIBRARIES:INTERNAL=gtkmm-3.0;atkmm-1.6;gdkmm-3.0;giomm-2.4;pangomm-1.4;glibmm-2.4;gtk-3;gdk-3;pangocairo-1.0;pango-1.0;atk-1.0;cairo-gobject;gio-2.0;cairomm-1.0;cairo;sigc-2.0;gdk_pixbuf-2.0;gobject-2.0;glib-2.0
GTKMM_LIBRARY_DIRS:INTERNAL=
GTKMM_LIBS:INTERNAL=
GTKMM_LIBS_L:INTERNAL=
GTKMM_LIBS_OTHER:INTERNAL=
GTKMM_LIBS_PATHS:INTERNAL=
GTKMM_PREFIX:INTERNAL=/usr
GTKMM_STATIC_CFLAGS:INTERNAL=-pthread;-I/usr/include/gtkmm-3.0;-I/usr/lib/x86_64-linux-gnu/gtkmm-3.0/include;-I/usr/include/atkmm-1.6;-I/usr/include/gtk-3.0/unix-print;-I/usr/include/gdkmm-3.0;-I/usr/lib/x86_64-linux-gnu/gdkmm-3.0/include;-I/usr/include/giomm-2.4;-I/usr/lib/x86_64-linux-gnu/giomm-2.4/include;-I/usr/include/pangomm-1.4;-I/usr/lib/x86_64-linux-gnu/pangomm-1.4/include;-I/usr/include/glibmm-2.4;-I/usr/lib/x86_64-linux-gnu/glibmm-2.4/include;-I/usr/include/gtk-3.0;-I/usr/include/at-spi2-atk/2.0;-I/usr/include/at-spi-2.0;-I/usr/include/dbus-1.0;-I/usr/lib/x86_64-linux-gnu/dbus-1.0/include;-I/usr/include/gtk-3.0;-I/usr/include/gio-unix-2.0/;-I/usr/include/cairo;-I/usr/include/pango-1.0;-I/usr/include/harfbuzz;-I/usr/include/pango-1.0;-I/usr/include/atk-1.0;-I/usr/include/cairo;-I/usr/include/cairomm-1.0;-I/usr/lib/x86_64-linux-gnu/cairomm-1.0/include;-I/usr/include/cairo;-I/usr/include/pixman-1;-I/usr/include/freetype2;-I/usr/include/libpng16;-I/usr/include/freetype2;-I/usr/include/libpng16;-I/usr/include/sigc++-2.0;-I/usr/lib/x86_64-linux-gnu/sigc++-2.0/include;-I/usr/include/gdk-pixbuf-2.0;-I/usr/include/libpng16;-I/usr/include/glib-2.0;-I/usr/lib/x86_64-linux-gnu/glib-2.0/include
GTKMM_STATIC_CFLAGS_I:INTERNAL=
GTKMM_STATIC_CFLAGS_OTHER:INTERNAL=-pthread
GTKMM_STATIC_INCLUDE_DIRS:INTERNAL=/usr/include/gtkmm-3.0;/usr/lib/x86_64-linux-gnu/gtkmm-3.0/include;/usr/include/atkmm-1.6;/usr/include/gtk-3.0/unix-print;/usr/include/gdkmm-3.0;/usr/lib/x86_64-linux-gnu/gdkmm-3.0/include;/usr/include/giomm-2.4;/usr/lib/x86_64-linux-gnu/giomm-2.4/include;/usr/include/pangomm-1.4;/usr/lib/x86_64-linux-gnu/pangomm-1.4/include;/usr/include/glibmm-2.4;/usr/lib/x86_64-linux-gnu/glibmm-2.4/include;/usr/include/gtk-3.0;/usr/include/at-spi2-atk/2.0;/usr/include/at-spi-2.0;/usr/include/dbus-1.0;/usr/lib/x86_64-linux-gnu/dbus-1.0/include;/usr/include/gtk-3.0;/usr/include/gio-unix-2.0/;/usr/include/cairo;/usr/include/pango-1.0;/usr/include/harfbuzz;/usr/include/pango-1.0;/usr/include/atk-1.0;/usr/include/cairo;/usr/include/cairomm-1.0;/usr/lib/x86_64-linux-gnu/cairomm-1.0/include;/usr/include/cairo;/usr/include/pixman-1;/usr/include/freetype2;/usr/include/libpng16;/usr/include/freetype2;/usr/include/libpng16;/usr/include/sigc++-2.0;/usr/lib/x86_64-linux-gnu/sigc++-2.0/include;/usr/include/gdk-pixbuf-2.0;/usr/include/libpng16;/usr/include/glib-2.0;/usr/lib/x86_64-linux-gnu/glib-2.0/include
GTKMM_STATIC_LDFLAGS:INTERNAL=-lgtkmm-3.0;-latkmm-1.6;-lgdkmm-3.0;-lgiomm-2.4;-lpangomm-1.4;-lglibmm-2.4;-lgtk-3;-latk-bridge-2.0;-latspi;-ldbus-1;-lpthread;-lsystemd;-lgdk-3;-lgio-2.0;-lXinerama;-lXi;-lXrandr;-lXcursor;-lXcomposite;-lXdamage;-lXfixes;-lxkbcommon;-lwayland-cursor;-lwayland-egl;-lwayland-client;-lepoxy;-ldl;-lpangocairo-1.0;-lpangoft2-1.0;-lharfbuzz;-lm;-lgraphite2;-lpango-1.0;-lm;-latk-1.0;-lcairo-gobject;-lgio-2.0;-lz;-lresolv;-lselinux;-lmount;-lcairomm-1.0;-lcairo;-lz;-lpixman-1;-lfontconfig;-lexpat;-lfreetype;-lexpat;-lfreetype;-lpng16;-lm;-lz;-lm;-lxcb-shm;-lxcb-render;-lXrender;-lXext;-lX11;-lpthread;-lxcb;-lXau;-lXdmcp;-lsigc-2.0;-lgdk_pixbuf-2.0;-lm;-lgmodule-2.0;-pthread;-ldl;-lpng16;-lm;-lz;-lm;-lz;-lgobject-2.0;-lffi;-lglib-2.0;-pthread;-lpcre;-pthread
GTKMM_STATIC_LDFLAGS_OTHER:INTERNAL=-pthread
GTKMM_STATIC_LIBDIR:INTERNAL=
GTKMM_STATIC_LIBRARIES:INTERNAL=gtkmm-3.0;atkmm-1.6;gdkmm-3.0;giomm-2.4;pangomm-1.4;glibmm-2.4;gtk-3;atk-bridge-2.0;atspi;dbus-1;pthread;systemd;gdk-3;gio-2.0;Xinerama;Xi;Xrandr;Xcursor;Xcomposite;Xdamage;Xfixes;xkbcommon;wayland-cursor;wayland-egl;wayland-client;epoxy;dl;pangocairo-1.0;pangoft2-1.0;harfbuzz;m;graphite2;pango-1.0;m;atk-1.0;cairo-gobject;gio-2.0;z;resolv;selinux;mount;cairomm-1.0;cairo;z;pixman-1;fontconfig;expat;freetype;expat;freetype;png16;m;z;m;xcb-shm;xcb-render;Xrender;Xext;X11;pthread;xcb;Xau;Xdmcp;sigc-2.0;gdk_pixbuf-2.0;m;gmodule-2.0;dl;png16;m;z;m;z;gobject-2.0;ffi;glib-2.0;pcre
GTKMM_STATIC_LIBRARY_DIRS:INTERNAL=
GTKMM_STATIC_LIBS:INTERNAL=
GTKMM_STATIC_LIBS_L:INTERNAL=
GTKMM_STATIC_LIBS_OTHER:INTERNAL=
GTKMM_STATIC_LIBS_PATHS:INTERNAL=
GTKMM_VERSION:INTERNAL=3.22.2
GTKMM_gtkmm-3.0_INCLUDEDIR:INTERNAL=
GTKMM_gtkmm-3.0_LIBDIR:INTERNAL=
GTKMM_gtkmm-3.0_PREFIX:INTERNAL=
GTKMM_gtkmm-3.0_VERSION:INTERNAL=
//ADVANCED property for variable: PKG_CONFIG_EXECUTABLE
PKG_CONFIG_EXECUTABLE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: PYTHON_EXECUTABLE
PYTHON_EXECUTABLE-ADVANCED:INTERNAL=1
//Result of TRY_COMPILE
THREADS_HAVE_PTHREAD_ARG:INTERNAL=TRUE
//ADVANCED property for variable: X11_ICE_INCLUDE_PATH
X11_ICE_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_ICE_LIB
X11_ICE_LIB-ADVANCED:INTERNAL=1
//Have library /usr/lib/x86_64-linux-gnu/libX11.so;/usr/lib/x86_64-linux-gnu/libXext.so
X11_LIB_X11_SOLO:INTERNAL=1
//ADVANCED property for variable: X11_SM_INCLUDE_PATH
X11_SM_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_SM_LIB
X11_SM_LIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_X11_INCLUDE_PATH
X11_X11_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_X11_LIB
X11_X11_LIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_XRes_INCLUDE_PATH
X11_XRes_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_XRes_LIB
X11_XRes_LIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_XShm_INCLUDE_PATH
X11_XShm_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_XSync_INCLUDE_PATH
X11_XSync_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_XTest_INCLUDE_PATH
X11_XTest_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_XTest_LIB
X11_XTest_LIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xaccessrules_INCLUDE_PATH
X11_Xaccessrules_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xaccessstr_INCLUDE_PATH
X11_Xaccessstr_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xau_INCLUDE_PATH
X11_Xau_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xau_LIB
X11_Xau_LIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xcomposite_INCLUDE_PATH
X11_Xcomposite_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xcomposite_LIB
X11_Xcomposite_LIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xcursor_INCLUDE_PATH
X11_Xcursor_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xcursor_LIB
X11_Xcursor_LIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xdamage_INCLUDE_PATH
X11_Xdamage_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xdamage_LIB
X11_Xdamage_LIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xdmcp_INCLUDE_PATH
X11_Xdmcp_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xdmcp_LIB
X11_Xdmcp_LIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xext_LIB
X11_Xext_LIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xfixes_INCLUDE_PATH
X11_Xfixes_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xfixes_LIB
X11_Xfixes_LIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xft_INCLUDE_PATH
X11_Xft_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xft_LIB
X11_Xft_LIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xi_INCLUDE_PATH
X11_Xi_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xi_LIB
X11_Xi_LIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xinerama_INCLUDE_PATH
X11_Xinerama_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xinerama_LIB
X11_Xinerama_LIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xinput_INCLUDE_PATH
X11_Xinput_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xinput_LIB
X11_Xinput_LIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xkb_INCLUDE_PATH
X11_Xkb_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xkbfile_INCLUDE_PATH
X11_Xkbfile_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xkbfile_LIB
X11_Xkbfile_LIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xkblib_INCLUDE_PATH
X11_Xkblib_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xlib_INCLUDE_PATH
X11_Xlib_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xmu_INCLUDE_PATH
X11_Xmu_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xmu_LIB
X11_Xmu_LIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xpm_INCLUDE_PATH
X11_Xpm_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xpm_LIB
X11_Xpm_LIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xrandr_INCLUDE_PATH
X11_Xrandr_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xrandr_LIB
X11_Xrandr_LIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xrender_INCLUDE_PATH
X11_Xrender_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xrender_LIB
X11_Xrender_LIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xscreensaver_INCLUDE_PATH
X11_Xscreensaver_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xscreensaver_LIB
X11_Xscreensaver_LIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xshape_INCLUDE_PATH
X11_Xshape_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xt_INCLUDE_PATH
X11_Xt_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xt_LIB
X11_Xt_LIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xutil_INCLUDE_PATH
X11_Xutil_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xv_INCLUDE_PATH
X11_Xv_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xv_LIB
X11_Xv_LIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xxf86misc_LIB
X11_Xxf86misc_LIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_Xxf86vm_LIB
X11_Xxf86vm_LIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_dpms_INCLUDE_PATH
X11_dpms_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_xf86misc_INCLUDE_PATH
X11_xf86misc_INCLUDE_PATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: X11_xf86vmode_INCLUDE_PATH
X11_xf86vmode_INCLUDE_PATH-ADVANCED:INTERNAL=1
//CMAKE_INSTALL_PREFIX during last run
_GNUInstallDirs_LAST_CMAKE_INSTALL_PREFIX:INTERNAL=/usr/local
__pkg_config_arguments_GLIB:INTERNAL=REQUIRED;glib-2.0
__pkg_config_arguments_GTKMM:INTERNAL=REQUIRED;gtkmm-3.0
__pkg_config_checked_GLIB:INTERNAL=1
__pkg_config_checked_GTKMM:INTERNAL=1
prefix_result:INTERNAL=/usr/lib/x86_64-linux-gnu

