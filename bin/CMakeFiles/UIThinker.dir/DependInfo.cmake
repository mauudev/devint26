# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/maudev/Escritorio/devint26/source/sourceUI/Main.cpp" "/home/maudev/Escritorio/devint26/bin/CMakeFiles/UIThinker.dir/source/sourceUI/Main.cpp.o"
  "/home/maudev/Escritorio/devint26/source/sourceUI/component/ActionToolButton.cpp" "/home/maudev/Escritorio/devint26/bin/CMakeFiles/UIThinker.dir/source/sourceUI/component/ActionToolButton.cpp.o"
  "/home/maudev/Escritorio/devint26/source/sourceUI/component/ActionToolToggle.cpp" "/home/maudev/Escritorio/devint26/bin/CMakeFiles/UIThinker.dir/source/sourceUI/component/ActionToolToggle.cpp.o"
  "/home/maudev/Escritorio/devint26/source/sourceUI/component/Canvas.cpp" "/home/maudev/Escritorio/devint26/bin/CMakeFiles/UIThinker.dir/source/sourceUI/component/Canvas.cpp.o"
  "/home/maudev/Escritorio/devint26/source/sourceUI/component/Infobar.cpp" "/home/maudev/Escritorio/devint26/bin/CMakeFiles/UIThinker.dir/source/sourceUI/component/Infobar.cpp.o"
  "/home/maudev/Escritorio/devint26/source/sourceUI/component/Linkbutton.cpp" "/home/maudev/Escritorio/devint26/bin/CMakeFiles/UIThinker.dir/source/sourceUI/component/Linkbutton.cpp.o"
  "/home/maudev/Escritorio/devint26/source/sourceUI/component/Menubar.cpp" "/home/maudev/Escritorio/devint26/bin/CMakeFiles/UIThinker.dir/source/sourceUI/component/Menubar.cpp.o"
  "/home/maudev/Escritorio/devint26/source/sourceUI/component/Togglebutton.cpp" "/home/maudev/Escritorio/devint26/bin/CMakeFiles/UIThinker.dir/source/sourceUI/component/Togglebutton.cpp.o"
  "/home/maudev/Escritorio/devint26/source/sourceUI/component/Tool.cpp" "/home/maudev/Escritorio/devint26/bin/CMakeFiles/UIThinker.dir/source/sourceUI/component/Tool.cpp.o"
  "/home/maudev/Escritorio/devint26/source/sourceUI/component/ToolDefinition.cpp" "/home/maudev/Escritorio/devint26/bin/CMakeFiles/UIThinker.dir/source/sourceUI/component/ToolDefinition.cpp.o"
  "/home/maudev/Escritorio/devint26/source/sourceUI/view/Buttonbox.cpp" "/home/maudev/Escritorio/devint26/bin/CMakeFiles/UIThinker.dir/source/sourceUI/view/Buttonbox.cpp.o"
  "/home/maudev/Escritorio/devint26/source/sourceUI/view/CanvasToolBox.cpp" "/home/maudev/Escritorio/devint26/bin/CMakeFiles/UIThinker.dir/source/sourceUI/view/CanvasToolBox.cpp.o"
  "/home/maudev/Escritorio/devint26/source/sourceUI/view/ToolPalette.cpp" "/home/maudev/Escritorio/devint26/bin/CMakeFiles/UIThinker.dir/source/sourceUI/view/ToolPalette.cpp.o"
  "/home/maudev/Escritorio/devint26/source/sourceUI/view/WindowUI.cpp" "/home/maudev/Escritorio/devint26/bin/CMakeFiles/UIThinker.dir/source/sourceUI/view/WindowUI.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GTEST"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/gtkmm-3.0"
  "/usr/lib/x86_64-linux-gnu/gtkmm-3.0/include"
  "/usr/include/atkmm-1.6"
  "/usr/include/gtk-3.0/unix-print"
  "/usr/include/gdkmm-3.0"
  "/usr/lib/x86_64-linux-gnu/gdkmm-3.0/include"
  "/usr/include/giomm-2.4"
  "/usr/lib/x86_64-linux-gnu/giomm-2.4/include"
  "/usr/include/pangomm-1.4"
  "/usr/lib/x86_64-linux-gnu/pangomm-1.4/include"
  "/usr/include/glibmm-2.4"
  "/usr/lib/x86_64-linux-gnu/glibmm-2.4/include"
  "/usr/include/gtk-3.0"
  "/usr/include/at-spi2-atk/2.0"
  "/usr/include/at-spi-2.0"
  "/usr/include/dbus-1.0"
  "/usr/lib/x86_64-linux-gnu/dbus-1.0/include"
  "/usr/include/gio-unix-2.0"
  "/usr/include/cairo"
  "/usr/include/pango-1.0"
  "/usr/include/harfbuzz"
  "/usr/include/atk-1.0"
  "/usr/include/cairomm-1.0"
  "/usr/lib/x86_64-linux-gnu/cairomm-1.0/include"
  "/usr/include/pixman-1"
  "/usr/include/freetype2"
  "/usr/include/libpng16"
  "/usr/include/sigc++-2.0"
  "/usr/lib/x86_64-linux-gnu/sigc++-2.0/include"
  "/usr/include/gdk-pixbuf-2.0"
  "/usr/include/glib-2.0"
  "/usr/lib/x86_64-linux-gnu/glib-2.0/include"
  "../library/python-ast/include"
  "../include"
  "../include/includeUI/configure"
  "../include/includeUI/view"
  "../include/includeUI/component"
  "../include/includeUI/service"
  "../thirdparty/googletest/googletest/include"
  "../thirdparty/googletest/googletest"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
