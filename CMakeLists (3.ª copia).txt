PROJECT(thinker)
PROJECT(tests)
CMAKE_MINIMUM_REQUIRED(VERSION 3.0.0)

ADD_COMPILE_OPTIONS(-std=c++14)
ADD_DEFINITIONS(-DGTEST)
ADD_SUBDIRECTORY(thirdparty/googletest)

FIND_PACKAGE(X11 REQUIRED)
FIND_PACKAGE(PkgConfig REQUIRED)

PKG_CHECK_MODULES(GTKMM REQUIRED gtkmm-3.0)
PKG_CHECK_MODULES(GLIB REQUIRED glib-2.0)

INCLUDE_DIRECTORIES(${X11_INCLUDE_DIR})
INCLUDE_DIRECTORIES(${GTKMM_INCLUDE_DIRS})
INCLUDE_DIRECTORIES(library/python-ast/include)
INCLUDE_DIRECTORIES(${GLIB_INCLUDE_DIRS})

LINK_DIRECTORIES(${GTKMM_LIBRARY_DIRS})
LINK_DIRECTORIES(${GLIB_LDFLAGS})

ADD_DEFINITIONS(${GTKMM_CFLAGS_OTHER})

INCLUDE_DIRECTORIES(include)
INCLUDE_DIRECTORIES(include/includeUI/configure)
INCLUDE_DIRECTORIES(include/includeUI/view)
INCLUDE_DIRECTORIES(include/includeUI/component)
INCLUDE_DIRECTORIES(include/includeUI/service)
INCLUDE_DIRECTORIES(library/persistence/include)
INCLUDE_DIRECTORIES(thirdparty/jsonlib)
INCLUDE_DIRECTORIES(${gtest_SOURCE_DIR}/include ${gtest_SOURCE_DIR})


ADD_SUBDIRECTORY(library/persistence)
ADD_SUBDIRECTORY(library/python-ast)

#ADD_DEFINITIONS(-DGTEST)

ENABLE_TESTING()


SET(UIThinker
    source/sourceUI/view/ToolPalette.cpp
    source/sourceUI/view/Buttonbox.cpp
    source/sourceUI/view/WindowUI.cpp
    source/sourceUI/view/CanvasToolBox.cpp
    source/sourceUI/component/ActionToolButton.cpp
    source/sourceUI/component/ActionToolToggle.cpp
    source/sourceUI/component/Linkbutton.cpp
    source/sourceUI/component/Togglebutton.cpp
    source/sourceUI/component/Menubar.cpp
    source/sourceUI/component/Infobar.cpp
    source/sourceUI/component/Canvas.cpp
    source/sourceUI/component/Tool.cpp
    source/sourceUI/Main.cpp
)

SET(thinker
    source/Agent.cpp
    source/AgentToolbox.cpp
    source/Project.cpp
    source/OpenAI.cpp
    source/ProjectToolbox.cpp
    source/Thinker.cpp
    source/InputOfAgent.cpp
    source/OutputOfAgent.cpp
    source/Toolbox.cpp
    source/ScreenHandler.cpp
    source/KeyboardHandler.cpp
    source/TensorFlow.cpp
)

SET(tests
    test/ThinkerTest.cpp
    test/AgentTest.cpp
    test/OutputOfAgentTest.cpp
    test/OpenAITest.cpp
    test/InputOfAgentTest.cpp
    test/ScreenHandlerTest.cpp
    test/KeyboardHandlerTest.cpp
    test/ProjectPersistenceTest.cpp
    test/OpenAIPersistenceTest.cpp
    test/SaveLoadPersistenceTest.cpp
    test/KeyboardHandlerPersistenceTest.cpp
    test/ScreenHandlerPersistenceTest.cpp
    test/AgentPersistenceTest.cpp
    test/AgentASTTest.cpp
    test/OpenAIASTNodeTest.cpp
    test/ScreenHandlerAstNodeTest.cpp
    test/InputOfAgentASTNodeTest.cpp
    test/OutputOfAgentASTNodeTest.cpp
    test/KeyboardHandlerASTNodeTest.cpp
    test/TensorFlowASTNodeTest.cpp
    test/Main.cpp
    source/Agent.cpp
    source/AgentToolbox.cpp
    source/Project.cpp
    source/OpenAI.cpp
    source/ProjectToolbox.cpp
    source/Thinker.cpp
    source/InputOfAgent.cpp
    source/OutputOfAgent.cpp
    source/Toolbox.cpp
    source/ScreenHandler.cpp
    source/KeyboardHandler.cpp
    source/TensorFlow.cpp
)

ADD_EXECUTABLE(UIThinker ${UIThinker})
ADD_EXECUTABLE(thinker ${thinker})
ADD_EXECUTABLE(tests ${tests})

TARGET_LINK_LIBRARIES(thinker persistence)
TARGET_LINK_LIBRARIES(thinker python-ast)

TARGET_LINK_LIBRARIES(thinker gtest gtest_main -lpthread)
TARGET_LINK_LIBRARIES(tests persistence)
TARGET_LINK_LIBRARIES(tests python-ast)
TARGET_LINK_LIBRARIES(tests gtest gtest_main -lpthread)
TARGET_LINK_LIBRARIES(UIThinker ${GTKMM_LIBRARIES})

ADD_TEST(thinker thinker)
ADD_TEST(tests tests)
