#include "gtest/gtest.h"
#include "Thinker.h"

using namespace std;

TEST(GetNewInputTest, GetNewInput)
{
    Project *m_project = Thinker::getInstance()->newProject();
    const string agentName = "agente de prueba";
    ProjectToolbox *m_projectToolbox = Thinker::getInstance()
    ->getProjectToolbox();
    Agent *m_agentNew = m_projectToolbox->getNewAgent(agentName);
    AgentToolbox *m_agentToolbox = Thinker::getInstance()->getAgentToolbox();
    InputOfAgent *m_agentInput = m_agentToolbox->getNewInput();
    InputOfAgent *m_agentOfInput = m_agentNew->getInputOfAgent();
    ASSERT_NE(m_agentInput, nullptr);
}

TEST(GetInputOfAgentTest, GetInputOfAgent)
{
    Project *m_project = Thinker::getInstance()->newProject();
    const string agentName = "agente de prueba";
    ProjectToolbox *m_projectToolbox = Thinker::getInstance()
    ->getProjectToolbox();
    Agent *m_agentNew = m_projectToolbox->getNewAgent(agentName);
    AgentToolbox *m_agentToolbox = Thinker::getInstance()->getAgentToolbox();
    InputOfAgent *m_agentInput = m_agentToolbox->getNewInput();
    InputOfAgent *m_agentOfInput = m_agentNew->getInputOfAgent();
    ASSERT_EQ(m_agentOfInput, m_agentNew->getInputOfAgent());
}

TEST(InputOfAgentTest, VerifyNotInputAgent)
{
    Project *m_project = Thinker::getInstance()->newProject();
    ProjectToolbox *m_projectToolbox = Thinker::getInstance()
        ->getProjectToolbox();
    string m_agentName = "agente de prueba";
    Agent *m_agent = m_projectToolbox->getNewAgent(m_agentName);
    m_project->addNewAgent(m_agent);
    Agent *m_agentOfProject = m_project->getAgent(m_agentName);
    AgentToolbox *m_agentToolbox = Thinker::getInstance()->getAgentToolbox();
    InputOfAgent *m_agentInput1 = m_agentToolbox->getNewInput();
    ASSERT_EQ (m_agentOfProject->getInputOfAgent(), nullptr);
}

TEST(CreateOneInputOfAgentTest, VerifyAddNewInputAgent)
{
    Project *m_project = Thinker::getInstance()->newProject();
    ProjectToolbox *m_projectToolbox = Thinker::getInstance()
        ->getProjectToolbox();
    string m_agentName = "agente de prueba";
    Agent *m_agent = m_projectToolbox->getNewAgent(m_agentName);
    m_project->addNewAgent(m_agent);
    Agent *m_agentOfProject = m_project->getAgent(m_agentName);
    AgentToolbox *m_agentToolbox = Thinker::getInstance()->getAgentToolbox();
    InputOfAgent *m_agentInput1 = m_agentToolbox->getNewInput();
    InputOfAgent *m_agentInputReference0 = m_agentOfProject
        ->getInputOfAgent();
    m_agentOfProject->addNewInput(m_agentInput1);
    ASSERT_NE (m_agentOfProject->getInputOfAgent(), nullptr);
}

TEST(CreateTwoInputOfAgentTest, VerifyAddNewInputAgent)
{
    Project *m_project = Thinker::getInstance()->newProject();
    ProjectToolbox *m_projectToolbox = Thinker::getInstance()
        ->getProjectToolbox();
    string m_agentName = "agente de prueba";
    Agent *m_agent = m_projectToolbox->getNewAgent(m_agentName);
    m_project->addNewAgent(m_agent);
    Agent *m_agentOfProject = m_project->getAgent(m_agentName);
    AgentToolbox *m_agentToolbox = Thinker::getInstance()->getAgentToolbox();
    InputOfAgent *m_agentInput1 = m_agentToolbox->getNewInput();
    InputOfAgent *m_agentInputReference0 = m_agentOfProject
        ->getInputOfAgent();
    m_agentOfProject->addNewInput(m_agentInput1);
    InputOfAgent *m_agentInput2 = m_agentToolbox->getNewInput();
    m_agentOfProject->addNewInput(m_agentInput2);
    ASSERT_NE (m_agentOfProject->getInputOfAgent(), m_agentInput1);
}
