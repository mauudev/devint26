#include "gtest/gtest.h"
#include "Thinker.h"
#include "KeyboardHandler.h"
#include <BuilderCodePy.h>
#include <PythonAST.h>
#include <BuilderCode.h>
#include <PythonInjector.h>
#include <string>

TEST(classNodeKeyboard, creation)
{
    Project *m_project = Thinker::getInstance()->newProject();
    string m_agentName = "agente de prueba";
    m_project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(m_agentName));
    m_project->getAgent(m_agentName)
        ->addNewKeyboardHandler(Thinker::getInstance()
        ->getAgentToolbox()->getNewKeyboardHandler("KeyboardHandler 1"));

    KeyboardHandler *keyboardHandlertest = m_project->getAgent(m_agentName)
        ->getKeyboardHandler();

    string pythonCode1 = "class KeyboardHandler(object):\n\tdef __init__(self):\n\t\tself.KeyboardName = \"KeyboardHandler 1\"\n\t\n\tdef getKeyboardName(self):\n\t\treturn self.KeyboardName\n\n\tdef setKeyboardName(self, KeyboardName):\n\t\tself.KeyboardName = KeyboardName";

    string pythonCode2;
    BuilderCodePy py;
    py.addCode(&keyboardHandlertest->generateASTClassNode(py));
    pythonCode2 = py.generateCode();
    ASSERT_EQ(pythonCode1, pythonCode2);
}