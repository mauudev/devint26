#include "gtest/gtest.h"
#include "Thinker.h"
#include "OpenAI.h"
#include "Agent.h"
#include <BuilderCodePy.h>
#include <PythonAST.h>
#include <BuilderCode.h>
#include <PythonInjector.h>
#include <string>

TEST(classNodeOpenAI, areEquals)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
    ->getNewAgent(agentName));

    string gameName = "MsPacMan-ram-v0";
    project->getAgent(agentName)
        ->addNewOpenAI(Thinker::getInstance()->getAgentToolbox()
        ->getNewOpenAI(gameName));

    OpenAI *openIAtest = (project->getAgent(agentName))->getOpenAI();

    string pythonCode1 = "class OpenAI(object):\n\tdef __init__(self):\n\t\tself.openAIName = \"MsPacMan-ram-v0\"\n\t\n\tdef getOpenAIName(self):\n\t\treturn self.openAIName\n\n\tdef setOpenAIName(self, openAIName):\n\t\tself.openAIName = openAIName";

    string pythonCode2;
    BuilderCodePy py;
    py.addCode(&openIAtest->generateASTClassNode(py));
    pythonCode2 = py.generateCode();
    ASSERT_EQ(pythonCode1, pythonCode2);
}
