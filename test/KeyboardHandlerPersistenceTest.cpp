#include "gtest/gtest.h"
#include "Thinker.h"

TEST(Serializem_keyboardHandlerTest, serialize)
{
    Project *m_project = Thinker::getInstance()->newProject();
    m_project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent("Agent"));

    string keyboardHandlerName = "First KeyboardHandler";

    m_project->getAgent("Agent")
        ->addNewKeyboardHandler(Thinker::getInstance()->getAgentToolbox()
        ->getNewKeyboardHandler(keyboardHandlerName));

    Json m_jsonKeyboard = m_project->getAgent("Agent")->getKeyboardHandler()
        ->serialize();

    ASSERT_EQ(m_jsonKeyboard["keyboardName"], keyboardHandlerName);
}

TEST(Deserializem_keyboardHandlerTest, deserialize)
{
    Project *m_project = Thinker::getInstance()->newProject();
    m_project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent("Agent"));

    string keyboardHandlerName = "First KeyboardHandler";

    m_project->getAgent("Agent")
        ->addNewKeyboardHandler(Thinker::getInstance()->getAgentToolbox()
        ->getNewKeyboardHandler(keyboardHandlerName));

    Json m_jsonKeyboard = m_project->getAgent("Agent")->getKeyboardHandler()
        ->serialize();

    KeyboardHandler *keyboard = Thinker::getInstance()->getProject()
        ->getAgent("Agent")->getKeyboardHandler();

    keyboard->deserialize(m_jsonKeyboard);

    Json m_jsonKeyboard2 = keyboard->serialize();

    ASSERT_EQ(m_jsonKeyboard2, m_jsonKeyboard);
}

