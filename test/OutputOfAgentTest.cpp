#include "gtest/gtest.h"
#include "Thinker.h"

using namespace std;

TEST(OutputOfAgentTest, VerifyNotOutputAgent)
{
    //create a project
    Project *m_project = Thinker::getInstance()->newProject();

    // we ask for a projectToolbox to Thinker
    ProjectToolbox *m_projectToolbox = Thinker::getInstance()
        ->getProjectToolbox();

    // we create an agent
    string m_agentName = "agente de prueba";
    Agent *m_agent = m_projectToolbox->getNewAgent(m_agentName);

    // we added the agent to the project
    m_project->addNewAgent(m_agent);

    // we create a reference of an agent that is in a project by its name
    Agent *m_agentOfProject = m_project->getAgent(m_agentName);

    // we ask for an agentToolbox to the Thinker
    AgentToolbox *m_agentToolbox = Thinker::getInstance()->getAgentToolbox();

    // from the agentToolbox we create the agent output
    OutputOfAgent *m_agentOutput1 = m_agentToolbox->getNewOutput();

    // get reference of agent1
    OutputOfAgent *m_agentOutputReference0 = m_agentOfProject
        ->getOutputOfAgent();

    // test 1 verifies that there is no data output for an agent
    ASSERT_EQ (m_agentOfProject->getOutputOfAgent(), nullptr);

    // test 2 verifies agent Output was created
    ASSERT_NE (m_agentOutput1, nullptr);
}

TEST(CreateOneOutputOfAgentTest, VerifyAddNewOutputAgent)
{
    //create a project
    Project *m_project = Thinker::getInstance()->newProject();

    // we ask for a projectToolbox to Thinker
    ProjectToolbox *m_projectToolbox = Thinker::getInstance()
        ->getProjectToolbox();

    // we create an agent
    string m_agentName = "agente de prueba";
    Agent *m_agent = m_projectToolbox->getNewAgent(m_agentName);

    // we added the agent to the project
    m_project->addNewAgent(m_agent);

    // we create a reference of an agent that is in a project by its name
    Agent *m_agentOfProject = m_project->getAgent(m_agentName);

    // we ask for an agentToolbox to the Thinker
    AgentToolbox *m_agentToolbox = Thinker::getInstance()->getAgentToolbox();

    // from the agentToolbox we create the agent output
    OutputOfAgent *m_agentOutput1 = m_agentToolbox->getNewOutput();

    // get reference of agent1
    OutputOfAgent *m_agentOutputReference0 = m_agentOfProject
        ->getOutputOfAgent();

    // we add a data output to an agent
    m_agentOfProject->addNewOutput(m_agentOutput1);

    // test 3: verify that there is a data output for an agent
    ASSERT_NE (m_agentOfProject->getOutputOfAgent(), nullptr);
}

TEST(CreateTwoOutputOfAgentTest, VerifyAddNewOutputAgent)
{
    //create a project
    Project *m_project = Thinker::getInstance()->newProject();

    // we ask for a projectToolbox to Thinker
    ProjectToolbox *m_projectToolbox = Thinker::getInstance()
        ->getProjectToolbox();

    // we create an agent
    string m_agentName = "agente de prueba";
    Agent *m_agent = m_projectToolbox->getNewAgent(m_agentName);

    // we added the agent to the project
    m_project->addNewAgent(m_agent);

    // we create a reference of an agent that is in a project by its name
    Agent *m_agentOfProject = m_project->getAgent(m_agentName);

    // we ask for an agentToolbox to the Thinker
    AgentToolbox *m_agentToolbox = Thinker::getInstance()->getAgentToolbox();

    // from the agentToolbox we create the agent output
    OutputOfAgent *m_agentOutput1 = m_agentToolbox->getNewOutput();

    // get reference of agent1
    OutputOfAgent *m_agentOutputReference0 = m_agentOfProject
        ->getOutputOfAgent();

    // we add a data output to an agent
    m_agentOfProject->addNewOutput(m_agentOutput1);

    //create other agentOutput
    OutputOfAgent *m_agentOutput2 = m_agentToolbox->getNewOutput();
    m_agentOfProject->addNewOutput(m_agentOutput2);

    // test 4 verify that agentOfProject have other agentOutput
    ASSERT_NE (m_agentOfProject->getOutputOfAgent(), m_agentOutput1);
}