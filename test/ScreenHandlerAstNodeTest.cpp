#include "gtest/gtest.h"
#include "Thinker.h"
#include <BuilderCodePy.h>
#include <PythonAST.h>
#include <BuilderCode.h>
#include <PythonInjector.h>
#include <string>

TEST(classNodeScreenHandler, areEquals)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(agentName));

    int speed = 25;
    int screenHeight = 400;
    int screenWidth = 400;
    const string screenName = "Screen Handler";

    project->getAgent(agentName)
        ->addNewScreenHandler(Thinker::getInstance()->getAgentToolbox()
        ->getNewScreenHandler(speed, screenHeight, screenWidth, screenName));

    ScreenHandler *sc = (project->getAgent(agentName))->getScreenHandler();

    const string py_expected = "class ScreenHandler(object):\n\tdef __init__(self):\n\t\tself.screenName = \"Screen Handler\"\n\t\tself.screenSpeed = 25\n\t\tself.screenWidth = 400\n\t\tself.screenHeight = 400\n\t\n\tdef setScreenHandlerName(self, screenName):\n\t\tself.screenName = screenName\n\tdef setScreenHandlerSpeed(self, screenSpeed):\n\t\tself.screenSpeed = screenSpeed\n\tdef setScreenHandlerWidth(self, screenWidth):\n\t\tself.screenWidth = screenWidth\n\tdef setScreenHandlerHeight(self, screenHeight):\n\t\tself.screenHeight = screenHeight\n\tdef getScreenHandlerName(self):\n\t\treturn self.screenName\n\n\tdef getScreenHandlerSpeed(self):\n\t\treturn self.screenSpeed\n\n\tdef getScreenHandlerWidth(self):\n\t\treturn self.screenWidth\n\n\tdef getScreenHandlerHeight(self):\n\t\treturn self.screenHeight\n";

    BuilderCodePy py;
    string py_actual;
    py.addCode(&sc->generateASTClassNode(py));
    py_actual = py.generateCode();
    ASSERT_EQ(py_expected, py_actual);
}
