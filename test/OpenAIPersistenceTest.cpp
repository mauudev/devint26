#include "gtest/gtest.h"
#include "Thinker.h"

TEST(GetCustomOpenAISerialize, myOpenAISerializeTest)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(agentName));

    string gameName = "MsPacMan-ram-v0";
    project->getAgent(agentName)
        ->addNewOpenAI(Thinker::getInstance()->getAgentToolbox()
        ->getNewOpenAI(gameName));
    Json jsonOpenAI = project->getAgent(agentName)->getOpenAI()->serialize();

    ASSERT_EQ(jsonOpenAI["openAIName"], gameName);
}

TEST(GetOpenAIDeserialize, myOpenAIDeserializeTest)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(agentName));

    string gameName = "MsPacMan-ram-v0";
    project->getAgent(agentName)
        ->addNewOpenAI(Thinker::getInstance()->getAgentToolbox()
        ->getNewOpenAI(gameName));
    Json jsonOpenAI = project->getAgent(agentName)->getOpenAI()->serialize();

    string agentName1 = "agentito de prueba";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(agentName1));

    project->getAgent(agentName1)->addNewOpenAI(Thinker::getInstance()
        ->getAgentToolbox()->getNewOpenAI("Default"));

    project->getAgent(agentName1)->getOpenAI()->deserialize(jsonOpenAI);

    ASSERT_EQ(gameName, project->getAgent(agentName1)->getOpenAI()
        ->getOpenAIName());
}

