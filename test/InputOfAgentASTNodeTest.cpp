#include "gtest/gtest.h"
#include "Thinker.h"
#include "OutputOfAgent.h"
#include "Agent.h"
#include <BuilderCodePy.h>
#include <PythonAST.h>
#include <string>

TEST(classNodeInputOfAgent, AreEquals)
{
    Project *m_project = Thinker::getInstance()->newProject();
    ProjectToolbox *m_projectToolbox = Thinker::getInstance()
        ->getProjectToolbox();
    string m_agentName = "agente de prueba";
    Agent *m_agent = m_projectToolbox->getNewAgent(m_agentName);
    m_project->addNewAgent(m_agent);
    Agent *m_agentOfProject = m_project->getAgent(m_agentName);
    AgentToolbox *m_agentToolbox = Thinker::getInstance()->getAgentToolbox();

    InputOfAgent *m_agentInput1 = m_agentToolbox->getNewInput();

    string aux1 = "class InputOfAgent(object):\n\tdef __init__(self): pass";
    string aux2 = "\n\tdef __body__(self): pass";

    string pythonCode1 = aux1 + aux2;

    string pythonCode2;
    BuilderCodePy py;
    py.addCode(&m_agentInput1->generateASTClassNode(py));
    pythonCode2 = py.generateCode();
    ASSERT_EQ(pythonCode2, pythonCode1);
}
