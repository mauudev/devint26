#include "gtest/gtest.h"
#include "Thinker.h"

TEST(SerializeScreenHandlerTest, serialize)
{
    Project *project = Thinker::getInstance()->newProject();

    string agentName = "Agent";

    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
    ->getNewAgent(agentName));

    int screenSpeed = 25;
    int screenHeight = 400;
    int screenWidth = 500;
    const string screenName = "Screen Handler";

    project->getAgent(agentName)
        ->addNewScreenHandler(Thinker::getInstance()->getAgentToolbox()
        ->getNewScreenHandler(screenSpeed, screenHeight, screenWidth, screenName));

    Json jsonScreen = project->getAgent(agentName)
        ->getScreenHandler()->serialize();


    ASSERT_NE(jsonScreen, nullptr);

    ASSERT_EQ(jsonScreen["screenSpeed"], screenSpeed);

    ASSERT_EQ(jsonScreen["screenHeight"], screenHeight);

    ASSERT_EQ(jsonScreen["screenWidth"], screenWidth);

    ASSERT_EQ(jsonScreen["screenName"], screenName);

}

TEST(DeserializeScreenHandlerTest, deserialize)
{
    Project *project = Thinker::getInstance()->newProject();

    string agentName = "Agent";
    
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
    ->getNewAgent(agentName));

    int screenSpeed = 25;
    int screenHeight = 400;
    int screenWidth = 400;
    const string screenName = "Screen Handler";

    project->getAgent(agentName)
        ->addNewScreenHandler(Thinker::getInstance()->getAgentToolbox()
        ->getNewScreenHandler(screenSpeed, screenHeight, screenWidth, screenName));

    Json jsonScreen = project->getAgent(agentName)
        ->getScreenHandler()->serialize();

    ScreenHandler *screenHandler = Thinker::getInstance()->getProject()
        ->getAgent(agentName)->getScreenHandler();

    screenHandler->deserialize(jsonScreen);

    Json jsonScreen2 = screenHandler->serialize();

    ASSERT_EQ(jsonScreen, jsonScreen2);

}