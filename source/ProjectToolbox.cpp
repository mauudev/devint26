#include "ProjectToolbox.h"

ProjectToolbox::ProjectToolbox()
{
}

ProjectToolbox::~ProjectToolbox()
{
}

Agent *ProjectToolbox::getNewAgent(const string &p_agentName) const
{
    return new Agent{p_agentName};
}

OpenAI *ProjectToolbox::getNewOpenAI(const string &p_openAIName) const
{
    return new OpenAI{p_openAIName};
}