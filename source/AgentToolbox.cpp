#include "AgentToolbox.h"

AgentToolbox::AgentToolbox()
{
}

AgentToolbox::~AgentToolbox()
{
}

OutputOfAgent *AgentToolbox::getNewOutput() const
{
    return new OutputOfAgent{};
}

OpenAI *AgentToolbox::getNewOpenAI(const string &p_openAIName) const
{
    return new OpenAI{p_openAIName};
}

InputOfAgent *AgentToolbox::getNewInput() const
{
    return new InputOfAgent{};
}

ScreenHandler *AgentToolbox::getNewScreenHandler(const int &p_screenSpeed, const int &p_screenHeight, const int &p_screenWidth, const string &p_screenName) const
{
    return new ScreenHandler{p_screenSpeed, p_screenHeight, p_screenWidth, p_screenName};
}

KeyboardHandler *AgentToolbox::getNewKeyboardHandler(const string &p_keyboardName) const
{
    return new KeyboardHandler{p_keyboardName};
}

TensorFlow *AgentToolbox::getNewTensorFlow(const string &p_tensorFlowName) const
{
    return new TensorFlow{p_tensorFlowName};
}
