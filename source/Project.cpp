#include "Project.h"

Project::Project()
:m_projectName{"Default Project Name"}
{
}

Project::~Project()
{
}

bool Project::addNewAgent(Agent *p_agent)
{
    bool validator = m_agentMap.insert(std::pair<string, Agent*>
        (p_agent->getAgentName(), p_agent)).second;
    return validator;
}

Agent *Project::getAgent(const string &p_agentName) const
{
    map<string, Agent*>::const_iterator agentIterator = m_agentMap
        .find(p_agentName);
    if(agentIterator != m_agentMap.end())
    {
        return agentIterator->second;
    }
    return nullptr;
}

string Project::getProjectName() const
{
    return m_projectName;
}

void Project::setProjectName(const string &p_name)
{
    m_projectName = p_name;
}

Json Project::serialize()
{
    Json jsonAnswer;
    jsonAnswer["ProjectName"] = m_projectName;

    size_t accountant = 0;
    map<string, Agent*>::iterator iterator = m_agentMap.begin();
    while(iterator != m_agentMap.end())
    {
        jsonAnswer["Agents"][accountant] = (iterator->second)->serialize();
        accountant++;
        iterator++;
    }
    return jsonAnswer;
}

void Project::deserialize(Json &p_json)
{
    string auxiliarName = p_json["ProjectName"];
    this->setProjectName(auxiliarName.data());

    size_t sizeJsonAgents = p_json["Agents"].size();
    for(size_t i = 0; i < sizeJsonAgents; i++)
    {
        Agent *new_agent = new Agent{};
        new_agent->deserialize(p_json["Agents"][i]);
        this->addNewAgent(new_agent);
    }
}
