#include "OutputOfAgent.h"

OutputOfAgent::OutputOfAgent()
{
}

OutputOfAgent::~OutputOfAgent()
{
}

Statement &OutputOfAgent::generateASTInitNode(BuilderCodePy &py)
{
    return
    py.functionDef("__init__")
        .addParameter(&py.name("self"));
}

Statement &OutputOfAgent::generateASTBodyNode(BuilderCodePy &py)
{
    return 
    py.functionDef("__body__")
        .addParameter(&py.name("self"));
}

Statement &OutputOfAgent::generateASTClassNode(BuilderCodePy &py)
{
    return 
    py.classDef("OutputOfAgent")
        .addParameter(&py.name("object"))
        .addBodyNode(
            &OutputOfAgent::generateASTInitNode(py)
        )
        .addBodyNode(
            &OutputOfAgent::generateASTBodyNode(py)
        );
}