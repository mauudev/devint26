#include "Agent.h"
#include <iostream>

Agent::Agent()
:m_agentName{"Defaul Agent Name"}, m_outputOfAgent{nullptr}, m_openAI{nullptr},
    m_inputOfAgent{nullptr}, m_tensorFlow{nullptr}
{
}

Agent::Agent(const string &p_agentName)
:m_agentName{p_agentName}, m_outputOfAgent{nullptr}, m_openAI{nullptr},
    m_inputOfAgent{nullptr}, m_screenHandler{nullptr},
    m_keyboardHandler{nullptr}, m_tensorFlow{nullptr}
{
}

Agent::~Agent()
{
}

void Agent::addNewOutput(OutputOfAgent *p_outputOfAgent)
{
    m_outputOfAgent = p_outputOfAgent;
}

OutputOfAgent *Agent::getOutputOfAgent() const
{
    return m_outputOfAgent;
}

const string Agent::getAgentName() const
{
    return m_agentName;
}

void Agent::setAgentName(const string &p_agentName)
{
    m_agentName = p_agentName;
}

void Agent::addNewOpenAI(OpenAI *p_openAI)
{
    m_openAI = p_openAI;
}

OpenAI *Agent::getOpenAI() const
{
    return m_openAI;
}

void Agent::addNewInput(InputOfAgent *p_inputOfAgent)
{
    m_inputOfAgent = p_inputOfAgent;
}

InputOfAgent *Agent::getInputOfAgent() const
{
    return m_inputOfAgent;
}

void Agent::addNewScreenHandler(ScreenHandler *p_screenHandler)
{
    m_screenHandler = p_screenHandler;
}

ScreenHandler *Agent::getScreenHandler() const
{
    return m_screenHandler;
}

void Agent::addNewKeyboardHandler(KeyboardHandler *p_keyboardHandler)
{
    m_keyboardHandler = p_keyboardHandler;
}

KeyboardHandler *Agent::getKeyboardHandler() const
{
    return m_keyboardHandler;
}

void Agent::addNewTensorFlow(TensorFlow *p_tensorFlow)
{
    m_tensorFlow = p_tensorFlow;
}

TensorFlow *Agent::getTensorFlow() const
{
    return m_tensorFlow;
}

Json Agent::serialize()
{
    Json jsonAnswer;
    jsonAnswer["AgentName"] = m_agentName;
    if(m_outputOfAgent != nullptr)
    {
        jsonAnswer["OutputOfAgent"] = true;
    }
    else
    {
        jsonAnswer["OutputOfAgent"] = false;
    }
    if(m_openAI != nullptr)
    {
        jsonAnswer["OpenAI"] = m_openAI->serialize();
    }
    else
    {
        jsonAnswer["OpenAI"] = false;
    }
    if(m_inputOfAgent != nullptr)
    {
        jsonAnswer["InputOfAgent"] = true;
    }
    else
    {
        jsonAnswer["InputOfAgent"] = false;
    }
    if(m_screenHandler != nullptr)
    {
        jsonAnswer["ScreenHandler"] = m_screenHandler->serialize();
    }
    else
    {
        jsonAnswer["ScreenHandler"] = false;
    }
    if(m_keyboardHandler != nullptr)
    {
        jsonAnswer["KeyboardHandler"] = m_keyboardHandler->serialize();
    }
    else
    {
        jsonAnswer["KeyboardHandler"] = false;
    }

    if(m_tensorFlow != nullptr)
    {
        jsonAnswer["TensorFlow"] = m_tensorFlow->serialize();
    }
    else
    {
        jsonAnswer["TensorFlow"] = false;
    }
    return jsonAnswer;
}

void Agent::deserialize(Json &p_json)
{
    setAgentName(p_json["AgentName"]);
    if(p_json["InputOfAgent"] == true)
    {
        addNewInput(new InputOfAgent());
    }
    else
    {
        m_inputOfAgent = nullptr;
    }
    if(p_json["OutputOfAgent"] == true)
    {
        addNewOutput(new OutputOfAgent());
    }
    else
    {
        m_outputOfAgent = nullptr;
    }
    if(p_json["OpenAI"] != false)
    {
        OpenAI *openAI = new OpenAI();
        openAI->deserialize(p_json["OpenAI"]);
        addNewOpenAI(openAI);
    }
    else
    {
        m_openAI = nullptr;
    }
    if(p_json["ScreenHandler"] != false)
    {
        ScreenHandler *screenHandler = new ScreenHandler();
        screenHandler->deserialize(p_json["ScreenHandler"]);
        addNewScreenHandler(screenHandler);
    }
    else
    {
        m_screenHandler = nullptr;
    }
    if(p_json["KeyboardHandler"] != false)
    {
        KeyboardHandler *keyboardHandler = new KeyboardHandler();
        keyboardHandler->deserialize(p_json["KeyboardHandler"]);
        addNewKeyboardHandler(keyboardHandler);
    }
    else
    {
        m_keyboardHandler = nullptr;
    }

    if(p_json["TensorFlow"] != false)
    {
        TensorFlow *tensorFlow = new TensorFlow();
        tensorFlow->deserialize(p_json["TensorFlow"]);
        addNewTensorFlow(tensorFlow);
    }
    else
    {
        m_tensorFlow = nullptr;
    }
}

Statement &Agent::generateASTInitNode(BuilderCodePy &py)
{
    return
    py.functionDef("__init__")
        .addParameter(&py.name("self"))
        .addBodyNode(
            &py.assign(
                &py.attribute(
                    &py.name("self"), "AgentName"),
                    &py.name("\"" + m_agentName + "\"")
            )
        );
}

Statement &Agent::setAgentNameAST(BuilderCodePy &py)
{
    return
    py.functionDef("setAgentName")
        .addParameter(&py.name("self"))
        .addParameter(&py.name("AgentName"))
        .addBodyNode(
            &py.assign(
                &py.attribute(&py.name("self"), "AgentName"),
                &py.name("AgentName")
            )
        );
}

Statement &Agent::getAgentNameAST(BuilderCodePy &py)
{
    return
    py.functionDef("getAgentName")
        .addParameter(&py.name("self"))
        .addBodyNode(
            &py.returnStmt(
                &py.attribute(&py.name("self"), "AgentName")
            )
        );
}

Statement &Agent::generateASTBodyNode(BuilderCodePy &py)
{
    return
    py.concat()
        .addBodyNode(&setAgentNameAST(py))
        .addBodyNode(&getAgentNameAST(py));
}

Statement &Agent::generateASTClassNode(BuilderCodePy &py)
{
    return
    py.classDef("Agent")
        .addParameter(&py.name("object"))
        .addBodyNode(&generateASTInitNode(py))
        .addBodyNode(&generateASTBodyNode(py));
}

Statement &Agent::buildASTNode(BuilderCodePy &py)
{
    Statement &ClassAgent = generateASTClassNode(py);
    Statement &ClassInputOfAgent = m_inputOfAgent->generateASTClassNode(py);
    Statement &ClassOpenAI = m_openAI->generateASTClassNode(py);
    Statement &ClassKeyboardHandler =
        m_keyboardHandler->generateASTClassNode(py);
    Statement &ClassScreenHandler = m_screenHandler->generateASTClassNode(py);
    Statement &ClassOutputOfAgent = m_outputOfAgent->generateASTClassNode(py);
    Statement &ClassTensorFlow = m_tensorFlow->generateASTClassNode(py);

    return
    py.concat()
        .addBodyNode(&ClassAgent)
        .addBodyNode(&ClassInputOfAgent)
        .addBodyNode(&ClassOpenAI)
        .addBodyNode(&ClassKeyboardHandler)
        .addBodyNode(&ClassScreenHandler)
        .addBodyNode(&ClassOutputOfAgent)
        .addBodyNode(&ClassTensorFlow);
}

const string Agent::generateCode()
{
    BuilderCodePy py;
    return py.generateCode(&buildASTNode(py));
}
