#include "InputOfAgent.h"

InputOfAgent::InputOfAgent()
{
}

InputOfAgent::~InputOfAgent()
{
}

Statement &InputOfAgent::generateASTInitNode(BuilderCodePy &py)
{
    return
    py.functionDef("__init__")
        .addParameter(&py.name("self"));
}

Statement &InputOfAgent::generateASTBodyNode(BuilderCodePy &py)
{
    return
    py.functionDef("__body__")
        .addParameter(&py.name("self"));
}

Statement &InputOfAgent::generateASTClassNode(BuilderCodePy &py)
{
    return
    py.classDef("InputOfAgent")
        .addParameter(&py.name("object"))
        .addBodyNode(&InputOfAgent::generateASTInitNode(py))
        .addBodyNode(&InputOfAgent::generateASTBodyNode(py));
}