#include "KeyboardHandler.h"

KeyboardHandler::KeyboardHandler()
{
}

KeyboardHandler::KeyboardHandler(const string &p_keyboardName)
:m_keyboardName{p_keyboardName}
{
}

KeyboardHandler::~KeyboardHandler()
{
}

const string KeyboardHandler::getKeyboardName() const
{
    return m_keyboardName;
}

void KeyboardHandler::setKeyboardName(const string &p_keyboardName)
{
    m_keyboardName = p_keyboardName;
}

Statement &KeyboardHandler::setKeyboardNameGenerator(BuilderCodePy &py)
{
    return 
    py.functionDef("setKeyboardName")
        .addParameter(&py.name("self"))
        .addParameter(&py.name("KeyboardName"))
        .addBodyNode(
            &py.assign(
                &py.attribute(
                    &py.name("self"), "KeyboardName"), &py.name("KeyboardName")
            )
        );
}

Statement &KeyboardHandler::getKeyboardNameGenerator(BuilderCodePy &py) const
{
    return 
    py.functionDef("getKeyboardName")
        .addParameter(&py.name("self"))
        .addBodyNode(
            &py.returnStmt(
                &py.attribute(&py.name("self"), "KeyboardName")
            )
        );
}

Statement &KeyboardHandler::generateASTInitNode(BuilderCodePy &py)
{
    return 
    py.functionDef("__init__")
        .addParameter(&py.name("self"))
        .addBodyNode(
            &py.assign(
                &py.attribute(&py.name("self"), "KeyboardName"),
                &py.name("\""+m_keyboardName+"\"")
            )
        );
}

Statement &KeyboardHandler::generateASTBodyNode(BuilderCodePy &py)
{
    return 
    py.concat()
        .addBodyNode(&getKeyboardNameGenerator(py))
        .addBodyNode(&setKeyboardNameGenerator(py));

}

Statement &KeyboardHandler::generateASTClassNode(BuilderCodePy &py)
{
    return 
    py.classDef("KeyboardHandler")
        .addParameter(&py.name("object"))
        .addBodyNode(&generateASTInitNode(py))
        .addBodyNode(&generateASTBodyNode(py));
}

Json KeyboardHandler::serialize()
{
    Json jsonAnswer;

    jsonAnswer["keyboardName"]= m_keyboardName;

    return jsonAnswer;
}

void KeyboardHandler::deserialize(Json &p_json)
{
    this->setKeyboardName(p_json["keyboardName"]);
}



