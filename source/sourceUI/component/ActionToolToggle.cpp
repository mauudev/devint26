#include "ActionToolToggle.h"

ActionToolToggle::ActionToolToggle()
{
}

ActionToolToggle::ActionToolToggle(const std::string& m_file)
:ToggleButton(),m_img{m_file}
{
    this->set_image(m_img);
}

void ActionToolToggle::enable()
{
    this->set_opacity(1);
    this->set_sensitive(true);
}

void ActionToolToggle::disable()
{
    this->set_opacity(0.5);
    this->set_sensitive(false);
}

bool ActionToolToggle::isActive()
{
    return this->get_active();
}

void ActionToolToggle::deactivate()
{
    this->set_active(false);
}

void ActionToolToggle::activate()
{
    this->set_active(true);
}