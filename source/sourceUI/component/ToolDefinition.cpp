#include "ToolDefinition.h"
#include <iostream>     
using namespace std;

ToolDefinition::ToolDefinition(std::string p_name, std::string p_tooltip, 
    int p_id, int p_imageWidth, int p_imageHeight):Gtk::ToolButton(p_tooltip),
    m_typeName{p_name},
    m_id{p_id},
    m_toolTip{p_tooltip},
    m_imageHeight{p_imageWidth},
    m_imageWidth{p_imageHeight}
{
    m_filepath = "../resources/"+m_typeName + ".png";
    try{
        m_imageResource = Gdk::Pixbuf::create_from_file(m_filepath,m_imageWidth
            ,m_imageHeight);
        m_image.set(m_imageResource);
        this->set_label(p_tooltip);
        this->set_icon_name(p_name);
        this->set_icon_widget(m_image);
    }
    catch(...) 
    {
        cout << "An error occurred during image loading: " << m_filepath << 
            endl;
    }
}

ToolDefinition::ToolDefinition()
{

}

ToolDefinition::~ToolDefinition()
{
    delete this;
}

std::string ToolDefinition::getFilepath()const
{
    return m_filepath;
}

std::string ToolDefinition::getTypeName()const
{
    return m_typeName;
}

int ToolDefinition::getId()const
{
    return m_id;
}

Glib::RefPtr<Gdk::Pixbuf> ToolDefinition::getImage()
{
    return m_imageResource;
}

void ToolDefinition::setImageWidth(int p_imageWidth)
{
    m_imageWidth = p_imageWidth;
}

void ToolDefinition::setImageHeight(int p_imageHeight)
{
    m_imageHeight = p_imageHeight;
}
void ToolDefinition::setImage(std::string p_imageName, int p_imageWidth, int p_imageHeight)
{
     ToolDefinition::setImageWidth(p_imageWidth);
     ToolDefinition::setImageHeight(p_imageHeight);
     m_typeName = p_imageName;
     m_filepath = "../resources/"+ m_typeName + ".png";
    try{
        m_imageResource = Gdk::Pixbuf::create_from_file(m_filepath,
            m_imageWidth,m_imageHeight);
        m_image.set(m_imageResource);
        this->set_icon_widget(m_image);
    }
    catch(...) 
    {
        cout << "An error occurred during image loading: " << m_filepath << endl;
    }
}