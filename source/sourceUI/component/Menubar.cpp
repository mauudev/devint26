#include "Menubar.h"
MenuBar::MenuBar()
{
}

MenuBar::~MenuBar()
{
}

void MenuBar::createMenuBar(Gtk::Box &m_mainLayout)
{
    m_mainLayout.pack_start(m_menuBar, Gtk::PACK_SHRINK);
    m_menuFiles.set_label("Files");
    m_menuBar.append(m_menuFiles);
    m_menuFiles.set_submenu(m_subMenuFiles);
    m_newProject.set_label("New Project");
    m_newProject.signal_activate()
        .connect(sigc::mem_fun(*this, &MenuBar::onButtonClickedNew));
    m_subMenuFiles.append(m_newProject);
    m_openProject.set_label("Open Project");
    m_openProject.signal_activate()
        .connect(sigc::mem_fun(*this, &MenuBar::onButtonClickedOpen));
    m_subMenuFiles.append(m_openProject);
    m_saveProject.set_label("Save Project");
    m_saveProject.signal_activate()
        .connect(sigc::mem_fun(*this, &MenuBar::onButtonClickedSave));
    m_subMenuFiles.append(m_saveProject);
    m_subMenuFiles.append(m_line);
    m_menuEdit.set_label("Edit");
    m_menuBar.append(m_menuEdit);
    m_menuEdit.set_submenu(m_subMenuEdit);
    m_menuCut.set_label("Cut");
    m_menuCut.signal_activate()
        .connect(sigc::mem_fun(*this, &MenuBar::otherButtonClicked));
    m_subMenuEdit.append(m_menuCut);
    m_menuCopy.set_label("Copy");
    m_menuCopy.signal_activate()
        .connect(sigc::mem_fun(*this, &MenuBar::otherButtonClicked));
    m_subMenuEdit.append(m_menuCopy);
    m_menuPaste.set_label("Paste");
    m_menuPaste.signal_activate()
        .connect(sigc::mem_fun(*this, &MenuBar::otherButtonClicked));
    m_subMenuEdit.append(m_menuPaste);
    m_subMenuEdit.append(m_menuExample);
}

void MenuBar::onButtonClickedNew()
{
    cout << "new Project clicked\n";
}

void MenuBar::onButtonClickedOpen()
{
    cout << "open clicked\n";
}

void MenuBar::onButtonClickedSave()
{
    cout << "save clicked\n";
}

void MenuBar::onButtonClickedClose()
{
    cout << "close clicked\n";
}

void MenuBar::otherButtonClicked()
{
    cout << "other button clicked\n";
}
