#include "ActionToolButton.h"

ActionToolButton::ActionToolButton()
{
}

ActionToolButton::ActionToolButton(const std::string& m_file)
:Button(),m_img{m_file}
{
    this->set_image(m_img);
}

void ActionToolButton::enable()
{
    this->set_opacity(1);
    this->set_sensitive(true);
}

void ActionToolButton::disable()
{
    this->set_opacity(0.5);
    this->set_sensitive(false);
}