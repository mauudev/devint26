#include<Tool.h>

Tool::Tool(Gtk::Widget *p_canvas, ToolDefinition *p_button,
    double p_x,
    double p_y)
{
    Glib::ustring iconName(p_button->get_icon_name());
    if (iconName.empty())
    {
        iconName = p_button->get_label();
    }
    Glib::RefPtr<Gtk::IconTheme> iconTheme = 
        Gtk::IconTheme::get_for_screen(p_canvas->get_screen());
    int width = 0;
    int heigth = 0;
    Gtk::IconSize::lookup(Gtk::ICON_SIZE_DIALOG, width, heigth);
    // this->m_pixbuf = iconTheme->load_icon(iconName, width, 
    //     Gtk::ICON_LOOKUP_GENERIC_FALLBACK);
    this->m_pixbuf = p_button->getImage();
    this->m_x = p_x;
    this->m_y = p_y;
    m_button=p_button;
    m_toolSelectedForDraw=false;
    m_toolSelectedForEvent=false;
    this->m_heigth=p_y+25;
    this->m_width=p_x+25;
}

Glib::ustring Tool::getIconName()
{
    return m_button->get_icon_name();
}
int Tool::getAxisX()
{
    return this->m_x;
}
int Tool::getAxisY()
{
    return this->m_y;
}
 Glib::RefPtr<Gdk::Pixbuf> Tool::getImage()
 {
    return this->m_pixbuf;
 }
int Tool::getWidth()
 {
    return this->m_width;
 }
int Tool::getHeigth()
{
    return this->m_heigth;
}

bool Tool::isSelectedForEvent()
{
    return m_toolSelectedForEvent;
}

