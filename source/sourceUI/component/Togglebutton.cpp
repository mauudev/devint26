#include "Togglebutton.h"
#include <iostream>

ToggleButton::ToggleButton(const std::string &p_label)
:m_toggleButton{p_label}
{
}

ToggleButton::~ToggleButton()
{
}

void ToggleButton::onToggleButtonClicked() const
{
    std::cout << "The Toggle Button are: "
    << (m_toggleButton.get_active() ? "true" : "false") << ".\n";
}

Gtk::ToggleButton &ToggleButton::getInstance()
{
    return m_toggleButton;
}