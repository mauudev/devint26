#include <iostream>
#include "Canvas.h"
#include "ToolDefinition.h"

using namespace std;

Canvas::Canvas()
:m_dragDataRequestedForDrop(false), m_dropItem(nullptr),m_flag(false)
{
    add_events(Gdk::BUTTON_PRESS_MASK | Gdk::BUTTON_RELEASE_MASK | 
        Gdk::POINTER_MOTION_MASK);


    signal_key_press_event().connect(sigc::mem_fun(*this, 
            &Canvas::onDeleteKeyPress));

    set_app_paintable();
}

Canvas::~Canvas()
{
    while(!m_canvasTool.empty())
    {
        std::vector<Tool*>::iterator iter = m_canvasTool
            .begin();
        Tool* item = *iter;
        delete item;
        m_canvasTool.erase(iter);
    }

    delete m_dropItem;
}

int Canvas::getCanvasToolSize()
{
    return m_canvasTool.size();
}

std::vector<Tool*>* Canvas::getItemsVector()
{
    return &m_canvasTool;
}

void Canvas::itemDraw(const Tool *p_item,
    const Cairo::RefPtr<Cairo::Context> &p_cairo,
    bool p_preview)
{
    if(!p_item || !p_item->m_pixbuf)
        return;

    const double cairoX = p_item->m_pixbuf->get_width();
    const double cairoY = p_item->m_pixbuf->get_height();
    
    Gdk::Cairo::set_source_pixbuf(p_cairo, p_item->m_pixbuf,
    p_item->m_x - cairoX * 0.5, p_item->m_y - cairoY * 0.5);    
    if(p_preview)
    {
        p_cairo->paint_with_alpha(0.6);
    }
    else
    {
        p_cairo->paint();
    }
}

bool Canvas::on_draw(const Cairo::RefPtr<Cairo::Context>& p_cairo)
{
    p_cairo->set_source_rgb(1, 1, 1);
    const Gtk::Allocation allocation = get_allocation();
    p_cairo->rectangle(0, 0, allocation.get_width(), allocation.get_height());
    p_cairo->fill();

    for(typeVecItems::iterator iterator = m_canvasTool.begin();
        iterator != m_canvasTool.end(); ++iterator )
    {
        itemDraw(*iterator, p_cairo, false);
    }
    if(m_dropItem)
    {
        itemDraw (m_dropItem, p_cairo, false);
    }

    return true;
}

bool Canvas::on_drag_motion(const Glib::RefPtr<Gdk::DragContext> &p_context,
    int p_x,
    int p_y,
    guint p_time)
{
    m_dragDataRequestedForDrop = false;

    if(m_dropItem)
    {
        m_dropItem->m_x = p_x;
        m_dropItem->m_y = p_y;
        queue_draw();
        p_context->drag_status(Gdk::ACTION_COPY, p_time);

    }
    else
    {
        const Glib::ustring target = drag_dest_find_target(p_context);
        if (target.empty())
        {
            return false;
        }

        drag_get_data(p_context, target, p_time);
    }

    Gtk::DrawingArea::on_drag_motion(p_context, p_x, p_y, p_time);

    return true;
}


void Canvas::on_drag_data_received(
    const Glib::RefPtr<Gdk::DragContext> &p_context, 
    int p_x, 
    int p_y, 
    const Gtk::SelectionData& p_selectionData,
    guint p_info, guint p_time)
{
    Gtk::Widget* widget =nullptr;
    widget = drag_get_source_widget(p_context);

    Gtk::ToolPalette *drag_palette = dynamic_cast<Gtk::ToolPalette*>(widget);
    while(widget && !drag_palette)
    {
        widget = widget->get_parent();
        drag_palette = dynamic_cast<Gtk::ToolPalette*>(widget);
    }

    Gtk::ToolItem *drag_item = nullptr;
    if(drag_palette)
    {
        drag_item = drag_palette->get_drag_item(p_selectionData);
    }

    ToolDefinition *button = dynamic_cast<ToolDefinition*>(drag_item);
    if(button)
    {
        delete m_dropItem;
        m_dropItem = nullptr;
    }
    try
    {
        Tool *item = new Tool(this, button, p_x, p_y);

        if(m_dragDataRequestedForDrop)
        {
            m_canvasTool.push_back(item);
            p_context->drag_finish(true, false, p_time);
        }
        else
        {
            m_dropItem = item;
            p_context->drag_status(Gdk::ACTION_COPY, p_time);
        }
        queue_draw();
    }
    catch (const Gtk::IconThemeError &errorX)
    {
        std::cerr << "IconThemeError: " << errorX.what() << std::endl;
    }

    Gtk::DrawingArea::on_drag_data_received(p_context, p_x, p_y,
        p_selectionData, p_info, p_time);
}


bool Canvas::on_drag_drop(const Glib::RefPtr<Gdk::DragContext> &p_context, 
    int p_x,
    int p_y,
    guint p_time)
{
    const Glib::ustring p_target = drag_dest_find_target(p_context);

    if (p_target.empty())
    {
        return false;
    }

    m_dragDataRequestedForDrop = true;
    drag_get_data(p_context, p_target, p_time);

    return true;
}

bool Canvas::on_button_press_event(GdkEventButton *p_event)
{
    if(p_event->button == 1)
    {
        m_flag = true;

        int xMouse = p_event->x;
        int yMouse = p_event->y;


        int listSize=m_canvasTool.size();
        for (size_t i = 0; i < listSize; ++i)
        {
            int toolCanvasPositionX= m_canvasTool[i]->getAxisX();
            int toolCanvasPositionY= m_canvasTool[i]->getAxisY();
            int toolCanvasHeigth= m_canvasTool[i]->getHeigth();
            int toolCanvasWidth= m_canvasTool[i]->getWidth();
            if(xMouse >= toolCanvasPositionX && xMouse <= toolCanvasWidth && 
            yMouse >= toolCanvasPositionY && yMouse <= toolCanvasHeigth )
            {
                m_canvasTool[i]->m_toolSelectedForDraw=true;
                if(m_canvasTool[i]->m_toolSelectedForEvent){
                    m_canvasTool[i]->m_toolSelectedForEvent=false;
                    std::cout << "Deselected item: " << 
                    m_canvasTool[i]->getIconName() << "\n";
                }
                else 
                {
                    m_canvasTool[i]->m_toolSelectedForEvent=true;
                    std::cout << "Selected item: " << 
                    m_canvasTool[i]->getIconName() << "\n";
                }
                
            }
        }


        return true;
    }
    return false;
}

bool Canvas::on_button_release_event(GdkEventButton *p_event)
{

    if (p_event->button == 1)
    {
        m_flag = false;
        int listSize=m_canvasTool.size();
        for (size_t i = 0; i < listSize; ++i)
        {
            if( m_canvasTool[i]->m_toolSelectedForDraw)
            {
                m_canvasTool[i]->m_toolSelectedForDraw = false; 
            }
        }
        return true;
    }
    return false;
}

bool Canvas::on_motion_notify_event(GdkEventMotion*p_event)
{
    if(m_flag)
    {
        int xMouse = p_event->x;
        int yMouse = p_event->y;
        int listSize=m_canvasTool.size();
        for (size_t i = 0; i < listSize; ++i)
        {
            if(m_canvasTool[i]->m_toolSelectedForDraw)
            {
                m_canvasTool[i]->m_x = xMouse;
                m_canvasTool[i]->m_width = m_canvasTool[i]->m_x  + 50;
                m_canvasTool[i]->m_y = yMouse;
                m_canvasTool[i]->m_heigth = m_canvasTool[i]->m_y + 50;
            }
        }
        queue_draw();
    }
    return true;
}

bool Canvas::onDeleteKeyPress(GdkEventKey *p_event)
{
    bool p_eventHandlerFlag = false;

    std::cout << "Holi :v" << "\n";
    
    if (p_event->type == GDK_KEY_PRESS && p_event->keyval == GDK_KEY_Delete)
    {
        p_eventHandlerFlag = true;
    }
    if(p_eventHandlerFlag)
    {
        std::cout << "Del key pressed !" << "\n"; 
        // Tool::removeSelectedElements();
    }
    std::cout << "Key pressed: " << p_event->hardware_keycode
        << "\n";
    return false;
}