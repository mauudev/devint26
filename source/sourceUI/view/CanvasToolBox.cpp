#include "CanvasToolBox.h"
#include "ActionToolToggle.h"
#include "ActionToolButton.h"
#include <iostream>

CanvasToolBox::CanvasToolBox()
:Gtk::Box(Gtk::ORIENTATION_VERTICAL, 2),
    m_actionToolSelect{"../resources/cursor.svg"},
    m_actionToolRemove{"../resources/empty_trash.svg"},
    m_actionToolSelectArea{"../resources/add_row.svg"},
    m_actionToolLink{"../resources/serial_tasks.svg"},
    m_actionToolCamera{"../resources/camera.svg"},
    m_actionToolCopy{"../resources/add_image.svg"},
    m_actionToolDelete{"../resources/remove_image.svg"},
    m_actionToolClear{"../resources/clear.svg"}
{
    m_actionToolSelect.activate();
    eventsToggle();
    eventsButton();
    pack_start(m_actionToolSelect);
    pack_start(m_actionToolRemove);
    pack_start(m_actionToolSelectArea);
    pack_start(m_actionToolLink);
    pack_start(m_actionToolCamera);
    pack_start(m_actionToolCopy);
    pack_start(m_actionToolDelete);
    pack_start(m_actionToolClear);

}

CanvasToolBox::~CanvasToolBox()
{
    //delete m_pointCanvas;
}

void CanvasToolBox::eventsButton()
{
    m_actionToolCamera.signal_clicked().connect( sigc::mem_fun(*this,
        &CanvasToolBox::actionCanvasCamera) );
    m_actionToolCopy.signal_clicked().connect( sigc::mem_fun(*this,
        &CanvasToolBox::actionCanvasCopy) );
    m_actionToolDelete.signal_clicked().connect( sigc::mem_fun(*this,
        &CanvasToolBox::actionCanvasDelete) );
    m_actionToolClear.signal_clicked().connect( sigc::mem_fun(*this,
        &CanvasToolBox::actionCanvasClear) );
}
void CanvasToolBox::eventsToggle()
{
    m_actionToolSelect.signal_pressed().connect( sigc::mem_fun(*this,
        &CanvasToolBox::mutuallyExclusiveSelect) );
    m_actionToolRemove.signal_pressed().connect( sigc::mem_fun(*this,
        &CanvasToolBox::mutuallyExclusiveRemove) );
    m_actionToolLink.signal_pressed().connect( sigc::mem_fun(*this,
        &CanvasToolBox::mutuallyExclusiveLink) );
    m_actionToolSelectArea.signal_pressed().connect( sigc::mem_fun(*this,
        &CanvasToolBox::mutuallyExclusiveSelectArea) );
}

const char* CanvasToolBox::whichIsActive()
{
    if(m_actionToolSelect.isActive())
    {
        return "select"; 
    }
    if(m_actionToolRemove.isActive())
    {
        return "remove";
    }
    if(m_actionToolSelectArea.isActive())
    {
        return "selectarea";
    }
    if(m_actionToolLink.isActive())
    {
        return "link";
    }
    return "nothing";
}

void CanvasToolBox::mutuallyExclusiveSelect()
{
    m_actionToolSelectArea.deactivate();
    m_actionToolLink.deactivate();
    m_actionToolRemove.deactivate();
}
void CanvasToolBox::mutuallyExclusiveRemove()
{
    m_actionToolSelect.deactivate();
    m_actionToolSelectArea.deactivate();
    m_actionToolLink.deactivate();
}
void CanvasToolBox::mutuallyExclusiveSelectArea()
{
    m_actionToolSelect.deactivate();
    m_actionToolRemove.deactivate();
    m_actionToolLink.deactivate();
}
void CanvasToolBox::mutuallyExclusiveLink()
{
    m_actionToolSelect.deactivate();
    m_actionToolRemove.deactivate();
    m_actionToolSelectArea.deactivate();
}

void CanvasToolBox::actionCanvasCopy()
{
    if(m_pointCanvas==nullptr)
    {
        std::cout<<"no tenemos canvas donde apuntar\n";
    }
    else
    {
        std::cout<<"copiaste los archivos seleccionados\n";
    }
}

void CanvasToolBox::actionCanvasDelete()
{
    std::cout << "Tamano: " << m_pointCanvas->getCanvasToolSize() << "\n";
    removeSelectedTools();
}

void CanvasToolBox::actionCanvasCamera()
{
     if(m_pointCanvas==nullptr)
    {
        std::cout<<"no tenemos canvas donde apuntar\n";
    }
    else
    {
        std::cout<<"Sacaste fotito\n";
    }
}

void CanvasToolBox::actionCanvasClear()
{
     if(m_pointCanvas==nullptr)
    {
        std::cout<<"Limpiaste el canvas\n";
    }
    else
    {
        std::cout<<"Limpiaste el Canvas\n";
    }
}

void CanvasToolBox::setPointCanvas(Canvas* p_canvas)
{
    m_pointCanvas = p_canvas;
}

const CanvasToolBox* CanvasToolBox::getInstance()
{
    return this;
}

void CanvasToolBox::removeSelectedTools()
{
    std::vector<Tool*> *vectorItems = m_pointCanvas->getItemsVector();
    int i = 0;
    while(i < vectorItems->size())
    {
        if(vectorItems->at(i)->isSelectedForEvent()){
            std::cout << "Existen elemento seleccionado s" << "\n";
            vectorItems->erase(vectorItems->begin()+i);
            i = 0;
            continue;
        }
        else std::cout << "No seleccionado s" << "\n";
        i++;
    }   
}
