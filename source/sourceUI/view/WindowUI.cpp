#include "WindowUI.h"

WindowUI::WindowUI()
:m_vBoxMain(Gtk::ORIENTATION_VERTICAL), m_menuBox(Gtk::ORIENTATION_VERTICAL),
m_infoBox(Gtk::ORIENTATION_VERTICAL)
{
    set_title("Thinker");
    set_border_width(6);
    add(m_vBoxMain);

    m_vBoxMain.pack_start(m_menuBox, Gtk::PACK_SHRINK, 5);
    m_menuBox.set_border_width(5);
    m_menuBar->createMenuBar(m_menuBox);

    m_vBoxMain.pack_start(m_hBox, Gtk::PACK_EXPAND_WIDGET, 5);
    m_hBox.set_border_width(5);

    Gtk::ToolPalette& auxToolPalette = m_toolPalette.getToolPalette();
    m_hBox.pack_start(auxToolPalette, Gtk::PACK_SHRINK, 5);
    m_hBox.pack_start(m_canvas, Gtk::PACK_EXPAND_WIDGET, 5);
    m_hBox.pack_start(m_canvasToolBox, Gtk::PACK_SHRINK, 5);

    auxToolPalette.add_drag_dest(m_canvas,
    Gtk::DEST_DEFAULT_HIGHLIGHT, Gtk::TOOL_PALETTE_DRAG_ITEMS,
        Gdk::ACTION_COPY);


    m_vBoxMain.pack_start(m_infoBox, Gtk::PACK_SHRINK, 5);
    m_infoBox.set_border_width(5);
    m_infoBox.pack_start(m_infoBar.getInstance(), Gtk::PACK_SHRINK);
    m_infoBar.setMessage("MOCKUP's Demostration Example", Gtk::MESSAGE_OTHER);

    Canvas *p_canvas = &m_canvas;
    m_canvasToolBox.setPointCanvas(p_canvas);
    
    show_all_children();
}

WindowUI::~WindowUI()
{
}

