#include "Buttonbox.h"

ButtonBox::ButtonBox(bool p_horizontal, gint p_spacing,
    Gtk::ButtonBoxStyle p_layout)
{
    if(p_horizontal)
    {
        m_buttonBox.set_orientation(Gtk::ORIENTATION_HORIZONTAL);
    }
    else
    {
        m_buttonBox.set_orientation(Gtk::ORIENTATION_VERTICAL);
    }
    m_buttonBox.set_border_width(5);
    m_buttonBox.set_layout(p_layout);
    m_buttonBox.set_spacing(p_spacing);
}

Gtk::ButtonBox &ButtonBox::getInstance()
{
    return m_buttonBox;
}

void ButtonBox::packStart(Gtk::Widget &widget, const Gtk::PackOptions &option, int spacing)
{
    m_buttonBox.pack_start(widget, option, spacing);
}