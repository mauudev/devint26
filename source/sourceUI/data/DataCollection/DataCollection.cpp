#include "DataCollection.h"
#include <iostream>

DataCollection::DataCollection()
{
    m_dataCollection = new std::map<Glib::ustring,Glib::ustring>();
}

DataCollection::~DataCollection()
{
    delete m_dataCollection;
    delete this;
}

void DataCollection::insert( Glib::ustring p_id, Glib::ustring p_obj)const
{
    m_dataCollection->insert (
        std::pair<Glib::ustring,Glib::ustring>(p_id, p_obj));
}

void DataCollection::remove(const Glib::ustring p_id)
{
    try
    {
        m_dataCollection->erase(p_id);
    }
    catch(const std::exception &ex)
    {
        std::cout << "The element to remove doesn't exists: " 
            << ex.what() << "\n";
    }
}

const Glib::ustring DataCollection::find(const Glib::ustring p_id) const
{
    if(m_dataCollection->find(p_id)->second.empty()) return ""; 
    else return m_dataCollection->find(p_id)->second;
}

void DataCollection::clear()
{
    m_dataCollection->clear();
}

size_t DataCollection::getSize() const
{
    return m_dataCollection->size();
}

std::map<Glib::ustring,Glib::ustring>* DataCollection::getDataCollection()
{
    return m_dataCollection;
}
