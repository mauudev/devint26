#ifndef DATA_COLLECTION_H
#define DATA_COLLECTION_H

#include <glibmm.h>
#include <gtkmm.h>

class DataCollection
{
    std::map<Glib::ustring,Glib::ustring> *m_dataCollection;

public:

    DataCollection();
    ~DataCollection();
    void insert(Glib::ustring, Glib::ustring)const;
    void remove(const Glib::ustring);
    const Glib::ustring find(const Glib::ustring)const;
    void clear();
    size_t getSize()const; 
    std::map<Glib::ustring, Glib::ustring>* getDataCollection();
};

#endif