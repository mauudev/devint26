#include "Thinker.h"

Thinker::Thinker()
:m_project{nullptr}
{
}

Thinker::~Thinker()
{
}

Thinker *Thinker::getInstance()
{
    if (m_instance == nullptr)
    {
        m_instance = new Thinker();
    }
    return m_instance;
}

Thinker *Thinker::m_instance = nullptr;

Project *Thinker::newProject()
{
    m_project = new Project();
    return m_project;
}

ProjectToolbox *Thinker::getProjectToolbox() const
{
    return m_projectToolbox;
}

AgentToolbox *Thinker::getAgentToolbox() const
{
    return m_agentToolbox;
}

Project *Thinker::getProject() const
{
    return m_project;
}