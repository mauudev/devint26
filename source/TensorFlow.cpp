#include "TensorFlow.h"

TensorFlow::TensorFlow()
{

}

TensorFlow::TensorFlow(const string &p_tensorFlowName)
:m_tensorFlowName{p_tensorFlowName}
{
}

TensorFlow::~TensorFlow()
{
}

const string TensorFlow::getTensorFlowName() const
{
    return m_tensorFlowName;
}

void TensorFlow::setTensorFlowName(const string &p_tensorFlowName)
{
    m_tensorFlowName = p_tensorFlowName;
}

Json TensorFlow::serialize()
{
    Json jsonAnswer;
    jsonAnswer["tensorFlowName"] = m_tensorFlowName;
    return jsonAnswer;
}

void TensorFlow::deserialize(Json &p_json)
{
    string auxiliarName = p_json["tensorFlowName"];
    this->setTensorFlowName(auxiliarName.data());
}

Statement &TensorFlow::setASTTensorFlowName(BuilderCodePy &py)
{
    return 
    py.functionDef("setTensorFlowName")
        .addParameter(&py.name("self"))
        .addParameter(&py.name("tensorFlowName"))
            .addBodyNode(
                &py.assign(
                    &py.attribute(
                        &py.name("self"),"tensorFlowName"
                    ),
                    &py.name("tensorFlowName")
                )
            );
}

Statement &TensorFlow::getASTTensorFlowName(BuilderCodePy &py)
{
    return 
    py.functionDef("getTensorFlowName")
        .addParameter(&py.name("self"))
        .addBodyNode(
            &py.returnStmt(
                &py.attribute(&py.name("self"),"tensorFlowName")
            )
        );
}

Statement &TensorFlow::generateASTInitNode(BuilderCodePy &py)
{
    return
    py.functionDef("__init__")
        .addParameter(&py.name("self"))
            .addBodyNode(
                &py.assign(
                    &py.attribute(&py.name("self"),"tensorFlowName"),
                    &py.name("\""+m_tensorFlowName+"\"")
                )
            );
}

Statement &TensorFlow::generateASTBodyNode(BuilderCodePy &py)
{
    return
    py.concat()
        .addBodyNode(&getASTTensorFlowName(py))
        .addBodyNode(&setASTTensorFlowName(py));
}

Statement &TensorFlow::generateASTClassNode(BuilderCodePy &py)
{
    return
    py.classDef("TensorFlow")
        .addParameter(&py.name("object"))
        .addBodyNode(&generateASTInitNode(py))
        .addBodyNode(&generateASTBodyNode(py));
}