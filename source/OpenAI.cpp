#include "OpenAI.h"

OpenAI::OpenAI()
{
}

OpenAI::OpenAI(const string &p_openAIName)
:m_openAIName{p_openAIName}
{
}

OpenAI::~OpenAI()
{
}

const string OpenAI::getOpenAIName() const
{
    return m_openAIName;
}

void OpenAI::setOpenAIName(const string &p_openAIName)
{
    m_openAIName = p_openAIName;
}

Json OpenAI::serialize()
{
    Json jsonAnswer;
    jsonAnswer["openAIName"] = m_openAIName;
    return jsonAnswer;
}

void OpenAI::deserialize(Json &p_json)
{
    string auxiliarName = p_json["openAIName"];
    this->setOpenAIName(auxiliarName.data());
}

Statement &OpenAI::setASTOpenAIName(BuilderCodePy &py)
{
    return py.functionDef("setOpenAIName")
    .addParameter(&py.name("self"))
    .addParameter(&py.name("openAIName"))
        .addBodyNode(
            &py.assign(
                &py.attribute(
                    &py.name("self"),"openAIName"
                ),
                &py.name("openAIName")
            )
        );
}

Statement &OpenAI::getASTOpenAIName(BuilderCodePy &py)
{
    return py.functionDef("getOpenAIName")
    .addParameter(&py.name("self"))
        .addBodyNode(
            &py.returnStmt(
                &py.attribute(&py.name("self"),"openAIName")
            )
        );
}

Statement &OpenAI::generateASTInitNode(BuilderCodePy &py)
{
    return py.functionDef("__init__")
    .addParameter(&py.name("self"))
        .addBodyNode(
            &py.assign(
                &py.attribute(&py.name("self"),"openAIName"),
                &py.name("\""+m_openAIName+"\"")
            )
        );
}

Statement &OpenAI::generateASTBodyNode(BuilderCodePy &py)
{
    return py.concat()
    .addBodyNode(&getASTOpenAIName(py))
    .addBodyNode(&setASTOpenAIName(py));
}

Statement &OpenAI::generateASTClassNode(BuilderCodePy &py)
{
    return py.classDef("OpenAI")
    .addParameter(&py.name("object"))
        .addBodyNode(&generateASTInitNode(py))
        .addBodyNode(&generateASTBodyNode(py));
}
