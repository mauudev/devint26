#include "ScreenHandler.h"

ScreenHandler::ScreenHandler()
{
}

ScreenHandler::ScreenHandler(const int &p_screenSpeed, const int &p_screenHeight, const int &p_screenWidth, const string &p_screenName)
:m_screenSpeed{p_screenSpeed}, m_screenHeight{p_screenHeight}, m_screenWidth{p_screenWidth}
    , m_screenName{p_screenName}
{
}

ScreenHandler::~ScreenHandler()
{
}

void ScreenHandler::setScreenHandlerSpeed(const int &p_screenSpeed)
{
    m_screenSpeed = p_screenSpeed;
}

void ScreenHandler::setScreenHandlerHeight(const int &p_screenHeight)
{
    m_screenHeight = p_screenHeight;
}

void ScreenHandler::setScreenHandlerWidth(const int &p_screenWidth)
{
    m_screenWidth = p_screenWidth;
}

void ScreenHandler::setScreenHandlerName(const string &p_screenName)
{
    m_screenName = p_screenName;
}

const int ScreenHandler::getScreenHandlerSpeed() const
{
    return m_screenSpeed;
}

const int ScreenHandler::getScreenHandlerHeight() const
{
    return m_screenHeight;
}

const int ScreenHandler::getScreenHandlerWidth() const
{
    return m_screenWidth;
}

const string ScreenHandler::getScreenHandlerName() const
{
    return m_screenName;
}

Statement &ScreenHandler::setScreenHandlerNameGenerator(BuilderCodePy &py)
{
    return 
    py.functionDef("setScreenHandlerName")
        .addParameter(&py.name("self"))
        .addParameter(&py.name("screenName"))
        .addBodyNode(
            &py.assign(
                &py.attribute(
                    &py.name("self"),"screenName"), &py.name("screenName")
            )
        );
}

Statement &ScreenHandler::setScreenHandlerSpeedGenerator(BuilderCodePy &py)
{
    return 
    py.functionDef("setScreenHandlerSpeed")
        .addParameter(&py.name("self"))
        .addParameter(&py.name("screenSpeed"))
        .addBodyNode(
            &py.assign(
                &py.attribute(
                    &py.name("self"),"screenSpeed"), &py.name("screenSpeed")
            )
        );
}

Statement &ScreenHandler::setScreenHandlerHeightGenerator(BuilderCodePy &py)
{
    return 
    py.functionDef("setScreenHandlerHeight")
        .addParameter(&py.name("self"))
        .addParameter(&py.name("screenHeight"))
        .addBodyNode(
            &py.assign(
                &py.attribute(
                    &py.name("self"), "screenHeight"), &py.name("screenHeight")
            )
        );
}

Statement &ScreenHandler::setScreenHandlerWidthGenerator(BuilderCodePy &py)
{
    return 
    py.functionDef("setScreenHandlerWidth")
        .addParameter(&py.name("self"))
        .addParameter(&py.name("screenWidth"))
        .addBodyNode(
            &py.assign(
                &py.attribute(
                    &py.name("self"), "screenWidth"), &py.name("screenWidth")
            )
        );
}

Statement &ScreenHandler::getScreenHandlerNameGenerator(BuilderCodePy &py) const
{
    return 
    py.functionDef("getScreenHandlerName")
        .addParameter(&py.name("self"))
        .addBodyNode(
            &py.returnStmt(
                &py.attribute(
                    &py.name("self"), "screenName")
            )
        );
}

Statement &ScreenHandler::getScreenHandlerSpeedGenerator(BuilderCodePy &py) const
{
    return 
    py.functionDef("getScreenHandlerSpeed")
        .addParameter(&py.name("self"))
        .addBodyNode(
            &py.returnStmt(
                &py.attribute(
                    &py.name("self"), "screenSpeed")
            )
        );
}

Statement &ScreenHandler::getScreenHandlerHeightGenerator(BuilderCodePy &py) const
{
    return 
    py.functionDef("getScreenHandlerHeight")
        .addParameter(&py.name("self"))
        .addBodyNode(
            &py.returnStmt(
                &py.attribute(
                    &py.name("self"), "screenHeight")
            )
        );
}

Statement &ScreenHandler::getScreenHandlerWidthGenerator(BuilderCodePy &py) const
{
    return 
    py.functionDef("getScreenHandlerWidth")
        .addParameter(&py.name("self"))
        .addBodyNode(
            &py.returnStmt(
                &py.attribute(
                    &py.name("self"), "screenWidth")
            )
        );
}

Statement &ScreenHandler::generateASTInitNode(BuilderCodePy &py)
{
    return 
    py.functionDef("__init__")
        .addParameter(&py.name("self"))
        .addBodyNode(
            &py.assign(
                &py.attribute(
                    &py.name("self"),"screenName"),
                    &py.name("\""+m_screenName+"\"")
            )
        )
        .addBodyNode(
            &py.assign(
                &py.attribute(
                    &py.name("self"),"screenSpeed"),
                    &py.num(m_screenSpeed)
            )
        )
        .addBodyNode(
            &py.assign(
                &py.attribute(
                    &py.name("self"),"screenWidth"),
                    &py.num(m_screenWidth)
            )
        )
        .addBodyNode(
            &py.assign(
                &py.attribute(
                    &py.name("self"),"screenHeight"),
                    &py.num(m_screenHeight)
            )
        );
}

Statement &ScreenHandler::generateASTBodyNode(BuilderCodePy &py)
{
    return 
    py.concat()
        .addBodyNode(&setScreenHandlerNameGenerator(py))
        .addBodyNode(&setScreenHandlerSpeedGenerator(py))
        .addBodyNode(&setScreenHandlerWidthGenerator(py))
        .addBodyNode(&setScreenHandlerHeightGenerator(py))
        .addBodyNode(&getScreenHandlerNameGenerator(py))
        .addBodyNode(&getScreenHandlerSpeedGenerator(py))
        .addBodyNode(&getScreenHandlerWidthGenerator(py))
        .addBodyNode(&getScreenHandlerHeightGenerator(py));
}

Statement &ScreenHandler::generateASTClassNode(BuilderCodePy &py)
{
    return 
    py.classDef("ScreenHandler")
        .addParameter(&py.name("object"))
        .addBodyNode(&generateASTInitNode(py))
        .addBodyNode(&generateASTBodyNode(py));
}

Json ScreenHandler::serialize()
{
    Json json;
    json["screenSpeed"]= m_screenSpeed;
    json["screenHeight"] = m_screenHeight;
    json["screenWidth"] = m_screenWidth;
    json["screenName"] = m_screenName;
    return json;
}

void ScreenHandler::deserialize(Json &p_json)
{
    this->setScreenHandlerSpeed(p_json["screenSpeed"]);
    this->setScreenHandlerHeight(p_json["screenHeight"]);
    this->setScreenHandlerWidth(p_json["screenWidth"]);
    this->setScreenHandlerName(p_json["screenName"]);
}